var app = angular.module('testApp', []);
app.controller('treeTable',function ($scope) {
  $scope.list = [
    {
      name: 'Developer',
      // opened: true,
      children: [
        {
          name: 'Front-End',
          children: [
            {
              name: 'AAA',
              children: [
                {
                  name: 'BBB',
                  children: [
                    {
                      name: 'BBB',
                      title: 'Leader'
                    },
                    {
                      name: 'John',
                      title: 'Senior F2E'
                    },
                    {
                      name: 'Jason',
                      title: 'Junior F2E'
                    }
                  ]
                },
                {
                  name: 'John',
                  title: 'Senior F2E'
                },
                {
                  name: 'Jason',
                  title: 'Junior F2E'
                }
              ]
            },
            {
              name: 'John',
              title: 'Senior F2E'
            },
            {
              name: 'Jason',
              title: 'Junior F2E'
            }
          ]
        },
        {
          name: 'Back-End',
          children: [
            {
              name: 'Mary',
              title: 'Leader'
            },
            {
              name: 'Gary',
              title: 'Intern'
            }
          ]
        }
      ]
    },
    {
      name: 'Design',
      children: [
        {
          name: 'Freeman',
          title: 'Designer'
        }
      ]
    },
    {
      name: 'S&S',
      children: [
        {
          name: 'Nikky',
          title: 'Robot'
        }
      ]
    }
  ];
});
