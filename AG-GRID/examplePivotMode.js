agGrid.initialiseAgGridWithAngular1(angular);

var module = angular.module("example", ["agGrid"]);

module.controller("exampleCtrl", function($scope) {

    var columnDefs = [
        {headerName: "Country", field: "country", width: 120, rowGroupIndex: 1},
        {headerName: "Year", field: "year", width: 90, rowGroupIndex: 2},
        {headerName: "Date", field: "date", width: 110},
        {headerName: "Sport", field: "sport", width: 110},
        {headerName: "Gold", field: "gold", width: 100, aggFunc: 'sum'},
        {headerName: "Silver", field: "silver", width: 100, aggFunc: 'sum'},
        {headerName: "Bronze", field: "bronze", width: 100, aggFunc: 'sum'}
    ];


    var rowData = [
        {country: "USA", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "USA", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "AFRICA", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "INDONESIA", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "PAKISTAN", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "BRAZIL", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "SAUDI ARABIA", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"},
        {country: "ENGLAND", year: "2011", date: "2011.01", sport: "cricket", gold: "a", silver: "b" , bronze: "c"}
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: rowData,
        enableColResize: true,
    };
    $scope.onBtPivotMode = function(){
        $scope.gridOptions.columnApi.setPivotMode(true);
        $scope.gridOptions.columnApi.setPivotColumns([]);
        $scope.gridOptions.columnApi.setRowGroupColumns(['country','year']);
    }

});