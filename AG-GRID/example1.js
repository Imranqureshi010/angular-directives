agGrid.initialiseAgGridWithAngular1(angular);

var module = angular.module("example", ["agGrid"]);

module.controller("exampleCtrl", function($scope) {

    var columnDefs = [
      {
        headerName: "A",
        children: [
            {
              headerName: "AB",
              field: "AB",
              children: [
                {
                  headerName: "AC",
                  field: "AC"
                },
                {
                  headerName: "AD",
                  field: "AD"
                },
              ]
            },
        ]
      },
      {
        headerName: "B",
        children: [
            {
              headerName: "BA",
              field: "BA",
              children: [
                {
                  headerName: "BC",
                  field: "BC"
                },
                {
                  headerName: "BD",
                  field: "BD"
                },
              ]
            },
        ]
      },
    ];

    var rowData = [
        {make: "Toyota", model: "Celica", price: 35000},
        {make: "Ford", model: "Mondeo", price: 32000},
        {make: "Porsche", model: "Boxter", price: 72000}
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: rowData
    };

});