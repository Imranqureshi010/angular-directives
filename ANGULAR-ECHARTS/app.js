var app = angular.module('demo', ['angular-echarts']);
app.controller('LineChartController', function ($scope) {
    // CHART CONFIGURATION
    var chart_config = {
      "title": "Heat Map",
      "debug": true,
      "showXAxis": true,
      "showYAxis": true,
      "showLegend": false,
      "stack": false,
      "label": true,
      "height": 500,
      "calculable": false,
      "toolbox": {
        show : true,
        padding : [0,20,0,0],
        feature : {
          mark : {show: true},
          dataView : {show: true, readOnly: false},
          magicType : {show: true, type: [/*'line','bar'*/]},
          restore : {show: true},
          saveAsImage : {show: true}
        }
      }
    }

    // ARRAYS FOR CORRESPONDING CHART
    var arr_one = {
      datapoints: [
          {
            "y": "47.71",
            "x": "Count Duration of Movies PG  of Last 3 Months"
          },
          {
            "y": "17.29",
            "x": "Count Duration of PG  of Last 3 Months"
          },
          {
            "y": "4.59",
            "x": "Total Duration of PG-13  of Last 3 Months"
          },
          {
            "y": "3.12",
            "x": "Count Duration of PG-13  of Last 3 Months"
          },
          {
            "y": "0.39",
            "x": "Total Duration   of Last 3 Months"
          },
          {
            "y": "24.97",
            "x": "Total Duration of MOVIE  of Last 3 Months"
          },
          {
            "y": "1.93",
            "x": "Total Duration of R  of Last 3 Months"
          }
      ]
    };
    var arr_two = {
        name:"Auditirail",
        datapoints: [
            { x: 2001, y: 22 },
            { x: 2002, y: 13 },
            { x: 2003, y: 35 },
            { x: 2004, y: 52 },
            { x: 2005, y: 32 },
            { x: 2006, y: 40 },
            { x: 2007, y: 63 },
            { x: 2008, y: 80 },
            { x: 2009, y: 20 },
            { x: 2010, y: 25 },
        ]
    };
    var arr_three = {
        datapoints: [
            { x: 2001, y: 18 },
            { x: 2002, y: 34 },
            { x: 2003, y: 44 },
            { x: 2004, y: 4 },
            { x: 2005, y: 99 },
            { x: 2006, y: 40 },
            { x: 2007, y: 63 },
            { x: 2008, y: 80 },
            { x: 2009, y: 20 },
            { x: 2010, y: 25 },
        ]
    };
    var arr_four = {
      name:"temp",
      datapoints: [{x:"Allocated Budget",y:[4300, 10000, 28000, 35000, 50000, 19000]}]
    }
    var arr_five = {
      "name": "Tree View",
      "datapoints": [
        {
          "name": "Auditirail",
          "value": 2674998,
          "children": [
            {
              "name": "current_database",
              "value": 891666,
              "children": [
                {
                  "name": "INPUT_ADJ",
                  "value": 225000
                },
                {
                  "name": "ICJOURNAL",
                  "value": 220834
                },
                {
                  "name": "COPR_ADJ",
                  "value": 216666
                },
                {
                  "name": "MGMT_COGS_ADJ",
                  "value": 229166
                }
              ]
            },
            {
              "name": "forcasting_database",
              "value": 1783332,
              "children": [
                {
                  "name": "INPUT_ADJ",
                  "value": 460000
                },
                {
                  "name": "ICJOURNAL",
                  "value": 364000
                },
                {
                  "name": "COPR_ADJ",
                  "value": 403332
                },
                {
                  "name": "MGMT_COGS_ADJ",
                  "value": 556000
                }
              ]
            }
          ]
        }
      ]
    }

    var force_data = {
      nodes:[
        {"name": "SALARY", "value": 777, "label": "SALARY"},
        {"name": "SALARY (current_database)", "value": ""},
        {"name": "INPUT_ADJ(current_database)", "value": 225000 },
        {"name": "ICJOURNAL(current_database)", "value": 220834 },
        {"name": "COPR_ADJ(current_database)", "value": 216666 },
        {"name": "MGMT_COGS_ADJ(current_database)", "value": 229166 },
        {"name": "SALARY (forcasting_database)", "value": ""},
        {"name": "INPUT_ADJ(forcasting_database)", "value": 230000 },
        {"name": "ICJOURNAL(forcasting_database)", "value": 182000 },
        {"name": "COPR_ADJ(forcasting_database)", "value": 201666 },
        {"name": "MGMT_COGS_ADJ(forcasting_database)", "value": 278000 }
      ],
      links:[
        {"source": "SALARY (current_database)", "target": "SALARY"},
        {"source": "INPUT_ADJ(current_database)", "target": "SALARY (current_database)"},
        {"source": "ICJOURNAL(current_database)", "target": "SALARY (current_database)"},
        {"source": "COPR_ADJ(current_database)", "target": "SALARY (current_database)"},
        {"source": "MGMT_COGS_ADJ(current_database)", "target": "SALARY (current_database)"},
        {"source": "SALARY (forcasting_database)", "target": "SALARY"},
        {"source": "INPUT_ADJ(forcasting_database)", "target": "SALARY (forcasting_database)"},
        {"source": "ICJOURNAL(forcasting_database)", "target": "SALARY (forcasting_database)"},
        {"source": "COPR_ADJ(forcasting_database)", "target": "SALARY (forcasting_database)"},
        {"source": "MGMT_COGS_ADJ(forcasting_database)", "target": "SALARY (forcasting_database)"}
      ]
    }


    $scope.lasanga_pie_chart = function(){
      $scope.lasanga_pie_chart_config = angular.copy(chart_config);
      $scope.lasanga_pie_chart_config.title = "Lasanga Chart";
      $scope.lasanga_pie_chart_config.subtitle = "Lasanga Chart";
      $scope.lasanga_pie_chart_config.toolbox.feature.magicType.type = ['pie','funnel'];
      $scope.lasanga_pie_chart_config.showLegend = true;
      $scope.lasanga_pie_chart_config.support_type = 'lasanga';
      /*--There is only one changes will be for nested pie chart and that is "Radius"
      For example : If you need two nested so set two array for radius as like for more, You also can set only string if there is only one like "60%"
      Js Changes : '/src/util.js/' find for 'if(config.radius){'";*/
      $scope.lasanga_pie_chart_config.radius = [[0,70],[100,140]];
      // $scope.lasanga_pie_chart_data = [arr_one]; //CHART WITH SINGLE DATA
      $scope.lasanga_pie_chart_data = [arr_one,arr_two,arr_three]; //CHART WITH MULTIPLE DATA
    }

    $scope.pie_chart = function(){
      $scope.pie_chart_config = angular.copy(chart_config);
      $scope.pie_chart_config.title = "Pie Chart";
      $scope.pie_chart_config.subtitle = "Pie Chart";
      $scope.pie_chart_config.toolbox.feature.magicType.type = ['pie','funnel'];
      $scope.pie_chart_config.showLegend = true;
      /*--There is only one changes will be for nested pie chart and that is "Radius"
      For example : If you need two nested so set two array for radius as like for more, You also can set only string if there is only one like "60%"
      Js Changes : '/src/util.js/' find for 'if(config.radius){'";*/
      $scope.pie_chart_config.radius = [[0,70],[100,140]];
      // $scope.pie_chart_data = [arr_one]; //CHART WITH SINGLE DATA
      $scope.pie_chart_data = [arr_one,arr_two]; //CHART WITH MULTIPLE DATA
    }

    $scope.scatter_chart =  function(){
      var group_one = {
        name:"Cluster #1",
        datapoints: [
          [161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6],
          [170.0, 59.0], [159.1, 47.6], [166.0, 69.8], [176.2, 66.8], [160.2, 75.2],
          [172.5, 55.2], [170.9, 54.2], [172.9, 62.5], [153.4, 42.0], [160.0, 50.0],
          [147.2, 49.8], [168.2, 49.2], [175.0, 73.2], [157.0, 47.8], [167.6, 68.8],
          [159.5, 50.6], [175.0, 82.5], [166.8, 57.2], [176.5, 87.8], [170.2, 72.8],
          [174.0, 54.5], [173.0, 59.8], [179.9, 67.3], [170.5, 67.8], [160.0, 47.0],
          [154.4, 46.2], [162.0, 55.0], [176.5, 83.0], [160.0, 54.4], [152.0, 45.8],
          [162.1, 53.6], [170.0, 73.2], [160.2, 52.1], [161.3, 67.9], [166.4, 56.6],
          [168.9, 62.3]
        ]
      };

      var group_two = {
        name:"Cluster #2",
        datapoints: [
          [174.0, 65.6], [175.3, 71.8], [193.5, 80.7], [186.5, 72.6], [187.2, 78.8],
          [181.5, 74.8], [184.0, 86.4], [184.5, 78.4], [175.0, 62.0], [184.0, 81.6],
          [180.0, 76.6], [177.8, 83.6], [192.0, 90.0], [176.0, 74.6], [174.0, 71.0],
          [184.0, 79.6], [192.7, 93.8], [171.5, 70.0], [173.0, 72.4], [176.0, 85.9],
          [176.0, 78.8], [180.5, 77.8], [172.7, 66.2], [176.0, 86.4], [173.5, 81.8],
          [178.0, 89.6], [180.3, 82.8], [180.3, 76.4], [164.5, 63.2], [173.0, 60.9],
          [183.5, 74.8], [175.5, 70.0], [188.0, 72.4], [189.2, 84.1], [172.8, 69.1],
          [170.0, 59.5], [182.0, 67.2], [170.0, 61.3], [177.8, 68.6], [184.2, 80.1],
          [186.7, 87.8], [171.4, 84.7]
        ]
      };

      $scope.scatter_chart_config = angular.copy(chart_config);
      $scope.scatter_chart_config.title = "Scatter Chart";
      $scope.scatter_chart_config.subtitle = "Scatter Chart";
      $scope.scatter_chart_data = [group_one,group_two]; 
    }

    $scope.heatmap_chart =  function(){
      var group_one = {
        name:"heatmap #1",
        datapoints: [
          [240,50,100],
          [930, 50,300],
          [930,425,2],
          [60,425,2]
        ]
      };


      $scope.heatmap_chart_config = angular.copy(chart_config);
      $scope.heatmap_chart_config.title = "heatmap Chart";
      $scope.heatmap_chart_config.subtitle = "heatmap Chart";
      $scope.heatmap_chart_data = [
        {
          "datapoints": [
            [
              240, // LEFT MARGIN
              50, // TOP MARGIN
              1 // OPACITY
            ],
            [
              930,
              50,
              1
            ],
            [
              930,
              425,
              1
            ],
            [
              60,
              425,
              1
            ]
          ]
        }
      ]; 
    }

    $scope.bar_chart = function(){
      $scope.bar_chart_config = angular.copy(chart_config);
      $scope.bar_chart_config.title = "Bar Chart";
      $scope.bar_chart_config.subtitle = "Bar Chart";
      $scope.bar_chart_data = [arr_one]; //CHART WITH SINGLE DATA
      $scope.bar_chart_data = [arr_one,arr_two,arr_three]; //CHART WITH MULTIPLE DATA
    }
    

    $scope.radar_chart = function(){
      $scope.radar_chart_config = angular.copy(chart_config);
      $scope.radar_chart_config.title = "Radar Chart";
      $scope.radar_chart_config.subtitle = "Radar Chart";
      $scope.radar_chart_config.polar = [
         {
             indicator : [
                 { text: '销售（sales）', max: 6000},
                 { text: '管理（Administration）', max: 16000},
                 { text: '信息技术（Information Techology）', max: 30000},
                 { text: '客服（Customer Support）', max: 38000},
                 { text: '研发（Development）', max: 52000},
                 { text: '市场（Marketing）', max: 25000}
              ]
          }
      ];
      $scope.radar_chart_data = [arr_four]; //CHART WITH SINGLE DATA
    }

    $scope.filled_radar_chart = function(){
      $scope.filled_radar_chart_config = angular.copy(chart_config);
      $scope.filled_radar_chart_config.title = "Filled Radar Chart";
      $scope.filled_radar_chart_config.subtitle = "Filled Radar Chart";
      $scope.filled_radar_chart_config.support_type = "filled_radar";
      $scope.filled_radar_chart_config.polar = [
         {
             indicator : [
                 { text: '销售（sales）', max: 6000},
                 { text: '管理（Administration）', max: 16000},
                 { text: '信息技术（Information Techology）', max: 30000},
                 { text: '客服（Customer Support）', max: 38000},
                 { text: '研发（Development）', max: 52000},
                 { text: '市场（Marketing）', max: 25000}
              ]
          }
      ];
      $scope.filled_radar_chart_data = [arr_four]; //CHART WITH SINGLE DATA
    }

    $scope.funnel_chart = function(){
      var config = {"data":[{"name":"SALARY (current_database)","databaseName":"current_database","datapoints":[{"x":"INPUT_ADJ","y":225000},{"x":"ICJOURNAL","y":220834},{"x":"COPR_ADJ","y":216666},{"x":"MGMT_COGS_ADJ","y":229166}]},{"name":"SALARY (forcasting_database)","databaseName":"forcasting_database","datapoints":[{"x":"INPUT_ADJ","y":230000},{"x":"ICJOURNAL","y":182000},{"x":"COPR_ADJ","y":201666},{"x":"MGMT_COGS_ADJ","y":278000}]}]}
      angular.forEach(config.data,function(value,key){
        var total = 0;
        angular.forEach(value.datapoints,function(datapoint,index){
          total += datapoint.y;
        })
        value.total = total;
      })
      angular.forEach(config.data,function(value,key){
        value.width = "50%";
        if(key > 0){
          value.x = "50%";
        }else{
          value.x = "0%";
        }
        angular.forEach(value.datapoints,function(datapoint,index){
          datapoint.y = datapoint.y/value.total*100;
        })
      })
      $scope.funnel_chart_config = angular.copy(chart_config);
      $scope.funnel_chart_config.title = "Funnel Chart";
      $scope.funnel_chart_config.subtitle = "Funnel Chart";
      $scope.funnel_chart_config.showLegend = true;
      $scope.funnel_chart_config.calculable = true;
      $scope.funnel_chart_data = config.data; //CHART WITH SINGLE DATA
    }
    $scope.basic_bar_chart = function(){
      $scope.basic_chart_config = angular.copy(chart_config);
      $scope.basic_chart_config.title = "Bar Chart";
      $scope.basic_chart_config.subtitle = "Bar Chart";
      $scope.basic_chart_config.reverseAxis = true;
      $scope.basic_chart_data = [arr_one]; //CHART WITH SINGLE DATA
    }
    $scope.treemap_chart = function(){
      $scope.treemap_chart_config = angular.copy(chart_config);
      $scope.treemap_chart_config.title = "TreeMap Chart";
      $scope.treemap_chart_config.subtitle = "TreeMap Chart";
      $scope.treemap_chart_data = [arr_two]; //CHART WITH SINGLE DATA
    }
    $scope.tree_chart = function(){
      $scope.tree_chart_config = angular.copy(chart_config);
      $scope.tree_chart_config.title = "Tree Chart";
      $scope.tree_chart_config.subtitle = "Tree Chart";
      $scope.tree_chart_data = [arr_five];
    }
    $scope.thermometer_chart = function(){
      $scope.thermometer_chart_config = angular.copy(chart_config);
      $scope.thermometer_chart_config.title = "Thermometer Chart";
      $scope.thermometer_chart_config.subtitle = "Thermometer Chart";
      $scope.thermometer_chart_config.support_type = 'thermometer';
      $scope.thermometer_chart_data = [arr_one,arr_two];
    }
    $scope.rainbow_chart = function(){
      $scope.rainbow_chart_config = angular.copy(chart_config);
      $scope.rainbow_chart_config.title = "Rainbow Chart";
      $scope.rainbow_chart_config.subtitle = "Rainbow Chart";
      $scope.rainbow_chart_config.support_type = 'rainbow';
      $scope.rainbow_chart_data = [arr_one,arr_two];
    }
    $scope.multilevel_chart = function(){
      $scope.multilevel_chart_config = angular.copy(chart_config);
      $scope.multilevel_chart_config.title = "Multilevel Line Chart";
      $scope.multilevel_chart_config.subtitle = "Multilevel Line Chart";
      $scope.multilevel_chart_config.support_type = 'multilevel';
      $scope.multilevel_chart_data = [arr_one,arr_two];
    }
    $scope.force_chart = function(){
      $scope.force_chart_config = angular.copy(chart_config);
      $scope.force_chart_config.title = "Force-Directed Charts";
      $scope.force_chart_config.subtitle = "Force-Directed Charts";
      /*var conf_data = [];
      angular.forEach(arr_six.data,function(value,key){
        angular.forEach(value.datapoints,function(datapoint,index){
          conf_data.push({
            fieldName:value.fieldName,
            databaseName:value.databaseName,
            name:value.name,
            x:datapoint.x,
            y:datapoint.y         
          });
        });
      });
      var nodes = [{ name:conf_data[0].fieldName, value:777, label:conf_data[0].fieldName }];
      var links = [];
      angular.forEach(arr_six.data,function(value,key){
        nodes.push({ name:value.name, value:'' });
        links.push({ source:value.name, target:conf_data[0].fieldName });
        angular.forEach(conf_data,function(conf,index){
          if(conf.name == value.name){
            nodes.push({ name:conf.x+'('+conf.databaseName+')', value:conf.y });
            links.push({ source:conf.x+'('+conf.databaseName+')', target:value.name});
          }
        });
      });*/
      $scope.force_chart_data = [force_data];
    }
    $scope.reverse_area_chart = function(){
      $scope.reverse_area_chart_config = angular.copy(chart_config);
      $scope.reverse_area_chart_config.title = "Reverse Area Chart";
      $scope.reverse_area_chart_config.subtitle = "Reverse Area Chart";
      $scope.reverse_area_chart_config.support_type = 'reverse_area';
      $scope.reverse_area_chart_data = [arr_one,arr_two];
    }
});

