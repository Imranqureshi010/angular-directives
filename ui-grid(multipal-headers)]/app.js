var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.grouping', 'ui.grid.selection', 'ui.grid.resizeColumns']);

app.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
  var main = this;
  $scope.getCategoryWidth = function (columns, category, type) {
    var dw = 0;
    for (var i = 0; i < columns.length; i++) {
        if (columns[i].colDef[type] == category) {
            dw += columns[i].drawnWidth;
        }
    }
    var width = dw.toString() + "px";
    var res = {
        "max-width": width,
        "min-width": width
    }
    return res;
  }
  main.gridOptions = {
    treeRowHeaderAlwaysVisible: true,
    headerTemplate: 'header-template.html',
    categorp: [
      {name: 'column1',displayName: 'Column 1', visible: true,showCatName: false},
      {name: 'column2',displayName: 'Column 2', visible: true,showCatName: false},
      {name: 'People',displayName: 'Peopleeeeeeeeeeeeeeeeeeeeeeeee', visible: true,showCatName: true},
      {name: 'Year',displayName: 'Year', visible: true,showCatName: true},
      {name: 'Extra1',displayName: 'Extra1', visible: true,showCatName: true},
      {name: 'Extra2',displayName: 'Extra2', visible: true,showCatName: true}
      ],
    subcategory: [
      { name: 'none1', visible: true, showCatName: false },
      { name: 'none2', visible: true, showCatName: false },
      { name: 'sub1', displayName: 'Sub1', visible: true, showCatName: true },
      { name: 'sub2', displayName: 'Sub2', visible: true, showCatName: true },
      { name: 'sub3', displayName: 'Sub3', visible: true, showCatName: true },
      { name: 'none3', visible: true, showCatName: false },
      
    ],
    enableGridMenu: true,
    enableSorting: false,
    multiSelect: true,
    columnDefs: [
      {name:'Col 1', category:"column1", subcategory: "none1", width: 100, enableColumnMenu: false},
      {name:'Col 2', category:"column2", subcategory: "none2", width: 100, enableColumnMenu: false},
      {name: 'gender', category:'People', subcategory: "sub1", width: 100, enableColumnMenu: false},
      {name:'name', category:"People", subcategory: "sub1", width: 100, enableColumnMenu: false},
      {name:'title', category:"People", subcategory: "sub2", width: 100, enableColumnMenu: false},
      {name:'age', category:'Year', subcategory: "sub3", width: 50, enableColumnMenu: false},
      {name:'annee', category:'Year', subcategory: "none3", width: 50, enableColumnMenu: false},
      
      {name:'Ex1', category:'Extra1', width: 75, enableColumnMenu: false},
      {name:'Ex2', category:'Extra1', width: 75, enableColumnMenu: false},
      {name:'Ex3', category:'Extra2', width: 75, enableColumnMenu: false}
      ],
    data: [
      { column1: 'im colunm 1', column2: 'im colunm 2', gender: 'Male', name: 'Bob', title: 'CEO', age : '14', annee:'1' },
      { column1: 'im colunm 1', column2: 'im colunm 2', gender: 'Male', name: 'Bob', title: 'CEO', age : '14', annee:'1' },
      { column1: 'im colunm 1', column2: 'im colunm 2', gender: 'Male', name: 'Frank', title: 'Lowly Developer', age: '16', annee:'2' },
      { column1: 'im colunm 1', column2: 'im colunm 2', gender: 'Female', name: 'Catherine', title: 'Developer', age: '27', annee:'2' },
      { column1: 'im colunm 1', column2: 'im colunm 2', gender: 'Female', name: 'Jessica', title: 'Engineer', age: '35', annee:'3' }
    ],
    onRegisterApi: function( gridApi ) {
      main.gridApi = gridApi;
    }
  };
  
}]);
