vis_engine.service('menuService', function(){
  this.get_menus= function(){
    /*var menus = [
      {
        menu: 'Projects',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Projects',
        url:'/projects/',
        icon:'fa fa-file-o',
      },
      {
        menu: 'Data Sources',
        icon:'fa fa-database',
        view: 0,
        submenu: [
          {
            functname: 'Datasource Config',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Datasource Config',
            url:'/database_config/',
            icon:'fa fa-database'
          },
          {
            functname: 'Data Designer',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Database Config',
            url:'/database_config/data_designer',
            icon:'fa fa-database'
          },
          {
            functname: 'Data Source',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Data Source',
            url:'/database_config/data_sources',
            icon:'fa fa-database'
          },
        ]
      },
      {
        menu: 'Access Policies',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Access Policies',
        url:'/access_policies/',
        icon:'fa fa-check-square-o',
      },
      {
        menu: 'Users',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Users',
        url:'/users/',
        icon:'fa fa-users',
      },
      {
        menu: 'Settings',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Settings',
        url:'javascript:void(0);',
        icon:'fa fa-cogs',
      },
      {
        menu: 'Audit log',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Audit',
        url:'/audit/',
        icon:'fa fa-search',
      },
      /*{
        menu: 'Charts',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Datasets',
        url:'/datasets/',
        icon:'fa fa-line-chart',
      },
      {
        menu: 'Layout Setup',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Layout Setup',
        url:'/layout_setup/',
        icon:'fa fa-laptop',
      },
    ];*/
    
    var menus = [
      {
        menu: 'Data Import',
        icon:'fa fa-download',
        view: 0,
        submenu: [
          {
            functname: 'Data Configure',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Database Config',
            url:'/database_config/data_designer',
            icon:'fa fa-database'
          },
          {
            functname: 'Data Source',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Data Source',
            url:'/database_config/data_sources',
            icon:'fa fa-database'
          },
        ]
      },
      {
        menu: 'Projects',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Projects',
        url:'/projects/',
        icon:'fa fa-file-o',
      },
      {
        menu: 'Dashboard',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Dashboard',
        url:'/dashboard/dashboard/',
        icon:'fa fa-dashboard'
      },
      {
        menu: 'Report',
        view: 0,
        add: 0,
        edit: 0,
        'delete': 0,
        suspend:0,
        pagename:'Report',
        url:'/dashboard/report/',
        icon:'fa fa-file-o',
      },
      {
        menu: 'Settings',
        icon:'fa fa-cogs',
        view: 0,
        submenu: [
          {
            functname: 'Users',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Users',
            url:'/users/',
            icon:'fa fa-users',
          },
          {
            functname: 'Audit log',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Audit',
            url:'/audit/',
            icon:'fa fa-search',
          },
          {
            functname: 'Access Policies',
            view: 0,
            add: 0,
            edit: 0,
            'delete': 0,
            suspend:0,
            pagename:'Access Policies',
            url:'/access_policies/',
            icon:'fa fa-check-square-o',
          },
         
        ]
      },
    ];
    menus.map(function(menu){ 
      // console.log('menu is :',menu);
      if(menu.submenu){
        menu.submenu.map(function(submenu){
        submenu.active = false;
          if(submenu.pagename == pagetype){
            submenu.active = true;
            menu.active = true;
          }
        });
      }else{
        menu.active = false;
        if(menu.pagename == pagetype){
          menu.active = true;
        }
      }
    });
    return menus;
  }
});
