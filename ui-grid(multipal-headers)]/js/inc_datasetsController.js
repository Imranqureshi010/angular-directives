vis_engine.controller('DatasetsCtrl', function ($scope,$rootScope,$http,echartService,$timeout) {
  $scope.page_name = page;
  // CHART TYPES ARRAY
  $scope.chart_types = [
    { // Bar Chart
      type:"bar", //use the same name that is there in the library
      label:"",  //this is title shown on hover
      desc:"",
      data_url:'/datasets/api_get_chart_data', //where will all this data be sent
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "X Axis",
              placeholder: "Add Measure",
              max_columns:10,  //Max columns will be one. The + icon will hide when one column is added.
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Y Axis",
              placeholder: "Add Dimensions",
              max_columns:10,
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Line Chart
      type:"line",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Y Axis",
              placeholder: "Add Measure",
              max_columns:10,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "X Axis",
              placeholder: "Add Dimensions",
              max_columns:10,
              column_type: "dimension",
              values:[],
            },
            /*{
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },*/
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Area Chart
      type:"area",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Y Axis",
              placeholder: "Add Measure",
              max_columns:10,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "X Axis",
              placeholder: "Add Dimensions",
              max_columns:10,
              column_type: "dimension",
              values:[],
            },
            /*{
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },*/
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Pie Chart
      type:"pie",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Size",
              placeholder: "Add Measure",
              max_columns:10,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Donut Chart
      type:"donut",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Size",
              placeholder: "Add Measure",
              max_columns:1,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Gauge Chart
      type:"gauge",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Size",
              placeholder: "Add Measure",
              max_columns:1,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Scatter Chart
      type:"scatter", //use the same name that is there in the library
      label:"",  //this is title shown on hover
      desc:"",
      data_url:'/datasets/api_get_chart_data', //where will all this data be sent
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "X Axis",
              placeholder: "Add Measure",
              max_columns:10,  //Max columns will be one. The + icon will hide when one column is added.
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Y Axis",
              placeholder: "Add Dimensions",
              max_columns:10,
              column_type: "dimension",
              values:[],
            },
            /*{
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },*/
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Lasagna Pie Chart
      type:"lasagna",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Size",
              placeholder: "Add Measure",
              max_columns:10,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
    { // Nested Pie Chart
      type:"nested",
      label:"",
      desc:"",
      data_url:'/datasets/api_get_chart_data',
      fields:[
        {
          label:"MEASURES",
          data:[
            {
              label: "Size",
              placeholder: "Add Measure",
              max_columns:10,
              column_type: "measure",
              values:[],
            },
          ]
        },
        {
          label:"DIMENSIONS",
          data:[
            {
              label: "Color",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        },
        {
          label:"TRELLIS",
          data:[
            {
              label: "Rows",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
            {
              label: "Columns",
              placeholder: "Add Dimensions",
              max_columns: 10, //If max columns is null or not available, then unlimited columns can be added.
              column_type: "dimension",
              values:[],
            },
          ]
        }
      ]
    },
  ]
  // CALL THIS FUNCTION ON PAGE LOAD AND RESET THE DATA
  $scope.initialize_data = function(){
    $scope.field_show = {'display':'none'};
    $scope.working_chart_types = [];
    angular.copy($scope.chart_types,$scope.working_chart_types);
    $scope.selected_charts = [];
    $scope.selected_charts.push($scope.working_chart_types[0])
    $scope.current_chart = $scope.selected_charts[0];
    $scope.show_fields($scope.working_chart_types[0]);
    // BLANK TEMPLATE FOR MEASURE/DIMENSION POPOVER
    $scope.dynamicPopover = {
      templateUrl: 'template.html',
      content:[],
      isOpen: true
    };
    // BLANK TEMPLATE FOR EDIT/DELETE POPOVER
    $scope.chartPopover = {
      templateUrl: 'chart.html',
      title:"Actions",
      content:[],
      isOpen: true
    };

    // BLANK TEMPLATE FOR FILTER-DIMENSION POPOVER
    $scope.filterPopover = {
      templateUrl: 'filter_popover.html',
      title:"Dimensions",
      content:[],
      isOpen: true
    };

    for (var i = 0; i < $rootScope.measures_items.length; i++) {
      $rootScope.measures_items[i].added = false;
    };
    for (var i = 0; i < $scope.dimensions_items.length; i++) {
      $scope.dimensions_items[i].added = false;
    };
  }
  // FUNCTION TO GET DATA FOR CHART FROM SERVER
  $scope.chartDataLoading = false;
  $scope.get_chart_data = function(){
    $scope.chartDataLoading = true;
    if($scope.page_name == 'Datasets'){
      var get_chart_from = "localhost";
    }else{
      var get_chart_from = "java_server";
      $scope.table = {table_name:"txn_master"};
      $scope.database_config = {database_name:'test_database'};
    }
    $scope.chart = "";
    if(!$scope.database_config || !$scope.table){
      // alert("Please select a database and table first to get Measure & Dimensions");
      return false;
    }
    var post_data = {
      table_name:$scope.table.table_name,
      database_obj:$scope.database_config,
      chart_obj:$scope.current_chart,
      get_chart_from:get_chart_from
    }
    $http({method:'POST',url:$scope.current_chart.data_url,data:post_data})
    .success(function(data){
      console.log("Getting data from server : ",data);
      if(data['data']){
        var type = $scope.current_chart.type;
        var arr = [];
        for (var i = 0; i < data['data'].length; i++) {
          arr.push({datapoints:data['data'][i].data})
        };
        var config = {
          data:arr
        }
        switch(type){
          case 'pie':
            var chart = echartService.piechart(config);
            break;
          case 'donut':
            var chart = echartService.donutchart(config);
            break;
          case 'gauge':
            var chart = echartService.gaugechart(config);
            break;
          case 'bar':
            var chart = echartService.barchart(config);
            break;
          case 'line':
            var chart = echartService.linechart(config);
            break;
          case 'area':
            var chart = echartService.areachart(config);
            break;
          case 'scatter':
            var chart = echartService.scatterchart(config);
            break;
          case 'lasagna':
            var chart = echartService.lasagna_pie_chart(config);
            type = 'pie';
            break;
          case 'nested':
            var chart = echartService.nested_pie_chart(config);
            type = 'pie';
            break;
        }
        $scope.chart_config = chart;
        $scope.chart_config.type = type;
        $scope.chartDataLoading = false;
        $scope.chart = '<'+type+'-chart config="chart_config.options" data="chart_config.data"></'+type+'-chart>'
      }
    })
    .error(function(data){
        console.log("There is some error to get data for charts from server")
    })
  }
  // FUNCTION TO SHOW FIELDS RELATED TO SELECTED CHART ON RIGHT SIDEBAR
  $scope.show_fields = function(chart_obj){
    var found = $.grep($scope.selected_charts, function(e){ return !e.completed; });
    var index = $scope.selected_charts.indexOf(found[0]);
    $scope.selected_charts[index] = chart_obj;
    var label = $scope.current_chart.label;
    var desc = $scope.current_chart.desc;
    var fields = $scope.current_chart.fields;
    var filters = $scope.current_chart.filters;
    $scope.current_chart = $scope.selected_charts[index];
    $scope.current_chart.label = label;
    $scope.current_chart.desc = desc;
    $scope.current_chart.filters = filters;
    for (var i = 0; i < fields.length; i++) {
      for (var j = 0; j < fields[i].data.length; j++) {
        if($scope.current_chart.fields[i].data[j]){
          $scope.current_chart.fields[i].data[j].values = fields[i].data[j].values;
        }
      };
    };
    // $scope.get_chart_data();
  }
  // FUNCTION TO SHOW MEASURE/DIMENSION POPOVER
	$scope.show_popover = function($event,row,label){
    $scope.dynamicPopover = {
      templateUrl: 'template.html',
      content:[],
      isOpen: true
    };
    $event.preventDefault();
    if(row.column_type == 'measure'){
      angular.copy($rootScope.measures_items,$scope.dynamicPopover.content);
      $scope.dynamicPopover.title = label;
    }else{
      angular.copy($scope.dimensions_items,$scope.dynamicPopover.content);
      $scope.dynamicPopover.title = label;
    }
    for (var i = 0; i < $scope.dynamicPopover.content.length; i++) {
      var found = $.grep(row.values, function(e){ return e.field_name == $scope.dynamicPopover.content[i].field_name; });
      if(found.length){
        $scope.dynamicPopover.content[i].added = true;
      }
    };
	}
  // FUNCTION TO CLOSE POPOVER
  $scope.close_popover = function($event){
    angular.element(document.body).bind('click', function (e) {
      var popups = document.querySelectorAll('.popover');
      if (popups) {
        for (var i = 0; i < popups.length; i++) {
          var popup = popups[i];
          var popupElement = angular.element(popup);
          console.log(2);
          if (popupElement[0].previousSibling != e.target) {
              popupElement.scope().$parent.isOpen = false;
              // popupElement.scope().$parent.$apply();
          }
        }
      }
    });
  }
  // FUNCTION TO SHOW FILTER-DIMENSION POPOVER
  $scope.allDimensionsAdded = false;
  $scope.show_filter_popover = function($event){
    $event.preventDefault();
    var found = $.grep($scope.dimensions_items, function(e){ return e.selected == true; });
    if(found.length == $scope.dimensions_items.length){
      $scope.allDimensionsAdded = true;
    }
    angular.copy($scope.dimensions_items,$scope.filterPopover.content);
  }
  // ADD COLUMNS IN SELECTED CHARTS FIELD LIKE 'x-axis,y-axis,color etc'.
	$scope.add_columns = function(column,row){
    angular.element(document.body).bind('click', function (e) {
      var popups = document.querySelectorAll('.popover');
      if (popups) {
        for (var i = 0; i < popups.length; i++) {
          var popup = popups[i];
          var popupElement = angular.element(popup);
          if (popupElement[0].previousSibling != e.target) {
            console.log("popupElement.scope().$parent",popupElement.scope().$parent)
              popupElement.scope().$parent.isOpen = false;
              popupElement.scope().$parent.content = "";
              popupElement.scope().$parent.title = "";
              // popupElement.scope().$parent.$apply();
          }
        }
      }
    });
    row.values.push(column);
    $scope.get_chart_data();
	}

  $scope.add_columns_dashboard = function(column,label,axis){
    console.log('$scope.current_chart.fields is :-==================',$scope.current_chart.fields);
    // if($scope.current_chart.fields[0]['data'][0]['values'].length && $scope.current_chart.fields[1]['data'][0]['values'].length){
      var field_data = $.grep($scope.current_chart.fields, function(field){ return field.label == label; });
      if(field_data.length){
        field_data = field_data[0].data;
        var data = $.grep(field_data, function(series){ return series.label == axis; });
        if(data.length){
          var found_obj = $.grep(data[0].values, function(e){ return e.field_name == column.field_name; });
          if(found_obj.length){
            var index = data[0].values.indexOf(found_obj[0]);
            data[0].values.splice(index,1);
            column.added = false;
          }else{
            data[0].values.push(column);
          }
          if(label == 'MEASURES'){
            for (var i = 0; i < $rootScope.measures_items.length; i++) {
              var found = $.grep(data[0].values, function(e){ return e.field_name == $rootScope.measures_items[i].field_name; });
              if(found.length){
                $rootScope.measures_items[i].added = true;
              }
            };
          }else{
            for (var i = 0; i < $scope.dimensions_items.length; i++) {
              var found = $.grep(data[0].values, function(e){ return e.field_name == $scope.dimensions_items[i].field_name; });
              if(found.length){
                $scope.dimensions_items[i].added = true;
              }
            };
          }
        }
      }
      $scope.get_chart_data();
    // }
  }
  // REMOVE COLUMN FROM THE FIELDS OF SELECTED CHART LIKE 'x-axis,y-axis,color etc'.
	$scope.remove_column = function(selected_column,row){
		for (var i = 0; i < row.values.length; i++) {
			if(row.values[i].field_name == selected_column.field_name){
				row.values.splice(i, 1);
			}
		};
    $scope.get_chart_data();
	}
  // ADD NEW TEMPLATE TO MAKE ANOTHER NEW CHART
  $scope.add_new_chart_template = function(){
    $scope.current_chart.completed = true;
    $scope.current_chart.chart_config = $scope.chart_config;
    $scope.chart = "";
    angular.copy($scope.chart_types,$scope.working_chart_types);
    $scope.selected_charts.push($scope.working_chart_types[0]);
    $scope.current_chart = $scope.selected_charts[$scope.selected_charts.length-1];
    $scope.current_chart.label = "";
    $scope.current_chart.desc = "";
    for (var i = 0; i < $scope.current_chart.fields.length; i++) {
      for (var j = 0; j < $scope.current_chart.fields[i].data.length; j++) {
        $scope.current_chart.fields[i].data[j].values = [];
      };
    };
  }
  // DELETE SELECTED CHART
  $scope.delete_chart = function(){
    var index = $scope.selected_charts.indexOf($scope.current_chart);
    $scope.selected_charts.splice(index,1);
  }
  // FUNCTION TO SHOW THE CHART THAT CREATED BY USER  
  $scope.show_user_selected_chart = function($event,chart){
    $scope.current_chart.completed = true;
    $scope.current_chart.chart_config = $scope.chart_config;
    $scope.current_chart = chart;
    $scope.current_chart.completed = false;
    $scope.get_chart_data();
  }
  // SAVE USER CREATED CHARTS IN DATABASE
  $scope.save_dataset = function(){
    $scope.current_chart.completed = true;
    $scope.current_chart.chart_config = $scope.chart_config;
    $scope.new_dataset.json_object = $scope.selected_charts;
    $http({method:'POST',url:'/datasets/api_save/',data:$scope.new_dataset})
    .success(function(data){
      // console.log("Saved user charts",data)
      if(data.status == 200){
        location.href = "/layout_setup/index/"+data["data"].id;
      }else{
        alert("There is some error in save charts");
      }
    })
    .error(function(data){
      alert("There is some error in save charts");
    });  
  }
  // GET ALL DATASOURCES LIST
  $scope.database_config_list = [];
  $scope.get_database_config_list = function(){
    $http.get('/database_config/api_database_config_list/').success(function(data){
      if(data.status == 200){
        angular.copy(angular.fromJson(data.data),$scope.database_config_list);
        // $scope.database_config = $scope.database_config_list[0];
        // $scope.get_tables_list($scope.database_config);
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
  }
  // GET ALL TABLES OF SELECTED DATASOURCE
  $scope.table_list = [];
  $scope.get_tables_list = function(database){
    $http({
        method: 'POST',
        url: '/database_config/api_get_tables',
        data: {
            id:database.id,
        }
    })
    .success(function(resp) {
        if(resp.status==200){
          angular.copy(angular.fromJson(resp['data']),$scope.table_list);
          // $scope.table = $scope.table_list[0];
          // $scope.get_columns_list($scope.table);
        }else{
          alert(resp.msg);
        }
    })
    .error(function(err) {
        console.log('Got an error', err)
        alert(err)
    });
  }
  // GET ALL COLUMNS (MEASURES/DIMENSIONS) OF SELECTED TABLE
  // $rootScope.measures_items = [];
  // $scope.dimensions_items = [];
  $scope.get_columns_list = function(table){
    $scope.initialize_data();
    $rootScope.measures_items = [];
    $scope.dimensions_items = [];
    $http({
      method: 'POST',
      url: '/database_config/api_get_columns',
      data: {
        table_name:table.table_name,
        database_id:$scope.database_config.id,
      }
    })
    .success(function(resp) {
      if(resp.status==200){
        for (var i = 0; i < resp["data"].length; i++) {
          if(resp["data"][i].field_type == "measure"){
            $rootScope.measures_items.push(resp["data"][i]);
          }else{
            $scope.dimensions_items.push(resp["data"][i]);
          }
        };
        $scope.field_show = {'display':'block'};
      }else{
        alert(resp.msg);
      }
    })
    .error(function(err) {
      console.log('Got an error', err)
        alert(err)
    });
  }
  // SHOW FILTER POPUP
  $scope.attributes = [];
  $scope.current_filter = {};
  $scope.show_filter_config = function($event,column){
    $event.preventDefault();
    $scope.current_filter = {};
    $scope.current_filter = column;
    if(!$scope.current_filter.attributes){
      $scope.current_filter.condition = "IN";
      $scope.current_filter.attributes = [];
    }
    var post_data = {
      db_config:$scope.database_config,
      table_name:$scope.table.table_name,
      column_name:$scope.current_filter.field_name,
    }
    $http({method:"POST",url:"/database_config/api_get_attributes/",data:post_data})
    .success(function(data){
      // console.log("Getting attributes : ",data);
      angular.copy(angular.fromJson(data['data']),$scope.attributes);
        // console.log("is_selected : ",$scope.current_filter.attributes)
      if($scope.current_filter.attributes.length){
        for (var i = 0; i < $scope.attributes.length; i++) {
          var is_selected = $.grep($scope.current_filter.attributes, function(attr){ return attr.attribute_value == $scope.attributes[i].attribute_value; });
          // console.log("is_selected : ",is_selected)
          if(is_selected.length){
            $scope.attributes[i].selected = true;
          }
        };
      }
      $("#filter-modal").modal("show");
    })
    .error(function(data){
      console.log("Getting error to get attributes : ",data);
    })
  }
  // SELECT COLUMNS FOR FILTER
  $scope.select_attribute = function(attribute){
    var index =  $scope.current_filter.attributes.indexOf(attribute);
    if(index >= 0){
      attribute.selected = false;
      $scope.current_filter.attributes.splice(index,1);
    }else{
      attribute.selected = true;
      $scope.current_filter.attributes.push(attribute);
    }
  }
  // REMOVE COLUMS FROM FILTER
  $scope.remove_filter = function($event,filter){
    $event.preventDefault();
    $scope.current_filter = {};
    $scope.allDimensionsAdded = false;
    for (var i = 0; i < $scope.current_chart.filters.length; i++) {
      if($scope.current_chart.filters[i] === filter){
        $scope.current_chart.filters.splice(i,1);
        break;
      }
    };
    if(filter && $scope.dimensions_items){
      for (var i = $scope.dimensions_items.length - 1; i >= 0; i--) {
        if($scope.dimensions_items[i].field_name == filter.field_name){
          $scope.dimensions_items[i].selected = false;
        }
      };
    }
    $scope.get_chart_data();
  }
  // RESET FILTER OBJECT
  $scope.reset_current_filter_attr = function(){
    $scope.current_filter.attributes = [];
    for (var i = 0; i < $scope.attributes.length; i++) {
      $scope.attributes[i].selected = false;
    };
  }
  // APPLY FILTERS IN CURRENT SELECTED CHART
  $scope.apply_filters = function(){
    if(!$scope.current_chart.filters){
      $scope.current_chart.filters = [];
    }
    $scope.current_chart.filters.push($scope.current_filter);
    if($scope.current_filter && $scope.dimensions_items){
      for (var i = $scope.dimensions_items.length - 1; i >= 0; i--) {
        if($scope.dimensions_items[i].field_name == $scope.current_filter.field_name){
          $scope.dimensions_items[i].selected = true;
        }
      };
    }
    $scope.get_chart_data();
    $("#filter-modal").modal("hide");
  }
  // EDIT SELECTED FILTER
  $scope.edit_filter = function(filter){
    $scope.current_filter = filter;
     $("#filter-modal").modal("show");
  }
  // SAVE CHARTS FROM POPUP.
  $scope.save_charts = function(){
    if($rootScope.current_selected_div.chart_data){
      $scope.update_dashboard(null,$rootScope.current_selected_div);
      $rootScope.current_selected_div.chart_data.chart_config = $scope.chart_config;
      $rootScope.current_selected_div.chart_data.chart_config.options.height=$rootScope.current_selected_div.height-20;//This height should be same as Widget height
      $rootScope.current_selected_div.chart_data.chart_config.options.width=$rootScope.current_selected_div.width; //This width should be same as Widget width
      $rootScope.current_selected_div.chart_data.chart_config.options.showLegend = false;
      $rootScope.current_selected_div.chart_data.chart_config.options.noToolbox = true;
      $rootScope.current_selected_div.chart = '<'+$rootScope.current_selected_div.chart_data.chart_config.type+'-chart config="div.chart_data.chart_config.options" data="div.chart_data.chart_config.data"></'+$rootScope.current_selected_div.chart_data.chart_config.type+'-chart>';

      $rootScope.current_selected_div.select_chart_type = $scope.select_chart_type;
      $rootScope.current_selected_div.measures = [];
      $rootScope.current_selected_div.dimensions = [];
      if($scope.current_chart.fields[0]){
        for (var i = 0; i < $scope.current_chart.fields[0]['data'][0]['values'].length; i++) {
          $rootScope.current_selected_div.measures.push($scope.current_chart.fields[0]['data'][0]['values'][i].fieldName)
        }
      }
      if($scope.current_chart.fields[1]){
        for (var i = 0; i < $scope.current_chart.fields[1]['data'][0]['values'].length; i++) {
          $rootScope.current_selected_div.dimensions.push($scope.current_chart.fields[1]['data'][0]['values'][i].fieldName)
        }
      }
      $rootScope.current_selected_div.size = { x: 3, y: 2 };
      $rootScope.current_selected_div.sizeX = 6;
      $rootScope.current_selected_div.sizey = 2;
      $rootScope.current_selected_div.row = $rootScope.current_selected_div.row+1;
    }
    $scope.initialize_data();
    $("#add-charts-modal").modal("hide");
  }
  
  // ----------------- START CODE FOR EDIT TEMPLATES & CHARTS --------------------------------------//
  $('#add-charts-modal').on('shown.bs.modal', function () {
    if($rootScope.current_selected_div.measures.length || $rootScope.current_selected_div.dimensions.length){
      $scope.chartDataLoading = true;
      $scope.get_last_node_table_preview();
      $timeout(function() {
        $scope.get_chart_data_for_edit_chart();
        $scope.chartDataLoading = false;
      }, 10000);
    }
  })
  $scope.get_chart_data_for_edit_chart = function(){
    if($rootScope.current_selected_div.measures && $rootScope.current_selected_div.measures.length){
      for (var i = 0; i < $rootScope.current_selected_div.measures.length; i++) {
        var found = $.grep($rootScope.measures_items,function(e){ return e.field_name == $rootScope.current_selected_div.measures[i] });
        console.log("found measures ",found[0]);
        $scope.add_columns_dashboard(found[0],"MEASURES","X Axis");
      }
    }
    if($rootScope.current_selected_div.dimensions && $rootScope.current_selected_div.dimensions.length){
      for (var i = 0; i < $rootScope.current_selected_div.dimensions.length; i++) {
        var found = $.grep($rootScope.dimensions_items,function(e){ return e.field_name == $rootScope.current_selected_div.dimensions[i] });
        console.log("found dimensions ",found[0]);
        $scope.add_columns_dashboard(found[0],"DIMENSIONS","Y Axis");
      }
    }
  }
 /* $('#add-charts-modal').on('shown.bs.modal', function () {
    if($rootScope.current_selected_div.measures.length || $rootScope.current_selected_div.dimensions.length){
      $scope.get_last_node_table_preview();
    }
  })
  $scope.$watch('measures_items', function(newVal) {
    if(newVal.length && $rootScope.current_selected_div.measures && $rootScope.current_selected_div.measures.length){
      for (var i = 0; i < $rootScope.current_selected_div.measures.length; i++) {
        var found = $.grep($rootScope.measures_items,function(e){ return e.field_name == $rootScope.current_selected_div.measures[i] });
        console.log("found measures ",found[0]);
        $scope.add_columns_dashboard(found[0],"MEASURES","X Axis");
      }
    }
  }, true);

  $scope.$watch('dimensions_items', function(newVal) {
    if(newVal.length && $rootScope.current_selected_div.dimensions && $rootScope.current_selected_div.dimensions.length){
      for (var i = 0; i < $rootScope.current_selected_div.dimensions.length; i++) {
        var found = $.grep($rootScope.dimensions_items,function(e){ return e.field_name == $rootScope.current_selected_div.dimensions[i] });
        console.log("found dimensions ",found[0]);
        $scope.add_columns_dashboard(found[0],"DIMENSIONS","Y Axis");
      }
    }
  }, true);*/
  // ----------------- ENDS CODE FOR EDIT TEMPLATES & CHARTS --------------------------------------// 
});