vis_engine.controller('Database_configCtrl',function ($scope,$rootScope,$http,node_paramService,$timeout){
  var first_column_object = {
    "name":"columns",
    "enableCellEdit": true,
    "cellEditableCondition": true,
    "editableCellTemplate": "ui-grid/cellEditor",
    "enableCellEditOnFocus": false,
    "enableColumnMoving": true,
    "enablePinning": true,
    "enableColumnResizing": true,
    "allowCellFocus": true,
    "type": "string"    
  }
  $scope.database_list=[];
  $scope.db_types= node_paramService.get_database_types();
  $scope.operations = node_paramService.get_operation_types();
  /*$rootScope.gridOptions = {
    showGridFooter: true,
    showColumnFooter: true,
    enableFiltering: true,
    exporterMenuCsv: false,
    enableGridMenu: true,
    onRegisterApi: function(gridApi) {
      $rootScope.gridApi = gridApi;
    }
  };*/
  $rootScope.gridOptions.data = [];
  $rootScope.gridOptions.columnDefs = [first_column_object]
  // if(!$scope.database_config_connection){
    $scope.database_config_connection = 'database_config';
  // }

  // SET DATABASE SERVER CONNECTION 
  $scope.set_database_server_connection = function(connection_ctrl){
    if(connection_ctrl){
      $scope.database_config_connection = connection_ctrl;
    }
  }

  $scope.all_data_source = [];
  $scope.get_data_source_list = function(){
    $http.get('/database_config/api_get_data_sources_list').success(function(data){
      if(data.status == 200){
        $scope.all_data_source = data.data;
        if($scope.all_data_source.length){
          $scope.get_datasource_json($scope.all_data_source[0].id);
        }
      }else{
        $scope.err_msg = "There is some error to get all data from server"+data.data;
      }
    })
  }
  $scope.chart = "";
  $scope.pivot_table_list = [];
  $scope.get_chart_data_by_selected_chart_type = function(selected_chart_type){
    $scope.select_chart_type = "";
    $scope.pivot_id = "";
    $scope.chart = "";
    $scope.current_chart = {};
    $rootScope.measures_items = [];
    $rootScope.dimensions_items = [];
    $rootScope.current_selected_div = {};
    $rootScope.current_selected_div.related_reports = [
      {
        report_id:"",
        report_name:''
      }
    ];
    $rootScope.current_selected_div.chart_data = {};
    if(selected_chart_type){
      if(selected_chart_type == 'pivot_table'){
        $scope.select_chart_type = "";
        $http.get("/database_config/api_get_chart_data_by_selected_chart_type/"+$scope.chartViewModel.data.id)
        .success(function(data){
          if(data.status == 200){
            angular.copy(data.data,$scope.pivot_table_list);
          }else{
            $scope.err_msg = "There is some error to get all pivot tables from server"+data.message;
          }
        });
      }else{
        $scope.pivot_id = "";
        $scope.get_last_node_table_preview();
      }
    }
  }
  $scope.get_pivot_json_for_display_chart = function(pivot_id){
    if(pivot_id){
      $rootScope.measures_items = [];
      $rootScope.dimensions_items = [];
      $http.get("/database_config/api_get_pivot_json_for_display_chart/"+pivot_id).success(function(data){
        if(data.status == 200){
          if(data.data.pivot_json){
            var pivot_data = JSON.parse(data.data.pivot_json);
            for (var i = pivot_data.columns.length - 1; i >= 0; i--) {
              $rootScope.measures_items.push({field_name:pivot_data.columns[i].columnName});
            }
            for (var i = pivot_data.rows.length - 1; i >= 0; i--) {
              $rootScope.dimensions_items.push({field_name:pivot_data.rows[i].columnName});
            }
          }
        }
      });
    }
  }
  $scope.get_datasource_json = function(dataSourceId){
    if(dataSourceId){
      $http.get('/datasets/api_get_datasource_json/'+dataSourceId).success(function(data){
        if(data.status == 200){
          var source_data = JSON.parse(data.data.json_obj);
          $scope.chartViewModel = {};
          $scope.chartViewModel.data = source_data;
          // $scope.get_last_node_table_preview();
        }
      });
    }
  }

  // -------------- DATABASE CONFIG SCREENS RELATED FUNCTION STARTS FROM HERE ---------- //
    $scope.database_config_list = [];
    $scope.new_database = {}
    $scope.new_database.db={
      host:"localhost",
      port:3306,
      user:"root",
      password:"",
    }
    $scope.new_database.database_type=$scope.db_types[0];

    $scope.show_verify_database = function(){
      if($scope.database_list.length === 0){
          $http({
              method: 'POST',
              url: '/database_config/api_database_list',
              data: {
                  db_object:$scope.new_database.db,
                  db_type:$scope.new_database.database_type.db_cd
              }
          })
          .success(function(resp) {
              if(resp.status==200){
                  angular.copy(angular.fromJson(resp['data']), $scope.database_list);
                  $("#verify-database-modal").modal("show");
              }else{
                  alert(resp.msg);
              }

          })
          .error(function(err) {
              alert(err)
          });
      }else{
          $("#verify-database-modal").modal("show");
      }
    }

    $scope.select_database = function(database){
      var is_db_selected = $.grep($scope.database_list, function(e){ return e.selected == true; });
      if(is_db_selected.length > 0){
          $scope.show_database = true;
      }else{
          $scope.show_database = false;
      }
    }

    $scope.reset_database_list = function(){
      $scope.show_database = false;
      $scope.database_list = [];
    }

    $scope.save_database=function($event){
      $event.preventDefault();

      $http({
          method: 'POST',
          url: '/database_config/api_save_database',
          data: {
              db_object:$scope.new_database.db,
              db_type:$scope.new_database.database_type.db_cd,
              databases:$scope.database_list
          }
      })
      .success(function(resp) {
          if(resp.status==200){
              $scope.database_config_list = $scope.database_config_list.concat(resp['data']);
              $scope.database_list = [];
              $scope.show_database = false;
              $("#configure-datasource-modal").modal("hide");
              // location.href="/client/tables/"+resp["data"].id;
              // location.href="/client/manage_data_sources";
          }else{
              alert(resp.msg);
          }
      })
      .error(function(err) {
          console.log('Got an error', err)
      });
    }

    $scope.get_database_config_list = function(){
      $http.get('/database_config/api_database_config_list/').success(function(data){
        if(data.status == 200){
          angular.copy(angular.fromJson(data.data),$scope.database_config_list);
        }else{
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
    }

    $scope.show_database_config = function(db_type){
      $scope.new_database = {}
      $scope.new_database.db={
        host:"localhost",
        port:3306,
        user:"root",
        password:"",
      }
      $scope.new_database.database_type=db_type;
      $("#configure-datasource-modal").modal({ show:true, backdrop:'static', keyboard:true });
    }
    $scope.dontHavePermission  = function($event){
      $event.preventDefault();
      alert("You don't have permission to add a new datasource !");
    }
  // -------------- DATABASE CONFIG SCREENS RELATED FUNCTION ENDS HERE ---------- //


  //____________________ START FLOWCHART IMPLEMENTATION ___________________//
    // -------------- GLOBAL FUNCTIONS FOR ALL NODES STARTS FROM HERE ---------- //
      var nextNodeID = 1;
      $scope.last_node={
        x:20,
        y:20
      }

      $scope.node_database_list = {};
      $scope.node_table_list = {};
      $scope.node_table_structure = {};
      $scope.node_table_preview = {};
      $scope.node_table_tree = {};
      $scope.current_tab = '';

      $scope.load_nodes=function(nodes){
        if(!nodes){
          nodes={};
        }
        nodes=JSON.parse(nodes);
        $scope.chartViewModel = new flowchart.ChartViewModel(nodes);
        $scope.chartViewModel.data.database_config_connection = $scope.database_config_connection;
        $scope.get_data_sources();
      }
      
      $scope.get_data_sources = function(){
        if(typeof dataSource_id !== 'undefined'){
          var url = '/database_config/api_get_data_sources/'+dataSource_id;
        }else{
          var url = '/database_config/api_get_data_sources/';
        }
        $http.get(url).success(function(data){
          if(data.status == 200){
            if(data.data){
              if(data.data.pivot_json){
                if(page == "Database_config"){
                  $scope.pivot_table = JSON.parse(data.data.pivot_json.pivot_json);
                  $scope.pivot_table.id = data.data.pivot_json.id;
                }
              }
              var source_data = JSON.parse(data.data.json_obj);
              var source = source_data.nodes;
              $scope.chartViewModel.data.database_config_connection = source_data.database_config_connection;
              $scope.database_config_connection = source_data.database_config_connection;
              $scope.chartViewModel.data.data_source_name = data.data.data_source_name;
              $scope.chartViewModel.data.id = data.data.id;
              if(source.length){
                $scope.node_database_list = JSON.parse(data.data.database_list);
                $scope.node_table_list = JSON.parse(data.data.table_list);
              }
              for (var i = 0; i < source.length; i++) {
                $scope.last_node={
                  x:source[i].x,
                  y:source[i].y
                }
                $scope.chartViewModel.addNode(angular.copy(source[i]));
              };
              if($scope.chartViewModel.data.nodes && $scope.chartViewModel.data.nodes.length){
                for (var i = 0; i < $scope.chartViewModel.data.nodes.length; i++) {
                  if($scope.chartViewModel.data.nodes[i].params.tableName){
                    $scope.chartViewModel.data.nodes[i].name = $scope.chartViewModel.data.nodes[i].params.tableName;
                  }
                }
              }
              $scope.chartViewModel.data.connections = source_data.connections;
              $scope.chartViewModel = new flowchart.ChartViewModel($scope.chartViewModel.data);
            }
          }else{
            $scope.err_msg = "There is some error to get data from server"+data.data;
          }
        })
      }

      $scope.load_nodes("{}");
      
      $scope.move_last_node=function(){
        $scope.last_node.x+=160;
        if($scope.last_node.x>800){
          $scope.last_node.x=20;
          $scope.last_node.y+=100;
        }
      }

      $scope.$watch("chartViewModel.current_node",function(){
        if(!$scope.last_current_node && $scope.chartViewModel.current_node){
          $scope.current_node=$scope.chartViewModel.current_node.data;
        }else{
          $scope.current_node=$scope.last_current_node;
          $scope.last_current_node = "";
        }

        // Reset error messages
        if($scope.current_node){
          $scope.current_node.error_message="";
          $scope.current_node.has_error=false;
        }
      })

      $scope.add_node_data_show = function (obj,type) {
        var params = node_paramService.get_params(obj,type)
        params.hierarchies = [];
        $scope.add_node_data(params);
      };

      $scope.add_node_data = function (params) {
        if(params.type == 'join'){
          var found_data_sources = [];
          if($scope.chartViewModel.data.nodes){
            found_data_sources = $.grep($scope.chartViewModel.data.nodes,function(node){ return node.type == 'mysql'});
          }
          if(found_data_sources.length < 2){
            alert("There should be atleast two data tables to join.");
            return;
          }
        }
        if(!$scope.chartViewModel.data.data_source_name){
          alert("Please enter data source name first and select also server connection!");
          $("#data_source_name").focus();
          return;
        }
        if($scope.chartViewModel.data.data_source_name && !$scope.chartViewModel.data.database_config_connection){
          alert("Please select server connection!");
          return;   
        }
        params.data_source_name = $scope.chartViewModel.data.data_source_name;
        var nodeName = params.node_name ;
        if (!nodeName) {
          return;
        }
        if($scope.chartViewModel.data.nodes){
          nextNodeID = $scope.chartViewModel.data.nodes.length+1;
          $scope.move_last_node();
        }
        // Template for a new node.
        var newNodeDataModel = {
          name: nodeName,
          code:nodeName,
          type: params.type,
          last_run_status: "none",
          dirty: true,
          id: nextNodeID++,
          x: $scope.last_node.x,
          y: $scope.last_node.y,
          icon:"/images/icon_mysql.png",
          params:params,
          inputConnectors: [
              {
                  name: ""
              },
          ],
          outputConnectors: [ 
            {
              name: ""
            },
          ],
        };
        if(newNodeDataModel.type == 'mysql'){
          delete newNodeDataModel.inputConnectors;
          if($scope.database_config_connection == 'database_config_local'){
            newNodeDataModel.params.host = "localhost";
            newNodeDataModel.params.port = 3306;
            newNodeDataModel.params.user = "root";
            newNodeDataModel.params.password = "";
          }
        }
        if($scope.current_node && $scope.current_node.params.database_token){
          newNodeDataModel.params.database_token = $scope.current_node.params.database_token;
          if($scope.current_node.params.databaseName && newNodeDataModel.type != 'mysql'){
            newNodeDataModel.params.databaseName = $scope.current_node.params.databaseName;
          }
        }
        if($scope.chartViewModel.data.nodes){
          $scope.chartViewModel.data.nodes.map(function(node_obj){
            if(node_obj.params.database_token){
              newNodeDataModel.params.database_token = node_obj.params.database_token;
            }
            /*if(node_obj.params.databaseName && newNodeDataModel.type != 'mysql'){
              newNodeDataModel.params.databaseName = node_obj.params.databaseName;
            }*/
          })
        }
        $scope.move_last_node();
        $scope.chartViewModel.addNode(newNodeDataModel);
        $scope.current_node = newNodeDataModel;
        $scope.current_node.error_message="";
        $scope.current_node.has_error=false;
        $scope.current_tab = 'config';
        var keys = Object.keys($scope.node_database_list);
        if(keys.length){
          var key = keys[0];
          $scope.node_database_list['node'+newNodeDataModel.id] = $scope.node_database_list[key];
        }
        if(newNodeDataModel.type == 'mysql'){
          $("#configuration-modal").modal("show");
        }
      };

      $scope.execute_all_nodes_loading = false;
      $scope.execute_all_nodes = function($event){
        $scope.execute_all_nodes_loading = true;
        $event.preventDefault();
        if($scope.chartViewModel.data.nodes && $scope.chartViewModel.data.nodes.length){
          $http({method:"POST",url:"/"+$scope.database_config_connection+"/api_execute_all_nodes/",data:{data:$scope.chartViewModel.data.nodes}})
          .success(function(data){
            if(data.status == 200){
              $scope.execute_all_nodes_loading = false;
            }else{
              $scope.execute_all_nodes_loading = false;
              alert(data.message);
            }
          })
          .error(function(data){
            $scope.execute_all_nodes_loading = false;
            $scope.current_node.last_run_status='error';
            $scope.current_node.last_run_message=data.message;
          })
        }
      }

      $scope.execute_node=function(){
        delete $scope.node_table_structure["node"+$scope.current_node.id]
        delete $scope.node_table_preview["node"+$scope.current_node.id]
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! We are sending data on server...';
        switch($scope.current_node.type){
          case "mysql":
            $scope.execute_data_node();
            break;
          case "join":
            $scope.execute_join_node();
            break;
          case "split":
            $scope.execute_split_node();
            break;
          case "merge":
            $scope.execute_merge_node();
            break;
          case "aggregate":
            $scope.execute_aggregate_node();
            break;
          case "concatenate":
            $scope.execute_concatenate_node();
            break;
          case "reformat":
            $scope.execute_reformat_node();
            break;
          case "copy-data":
            $scope.execute_copy_data_node();
            break;
          case "duplicate":
            $scope.execute_duplicate_node();
            break;
          case "pivot":
            $scope.execute_pivot_node();
            break;
          case "partition":
            $scope.execute_partition_node();
            break;
          case "sorting":
            $scope.execute_sorting_node();
            break;
        }
      }   

      $scope.save_data_source = function(save_all){
        if($scope.chartViewModel.data){
          data = {
            source_data:$scope.chartViewModel.data,
          }
          if($scope.current_node && $scope.current_node.type == "mysql"){
            data.database_list = $scope.node_database_list;
            data.table_list = $scope.node_table_list;
          }
          if(save_all){
            data.save_all = save_all;
            $scope.save_all_run_status = "Working"
          }
          $http({method:'POST', url:"/database_config/api_save_data_sources/", data:data}).
            success(function (data) {
              if(data.status == 200){
                if(save_all){
                  $scope.save_all_run_status = "Done"
                  // location.href = '/'+$scope.database_config_connection+'/data_sources';
                  location.href = '/database_config/data_sources';
                }
                // $scope.chartViewModel.data.data_source_name = data.data.data_source_name;
                $scope.chartViewModel.data.id = data.data.id;
              }
            }).
            error(function (data, status, headers, config) {
              console.log("There is some error in save data source !",data.data);
            }
          );
        }
      }

      $scope.deleteSelected = function () {
        $scope.chartViewModel.deleteSelected();
      };

      $scope.delete_database_config = function(){
        delete $scope.current_node.params.database_token;    
        delete $scope.current_node.params.databaseName;    
        delete $scope.current_node.params.tableName;    
        $scope.current_tab = 'config';
      }

      $scope.connected_nodes = [];
      $(document).on( 'click', '.node-btn', function(e){
        e.preventDefault();
        $scope.current_node = {};
        $scope.connected_nodes = [];
        var source_hierarchy_arr = [];
        var destination_hierarchy_arr = [];
        $scope.current_tab = $(this).data("mode");
        var node_id = $(this).data("node-id");
        var found = $.grep($scope.chartViewModel.data.nodes,function(e){ return e.id == node_id });
        $scope.current_node = found[0];
        console.log('$scope.current_node is ;==================',$scope.current_node);
        var node_id = $scope.current_node.id;
        if($scope.current_tab == 'delete'){
          if($scope.chartViewModel.data.nodes){
            $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
            $scope.database_info = {
              host:$scope.database_info.host,
              port:$scope.database_info.port,
              user:$scope.database_info.user,
              password:$scope.database_info.password,
              database:$scope.database_info.data_source_name+userid,
            }
          }
          $scope.deleteSelected();
          $scope.save_data_source();
          if($scope.current_node.params.tableName){
            var post_data = {
              data_source_name : $scope.current_node.params.data_source_name,
              tableName:$scope.current_node.params.tableName,
              database_info:$scope.database_info
            }
            $http({method:"POST",url:"/"+$scope.database_config_connection+"/api_drop_table/",data:post_data})
            .success(function(data){
              if(data.status == 200){
                delete $scope.node_table_structure["node"+$scope.current_node.id]
                delete $scope.node_table_preview["node"+$scope.current_node.id]
                $scope.save_data_source();
              }else{
                alert(data.message)
              }
            })
            .error(function(err){
              alert(err)
            })
          }
        }else{
          if(!$scope.node_table_structure["node"+$scope.current_node.id]){
            $scope.get_data_source_table_structure_and_preview($scope.current_node);
          }
          if($scope.chartViewModel.data.connections){
            var get_connections = $.grep($scope.chartViewModel.data.connections,function(connection){ return connection.dest.nodeID == node_id });
            if(get_connections.length){
              for (var i = 0; i < get_connections.length; i++) {
                var node = $.grep($scope.chartViewModel.data.nodes,function(node){ return node.id == get_connections[i].source.nodeID });
                if(node.length){
                  $scope.connected_nodes.push(node[0]);
                }
              }  
              if($scope.connected_nodes[0]){
                if($scope.current_node.type == 'join' || $scope.current_node.type == 'merge'){
                  $scope.current_node.params.source_table = $scope.connected_nodes[0].params.tableName;
                  $scope.current_node.params["source_table_node_id"] = $scope.connected_nodes[0].id;
                  var source_hierarchy_arr = $scope.connected_nodes[0].params.hierarchies;
                }
                if($scope.current_node.type == 'split' || $scope.current_node.type=='aggregate' || $scope.current_node.type == 'concatenate' || $scope.current_node.type == 'reformat' || $scope.current_node.type == 'copy-data' || $scope.current_node.type == 'duplicate' || $scope.current_node.type == 'pivot' || $scope.current_node.type == 'partition' || $scope.current_node.type == 'sorting' ){
                  $scope.current_node.params.source_table = $scope.connected_nodes[0].params.tableName;
                  $scope.current_node.params["table_node_id"] = $scope.connected_nodes[0].id;
                  // SET HIERARCHY IN CURRENT NODE FROM CONNECTED NODE...
                  if($scope.current_node.params.hierarchies && !$scope.current_node.params.hierarchies.length){
                    $scope.current_node.params.hierarchies = $scope.connected_nodes[0].params.hierarchies;
                  }
                }
                $scope.current_node.params.database_token = $scope.connected_nodes[0].params.database_token;
                $scope.current_node.params.databaseName = $scope.connected_nodes[0].params.databaseName;
                if(!$scope.node_table_structure["node"+$scope.connected_nodes[0].id]){
                  $scope.get_data_source_table_structure_and_preview($scope.connected_nodes[0]);
                }
              }

              if($scope.connected_nodes[1]){
                if($scope.current_node.type == 'join' || $scope.current_node.type == 'merge'){
                  $scope.current_node.params.destination_table = $scope.connected_nodes[1].params.tableName;
                  $scope.current_node.params["destination_table_node_id"] = $scope.connected_nodes[1].id;
                  var destination_hierarchy_arr = $scope.connected_nodes[1].params.hierarchies;
                }
                if(!$scope.node_table_structure["node"+$scope.connected_nodes[1].id]){
                  $scope.get_data_source_table_structure_and_preview($scope.connected_nodes[1]);
                }
              }
              // SET HIERARCHY IN CURRENT NODE FROM CONNECTED NODE...
              if(($scope.current_node.params.hierarchies && !$scope.current_node.params.hierarchies.length) && (source_hierarchy_arr.length && destination_hierarchy_arr.length)){
                $scope.current_node.params.hierarchies = source_hierarchy_arr.concat(destination_hierarchy_arr);
              }
            }
          }
          $("#configuration-modal").modal("show");      
        }
      });

      $scope.set_tab = function(tab) {
        $scope.current_tab = tab;
      };

      $scope.remove_where_condition = function($event,condition){
        $event.preventDefault();
        if($scope.current_node.params){
          if($scope.current_node.params.type == "reformat"){
            var index = $scope.current_node.params.drop_conditions.indexOf(condition);
            delete $scope.current_node.params.drop_conditions[index].where_condition;
            delete $scope.current_node.params.drop_conditions[index].where_condition_arr;
            $scope.current_node.params.drop_conditions[index].condition = "";
          }
          delete $scope.current_node.params[condition];
          delete $scope.current_node.params[condition+'_arr'];
        }
      }

      $scope.save_edit_label = false;
      $scope.set_hierarchy = function(is_set_hierarchy,fieldName){
        if(is_set_hierarchy){
          $scope.current_node.params.hierarchies.push({'fieldName':fieldName});
          $scope.save_edit_label = true;
          for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
            if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == fieldName){
              $scope.node_table_structure['node'+$scope.current_node.id][i].added = true;
              $scope.node_table_structure['node'+$scope.current_node.id][i].edit_label = true;
            }
          };
        }else{
          $scope.save_edit_label = false;
          for (var i = 0; i < $scope.current_node.params.hierarchies.length; i++) {
            if($scope.current_node.params.hierarchies[i].fieldName == fieldName){
              $scope.current_node.params.hierarchies.splice(i,1);
            }
          };
          for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
            if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == fieldName){
              $scope.node_table_structure['node'+$scope.current_node.id][i].added = false;
            }
          };
        }
      }

      $scope.set_hierarchy_label = function(label,fieldName){
        $scope.save_edit_label = false;
        for (var i = 0; i < $scope.current_node.params.hierarchies.length; i++) {
          if($scope.current_node.params.hierarchies[i].fieldName == fieldName){
            $scope.current_node.params.hierarchies[i].label = label;
          }
        };
        for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
          if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == fieldName){
            $scope.node_table_structure['node'+$scope.current_node.id][i].edit_label = false;
          }
        };
      }
    // -------------- GLOBAL FUNCTIONS FOR ALL NODES ARE ENDING  HERE ---------- //
    
    // -------------- DATA NODE AND RELATED FUNCTION STARTS FROM HERE ---------- //
      $scope.verify_database=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! We are trying to connect with database...';
        var url="/"+$scope.database_config_connection+"/api_connect_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message=data.message;
          $scope.current_node.params.database_token=data.token;
          $scope.chartViewModel.data.nodes.map(function(node_obj){
            node_obj.params.database_token = data.token;
            $scope.node_database_list['node'+node_obj.id] = [];
            angular.copy(angular.fromJson(data.data),$scope.node_database_list['node'+node_obj.id]);
          })
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
          }else{
            $scope.current_node.last_run_status='error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.get_datanode_tables = function(node_obj){
        if(node_obj.params.databaseName){
          if($scope.chartViewModel.data.nodes){
            $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
            $scope.database_info = {
              host:$scope.database_info.host,
              port:$scope.database_info.port,
              user:$scope.database_info.user,
              password:$scope.database_info.password,
              database:$scope.database_info.databaseName,
            }
          }
          $scope.current_node.last_run_status='Working';
          $scope.current_node.last_run_message='Please Wait! We are trying to fetch tables of selected database...';

          var post_obj = {
            databaseName:node_obj.params.databaseName,
            token:node_obj.params.database_token,
            database_info:$scope.database_info,
          }
          $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_datanode_tables/',data:post_obj})
          .success(function(data){
            $scope.current_node.last_run_message=data.message;
            if(data.status == 200){
              $scope.current_node.last_run_status='Done';
              $scope.node_table_list['node'+node_obj.id] = [];
              angular.copy(angular.fromJson(data.data),$scope.node_table_list['node'+node_obj.id]);
            }else{
              $scope.current_node.last_run_status='Error';
              $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
            }
          })
          .error(function(data){
            $scope.current_node.last_run_status='Error';
            alert("There is some error in get_datanode_tables ");
          });  
        }
      }

      $scope.get_table_structure_and_preview = function(node_obj){
        if(node_obj.params.tableName){
          $scope.current_node.last_run_status='Working';
          $scope.current_node.last_run_message='Please Wait! We are trying to fetch table structure';
          if($scope.chartViewModel.data.nodes){
            $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
            $scope.database_info = {
              host:$scope.database_info.host,
              port:$scope.database_info.port,
              user:$scope.database_info.user,
              password:$scope.database_info.password,
              database:$scope.database_info.databaseName,
            }
          }
          var post_obj = {
            databaseName:node_obj.params.databaseName,
            tableName:node_obj.params.tableName,
            token:node_obj.params.database_token,
            database_info:$scope.database_info,
          }
          $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_table_structure_and_preview/',data:post_obj})
          .success(function(data){
            $scope.current_node.last_run_message=data.message;
            if(data.status == 200){
              $scope.current_node.last_run_status='Done';
              $scope.node_table_structure['node'+node_obj.id] = []
              $scope.node_table_preview['node'+node_obj.id] = []
              angular.copy(angular.fromJson(data.tableStructure),$scope.node_table_structure['node'+node_obj.id])
              angular.copy(angular.fromJson(data.data),$scope.node_table_preview['node'+node_obj.id])
            }else{
              $scope.current_node.last_run_status='Error';
              $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
            }
          })
          .error(function(data){
            $scope.current_node.last_run_status='Error';
            alert("There is some error in get_datanode_tables ");
          });  
        }
      }

      $scope.get_data_source_table_structure_and_preview = function(node_obj){
        if(node_obj.params.tableName){
          $scope.current_node.last_run_status='Working';
          $scope.current_node.last_run_message='Please Wait! We are trying to fetch table structure';
          if($scope.chartViewModel.data.nodes){
            $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
            $scope.database_info = {
              host:$scope.database_info.host,
              port:$scope.database_info.port,
              user:$scope.database_info.user,
              password:$scope.database_info.password,
              database:$scope.database_info.data_source_name+userid,
            }
          }
          var post_obj = {
            databaseName:node_obj.params.data_source_name,
            tableName:node_obj.params.tableName,
            database_info:$scope.database_info,
          }
          $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_data_source_table_structure_and_preview/',data:post_obj})
          .success(function(data){
            $scope.current_node.last_run_message=data.message;
            if(data.status == 200){
              $scope.current_node.last_run_status='Done';
              $scope.node_table_structure['node'+node_obj.id] = [];
              $scope.node_table_preview['node'+node_obj.id] = [];
              $scope.node_table_tree['node'+node_obj.id] = [];
              angular.copy(angular.fromJson(data.tableStructure),$scope.node_table_structure['node'+node_obj.id])
              angular.copy(angular.fromJson(data.data),$scope.node_table_preview['node'+node_obj.id])
              if($scope.current_node.params.hierarchies && $scope.current_node.params.hierarchies.length){
                for (var i = 0; i < $scope.node_table_structure['node'+node_obj.id].length; i++) {
                  var found = $.grep($scope.current_node.params.hierarchies, function(e){ return e.fieldName == $scope.node_table_structure['node'+node_obj.id][i].fieldName});
                  if(found.length){
                    $scope.node_table_structure['node'+node_obj.id][i].added = true;
                    $scope.node_table_structure['node'+node_obj.id][i].hierarchy_label = found[0].label
                    var hierarchy_column = $scope.node_table_structure['node'+node_obj.id][i];
                    hierarchy_column.children = [];
                    $scope.node_table_tree['node'+node_obj.id].push(hierarchy_column);
                  }
                };
              }else{
                $scope.current_node.params.hierarchies = [];
              }
              //This below code is using only for get dimension & measures for chart on dashboard.
              $rootScope.measures_items = [];
              $rootScope.dimensions_items = [];
              for (var i = 0; i < $scope.node_table_structure['node'+node_obj.id].length; i++) {
                $scope.node_table_structure['node'+node_obj.id][i].field_name = $scope.node_table_structure['node'+node_obj.id][i].fieldName;
                var splited_field_type = $scope.node_table_structure['node'+node_obj.id][i].fieldType.split("(");
                if(splited_field_type[0] == "VARCHAR"){
                  $rootScope.dimensions_items.push($scope.node_table_structure['node'+node_obj.id][i]);
                }else{
                  $rootScope.measures_items.push($scope.node_table_structure['node'+node_obj.id][i]);
                }
              };
            }else{
              $scope.current_node.last_run_status='Error';
              $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
            }
          })
          .error(function(data){
            $scope.current_node.last_run_status='Error';
            alert("There is some error in get_datanode_tables ");
          });  
        }
      }

      $scope.add_node_in_arr = function(){
        $scope.new_node = angular.copy($scope.current_node);
        $scope.new_node.x = $scope.last_node.x;
        $scope.new_node.y = $scope.last_node.y;
        if($scope.chartViewModel.data.nodes){
          nextNodeID = $scope.chartViewModel.data.nodes.length+1;
          $scope.move_last_node();
        }
        $scope.new_node.id = nextNodeID++;
        delete $scope.new_node.params.databaseName;
        delete $scope.new_node.params.tableName;
        $scope.move_last_node();
        var keys = Object.keys($scope.node_database_list);
        if(keys.length){
          var key = keys[0];
          $scope.node_database_list['node'+$scope.new_node.id] = $scope.node_database_list[key];
          console.log("$scope.node_database_list['node'+$scope.new_node.id] is :'=",$scope.node_database_list['node'+$scope.new_node.id]);
        }
        $scope.chartViewModel.addNode(angular.copy($scope.new_node));
        $scope.current_node = $scope.new_node;
        $scope.current_node.error_message="";
        $scope.current_node.has_error=false;
      }  

      $scope.remove_node_from_arr = function(node_obj){
        /*$scope.chartViewModel.deselectAll();
        var nodeViewModel = new flowchart.NodeViewModel(node_obj);
        nodeViewModel.select();
        console.log("Selected for remove",nodeViewModel)
        $scope.chartViewModel.deleteSelected();*/
      }

      $scope.execute_data_node = function(){
        if($scope.chartViewModel.data.nodes){
          $http({method:"POST",url:"/"+$scope.database_config_connection+"/api_import_tables/",data:{data:$scope.chartViewModel.data.nodes}})
          .success(function(data){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.last_run_message=data.message;
            if(data.status == 200){
              // $scope.current_node = $scope.chartViewModel.data.nodes[0];
              for (var i = 0; i < $scope.chartViewModel.data.nodes.length; i++) {
                if($scope.chartViewModel.data.nodes[i].params.tableName){
                  $scope.chartViewModel.data.nodes[i].name = $scope.chartViewModel.data.nodes[i].params.tableName;
                }
                delete $scope.node_table_structure["node"+$scope.chartViewModel.data.nodes[i].id];
                delete $scope.node_table_preview["node"+$scope.chartViewModel.data.nodes[i].id];
              }
              $scope.save_data_source();
              $("#configuration-modal").modal("hide");
            }else{
              alert(data.msg);
            }
          })
          .error(function(data){
            $scope.current_node.last_run_status='error';
            $scope.current_node.last_run_message=data.message;
          })
        }
      }
    // -------------- DATA NODE AND RELATED FUNCTIONS ARE ENDING HERE ---------- //
    
    // -------------- JOIN NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.create_connectors = function(table_type,node_name){
        var found_node = $.grep($scope.chartViewModel.data.nodes,function(node){ return node.name == node_name});
        if(found_node.length){
          found_node = found_node[0];
          var node_id = found_node.id;
          $scope.current_node.params[table_type+"_node_id"] = node_id;
          var obj = {
            "source": {
              "nodeID": node_id,
              "connectorIndex": 0
            },
            "dest": {
              "nodeID": $scope.current_node.id,
              "connectorIndex": 0
            }
          }
          if(!$scope.chartViewModel.data.connections){
            $scope.chartViewModel.data.connections =[]
          }
          $scope.chartViewModel.data.connections.push(obj);
          $scope.last_current_node = angular.copy($scope.current_node);
          $scope.chartViewModel = new flowchart.ChartViewModel($scope.chartViewModel.data);
        }
      }
      $scope.execute_join_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';
        // START OF MATCH COLUMN NAME FOR JOIN QUERY
        var table1_structure = [];
        var table2_structure = [];
        if($scope.connected_nodes[0] && $scope.connected_nodes[1]){
          table1_structure = angular.copy($scope.node_table_structure["node"+$scope.connected_nodes[0].id]);
          table2_structure = angular.copy($scope.node_table_structure["node"+$scope.connected_nodes[1].id]);
        }
        if((table1_structure && table1_structure.length) && (table2_structure && table2_structure.length)){
          for (var i = 0; i < table1_structure.length; i++) {
            var found = $.grep(table2_structure, function(e){ return table1_structure[i].fieldName == e.fieldName });
            if(found.length){
              table1_structure[i].fieldName = "`"+$scope.connected_nodes[0].params.tableName+'`.`'+table1_structure[i].fieldName+"`";
              found[0].fieldName = "`"+$scope.connected_nodes[1].params.tableName+"`.`"+found[0].fieldName+"` as `"+$scope.connected_nodes[1].params.tableName+"_"+found[0].fieldName+"`"
            }
          };
          $scope.join_table_structure = table1_structure.concat(table2_structure);
          console.log('$scope.join_table_structure is :-=',$scope.join_table_structure);
        }

        var join_table_column = [];
        for (var i = 0; i < $scope.join_table_structure.length; i++) {
          var split_as_column = $scope.join_table_structure[i].fieldName.split(" as ");
          var split_dot_column = $scope.join_table_structure[i].fieldName.split(".");
          if(split_as_column.length == 2 || split_dot_column.length == 2){
            join_table_column.push($scope.join_table_structure[i].fieldName);
          }else{
            join_table_column.push("`"+$scope.join_table_structure[i].fieldName+"`");
          }
        };
        // console.log('join_table_column is :',join_table_column);
        // START OF MATCH COLUMN NAME FOR JOIN QUERY


        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        if($scope.current_node.params.join_type == "FULL JOIN"){
          var sql = "SELECT * FROM "+$scope.current_node.params.source_table+" LEFT JOIN "+$scope.current_node.params.destination_table+" ON "+$scope.current_node.params.source_table+"."+$scope.current_node.params.source_table_column+" = "+$scope.current_node.params.destination_table+"."+$scope.current_node.params.destination_table_column+" UNION SELECT * FROM "+$scope.current_node.params.source_table+" RIGHT JOIN "+$scope.current_node.params.destination_table+" ON "+$scope.current_node.params.source_table+"."+$scope.current_node.params.source_table_column+" = "+$scope.current_node.params.destination_table+"."+$scope.current_node.params.destination_table_column;
        }else{
          // sql = "SELECT * FROM "+$scope.current_node.params.source_table+" "+$scope.current_node.params.join_type+" "+$scope.current_node.params.destination_table+" ON "+$scope.current_node.params.source_table+"."+$scope.current_node.params.source_table_column+" = "+$scope.current_node.params.destination_table+"."+$scope.current_node.params.destination_table_column;
          sql = "SELECT "+join_table_column+" FROM `"+$scope.current_node.params.source_table+"` "+$scope.current_node.params.join_type+" `"+$scope.current_node.params.destination_table+"` ON `"+$scope.current_node.params.source_table+"`.`"+$scope.current_node.params.source_table_column+"` = `"+$scope.current_node.params.destination_table+"`.`"+$scope.current_node.params.destination_table_column+"`";
        }
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS

        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message=data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status='error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
    // -------------- JOIN NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- SPLIT NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_split_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';
        
        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var column_arr = [];
        var column_name = "";
        $scope.current_node.params.table_columns.map(function(data){
          column_arr.push("`"+data.column_name+"`");
        });
        column_name = column_arr.join(",");
        var where_conditions = ''
        if($scope.current_node.params.where_condition){
          where_conditions = 'WHERE '+$scope.current_node.params.where_condition;
        }
        // var sql = "SELECT "+column_name+ " FROM "+$scope.current_node.params.source_table+" "+where_conditions;
        var sql = "SELECT "+column_name+ " FROM `"+$scope.current_node.params.source_table+"` "+where_conditions;
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_split_columns = function(){
        $scope.current_node.params.table_columns.push({column_name:''});
      }
      $scope.remove_split_column = function(index){
        $scope.current_node.params.table_columns.splice(index,1);
      }
    // -------------- SPLIT NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- MERGE NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_merge_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // START OF MATCH COLUMN NAME FOR JOIN QUERY
        var table1_structure = angular.copy($scope.node_table_structure["node"+$scope.connected_nodes[0].id]);
        var table2_structure = angular.copy($scope.node_table_structure["node"+$scope.connected_nodes[1].id]);
        if((table1_structure && table1_structure.length) && (table2_structure && table2_structure.length)){
          for (var i = 0; i < table1_structure.length; i++) {
            var found = $.grep(table2_structure, function(e){ return table1_structure[i].fieldName == e.fieldName });
            if(found.length){
              table1_structure[i].fieldName = "`"+$scope.connected_nodes[0].params.tableName+'`.`'+table1_structure[i].fieldName+"`";
              found[0].fieldName = "`"+$scope.connected_nodes[1].params.tableName+"`.`"+found[0].fieldName+"` as `"+$scope.connected_nodes[1].params.tableName+"_"+found[0].fieldName+"`"
            }
          };
          $scope.join_table_structure = table1_structure.concat(table2_structure);
          console.log('$scope.join_table_structure is :-=',$scope.join_table_structure);
        }

        var join_table_column = [];
        for (var i = 0; i < $scope.join_table_structure.length; i++) {
          var split_as_column = $scope.join_table_structure[i].fieldName.split(" as ");
          var split_dot_column = $scope.join_table_structure[i].fieldName.split(".");
          if(split_as_column.length == 2 || split_dot_column.length == 2){
            join_table_column.push($scope.join_table_structure[i].fieldName);
          }else{
            join_table_column.push("`"+$scope.join_table_structure[i].fieldName+"`");
          }
        };
        // START OF MATCH COLUMN NAME FOR JOIN QUERY

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS START.
        var sql = "SELECT "+join_table_column+" FROM `"+$scope.current_node.params.source_table+"`,`"+$scope.current_node.params.destination_table+"`";
        $scope.current_node.params.sql = sql;
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        console.log('$scope.database_info is :',$scope.database_info);
        $scope.current_node.params.database_info = $scope.database_info;
        // LET'S APPEND MYSQL QUERY IN NODE PARAMS ENDS.
        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
    // -------------- MERGE NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- AGGREGATE NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_aggregate_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var group_column_arr = [];
        var group_column_name = "";
        $scope.current_node.params.group_by_columns.map(function(data){
          group_column_arr.push(data.column_name);
        });
        group_column_name = group_column_arr.join(",");
        var aggregate_column_arr = [];
        var aggregate_column_name = "";
        $scope.current_node.params.aggregate_columns.map(function(data){
          aggregate_column_arr.push(data.operator+"("+data.column_name+") as `"+data.new_column_name+"`");
        });
        aggregate_column_name = aggregate_column_arr.join(",");
        // var sql = "SELECT "+aggregate_column_name+" FROM "+$scope.current_node.params.source_table
        var sql = "SELECT "+aggregate_column_name+" FROM `"+$scope.current_node.params.source_table+"`";
        if(group_column_name){
          var sql = sql+" GROUP BY "+group_column_name;
        }
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_aggregate_columns = function(){
        $scope.current_node.params.aggregate_columns.push({column_name:'', operator:'', new_column_name:''});
      }
      $scope.remove_aggregate_column = function(index){
        $scope.current_node.params.aggregate_columns.splice(index,1);
      }
    // -------------- AGGREGATE NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- CONCATENAT NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_concatenate_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var concat_column_arr = [];
        var concat_column_name = "";
        $scope.current_node.params.concat_columns.map(function(data){
          concat_column_arr.push(data.column_name);
        });
        concat_column_name = concat_column_arr.join(",");
        var sql = "SELECT CONCAT_WS('_', "+concat_column_name+") AS "+$scope.current_node.params.new_field_name+" FROM "+$scope.current_node.params.source_table;
        $scope.current_node.params.sql = sql;
        // LET'S APPEND MYSQL QUERY IN NODE PARAMS

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_concate_columns = function(){
        $scope.current_node.params.concat_columns.push({column_name:'',custom_text:''});
      }
      $scope.remove_concate_column = function(index){
        $scope.current_node.params.concat_columns.splice(index,1);
      }
    // -------------- CONCATENAT NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- REFORMAT NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_reformat_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var drop_column_arr = [];
        var drop_column_name = "";
        var group_by = "";
        $scope.current_node.params.drop_conditions.map(function(data){
          if(data.condition == "remove_duplicates"){
            group_by = "group by "+data.column_name;
          }
          if(data.condition == "remove_nulls"){
            drop_column_arr.push('`'+data.column_name+'` != "NULL"');
          }
          if(data.condition == "remove_zeros"){
            drop_column_arr.push("`"+data.column_name+"` > 0");
          }
          if(data.condition == "remove_blanks"){
            drop_column_arr.push('`'+data.column_name+'` != ""');
          }
          if(data.condition == "custom_condition"){
            drop_column_arr.push(data.where_condition);
          }
        });
        if(drop_column_arr.length){
          drop_column_name = "where "+drop_column_arr.join(" AND ");
        }
        var sql = "SELECT * FROM `"+$scope.current_node.params.source_table+"` "+drop_column_name+" "+group_by;
        $scope.current_node.params.sql = sql;
        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_reformat_columns = function(){
        $scope.current_node.params.drop_conditions.push({column_name:'',condition:''});
      }
      $scope.remove_reformat_column = function(index){
        $scope.current_node.params.drop_conditions.splice(index,1);
      }
      $scope.reformatWhereCondiColumn = '';
      $scope.check_reformat = function(condition,table_node_id,column){
        if(condition == 'custom_condition'){
          if(!column.column_name){
            alert("Select first column !");
            return;
          }
          $scope.reformat_customCondition = condition;
          $scope.show_where_condition(table_node_id,column,'where_condition');
        }else{
          if(column.where_condition){
            delete column.where_condition;
            delete column.where_condition_arr;
          }
        }
      }
    // -------------- REFORMAT NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- COPY DATA NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_copy_data_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var where_conditions = ''
        if($scope.current_node.params.where_condition){
          where_conditions = 'WHERE '+$scope.current_node.params.where_condition;
        }
        // var sql = "SELECT * FROM "+$scope.current_node.params.source_table+" "+where_conditions;
        var sql = "SELECT * FROM `"+$scope.current_node.params.source_table+"` "+where_conditions;
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
    // -------------- COPY DATA NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- DUPLICATE NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_duplicate_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';
        
        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        // var sql = "CREATE TABLE IF NOT EXISTS "+$scope.current_node.params.tableName+" like "+$scope.current_node.params.source_table;
        var sql = "SELECT * FROM `"+$scope.current_node.params.source_table+"`";
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;

        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
    // -------------- DUPLICATE NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- PIVOT NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_pivot_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';
        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_pivot_columns = function(){
        $scope.current_node.params.pivot_columns.push({column_name:''});
      }
      $scope.remove_pivot_column = function(index){
        $scope.current_node.params.pivot_columns.splice(index,1);
      }
      $scope.add_more_pivot_rows = function(){
        $scope.current_node.params.pivot_rows.push({column_name:''});
      }
      $scope.remove_pivot_rows = function(index){
        $scope.current_node.params.pivot_rows.splice(index,1);
      }
      $scope.add_more_pivot_aggregate_columns = function(){
        $scope.current_node.params.pivot_aggregate_columns.push({column_name:''});
      }
      $scope.remove_pivot_aggregate_column = function(index){
        $scope.current_node.params.pivot_aggregate_columns.splice(index,1);
      }
    // -------------- PIVOT NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- PARTITION NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_partition_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';
        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
    // -------------- PARTITION NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- SORTING NODE AND RELATED FUNCTION STARTS FROM HERE --------------------//
      $scope.execute_sorting_node=function(){
        $scope.current_node.last_run_status='Working';
        $scope.current_node.last_run_message='Please Wait! Query sending on server...';

        // LET'S APPEND MYSQL QUERY IN NODE PARAMS
        var sort_column_arr = [];
        var sort_column_name = "";
        $scope.current_node.params.sort_columns.map(function(data){
          sort_column_arr.push(data.column_name+" "+data.sort_order);
        });
        sort_column_name = sort_column_arr.join(",");
        var sql = "SELECT * FROM `"+$scope.current_node.params.source_table+"` ORDER BY "+sort_column_name;
        $scope.current_node.params.sql = sql;
        // APPENDED MYSQL QUERY IN PARAMS
        if($scope.chartViewModel.data.nodes){
          $scope.database_info = $scope.chartViewModel.data.nodes[0].params;
          $scope.database_info = {
            host:$scope.database_info.host,
            port:$scope.database_info.port,
            user:$scope.database_info.user,
            password:$scope.database_info.password,
            database:$scope.database_info.data_source_name+userid,
          }
        }
        $scope.current_node.params.database_info = $scope.database_info;


        var url="/"+$scope.database_config_connection+"/api_execute_query_in_cloud_database/";
        var req=$http.post(url, $scope.current_node.params);
        req.success(function(data){
          $scope.current_node.last_run_message = data.message;
          if(data.status == 200){
            $scope.current_node.last_run_status='Done';
            $scope.current_node.name = $scope.current_node.params.tableName;
            if(!$scope.node_table_structure['node'+$scope.current_node.id]){
              $scope.get_data_source_table_structure_and_preview($scope.current_node);
            }
            $scope.save_data_source();
            $("#configuration-modal").modal("hide");
          }else{
            $scope.current_node.last_run_status = 'error';
            $scope.current_node.last_run_message = data.message;
          }
        })
        req.error(function(err){
          $scope.current_node.last_run_status='error';
          $scope.current_node.last_run_message = err;
        })
        req.finally(function(){
          $scope.update_node();
        })
      }
      $scope.add_more_sorting_columns = function(){
        $scope.current_node.params.sort_columns.push({column_name:'',sort_order:''});
      }
      $scope.remove_sorting_column = function(index){
        $scope.current_node.params.sort_columns.splice(index,1);
      }
    // -------------- SORTING NODE AND RELATED FUNCTIONS ARE ENDING HERE --------------------//

    // -------------- WHERE CONDITION FUNCTIONS FOR DATA OPERATIONS NODES STARTS FROM HERE --------------------//

      $scope.add_more_groupby_columns = function(){
        $scope.current_node.params.group_by_columns.push({column_name:''});
      }
      $scope.remove_groupby_column = function(index){
        $scope.current_node.params.group_by_columns.splice(index,1);
      }

      $scope.show_where_condition = function(table_node_id,current_node_params,param_name){
        $scope.where_condition_arr = [{
          condition:'AND',
          column:'',
          operator:'',
          value:'',
        }];
        $scope.table_node_id = table_node_id;
        $scope.param_name = param_name;
        $scope.current_node_params = current_node_params;
        if($scope.current_node_params[$scope.param_name+'_arr']){
          $scope.where_condition_arr = $scope.current_node_params[$scope.param_name+'_arr'];
        }
        if($scope.current_node.params.type == "reformat"){
          if(current_node_params.column_name){
            $scope.where_condition_arr[0].column = current_node_params.column_name;
          }
        }
        $("#where-condition-modal").modal("show");
      }

      $scope.add_more_where_condition_row = function(){
        $scope.where_condition_arr.push({
          condition:'AND',
          column:'',
          operator:'',
          value:'',
        });
      }
      $scope.remove_where_condition_row = function(index){
        $scope.where_condition_arr.splice(index,1);
      }
      // $scope.reformat_custom_condition = 'custom_condition';
      $scope.set_where_condition = function(){
        var where_condition_string_arr = [];
        for (var i = 0; i < $scope.where_condition_arr.length; i++) {
          if(i > 0){
            where_condition_string_arr.push($scope.where_condition_arr[i].condition)
          }
          where_condition_string_arr.push($scope.where_condition_arr[i].column)
          where_condition_string_arr.push($scope.where_condition_arr[i].operator)
          where_condition_string_arr.push($scope.where_condition_arr[i].value)
        }
        $scope.current_node_params[$scope.param_name] = where_condition_string_arr.join(" ");
        $scope.current_node_params[$scope.param_name+"_arr"] = $scope.where_condition_arr;
        /*if($scope.reformat_customCondition == 'custom_condition'){
          for (var i = 0; i < $scope.current_node.params.drop_conditions.length; i++) {
            if($scope.current_node.params.drop_conditions[i].condition == $scope.reformat_customCondition){
              $scope.current_node.params.drop_conditions[i].condition = $scope.current_node.params[$scope.param_name];
            }
          };
        }*/
        $("#where-condition-modal").modal("hide"); 
      }
      $scope.change_where_condition = function($event,row){
        $event.preventDefault();
        if(row.condition == 'AND'){
          row.condition = 'OR';
        }else{
          row.condition = 'AND';
        }
      }
    // -------------- WHERE CONDITION FUNCTIONS FOR DATA OPERATIONS NODES  ARE ENDING HERE --------------------//

  //____________________ ENDS FLOWCHART IMPLEMENTATION  ___________________//

  // ----------------- START CODE OF DATA FILTERS --------------------------//
    $scope.data_filters = [{
      data_sources:'',
      field_structure:[]
    }];
    $scope.add_filter_option = function(){
      $scope.data_filters.push({data_sources:'',field_structure:[]});
    }
    $scope.remove_filter_option = function(index){
      $scope.data_filters.splice(index,1);
    }
    $scope.get_node_structure_for_data_filter = function(index,nodeObj){
      $scope.data_filters[index].field_structure = [];
      $scope.data_filters[index].loading = true;
      $scope.current_node = nodeObj;
      $scope.get_data_source_table_structure_and_preview(nodeObj);
      $timeout(function(){
        $scope.data_filters[index].field_structure = $scope.node_table_structure['node'+nodeObj.id];
        $scope.data_filters[index].loading = false;
      }, 1000);
    }
  // ----------------- ENDS  CODE OF DATA FILTERS --------------------------//

  // ----------------- START CODE OF FINAL DATA PREVIEW USING NODE STRUCTURE --------------------------//
    if(page == "Database_config" || page == "datasets"){
      $scope.pivot_table = {
        "databaseName": "",
        "tableName": "",
        "calculation": {
          "calculation_name": "sale_value",
          "calculation":"",
          "calculation_formula": [
            {
              "operator": "",
              "field": ""
            },
          ]
        },
        "rows": [],
        "columns": [],
        "filters": {
          "include": [],
          "exclude": [
            {
              "columnName": "",
              "operation": "",
              "value": ""
            }
          ]
        }
      }
    }
    $scope.flat_table_structure = [];
    $scope.tree_view = [];
    $scope.table_view = [];
    $scope.table_column = [];
    $scope.rows_arr = [];
    $scope.filter_arr = [];
    $scope.added_rows = [];
    $scope.added_cols = [];
    $scope.fetching_hierarchy_data = false;
    $scope.loading_data_label = false;
    $scope.filter_item = '';
    $scope.columnPopover = {
      templateUrl: 'popover.html',
      title:"Data for spreadsheet",
      content:[],
      isOpen: true
    };

    $scope.loading_data_label = false;
    $scope.send_pivot_json = function(current_selected_grid){
      $scope.loading_data_label = true;
      var post_data = {
        json:$scope.pivot_table,
        datasource_id:$scope.chartViewModel.data.id
      }
      if(current_selected_grid != "final"){
        post_data.current_selected_grid = current_selected_grid;
      }else{
        post_data.current_selected_grid = '';
      }
      var url='/'+$scope.database_config_connection+'/api_send_pivot_json/';
      var req = $http.post(url,post_data);
      req.success(function(data){
        if(data.status == 200){
          $scope.loading_data_label = false;
          var pivot_json = JSON.parse(data.data.pivot_json);
          pivot_json.id = data.data.id;
          angular.copy(pivot_json,$scope.pivot_table);
        }else{
          $scope.loading_data_label = false;
          $scope.error = "There is some error in send pivot table to server !"
        }
      })
      req.error(function(err){
        $scope.loading_data_label = false;
        alert('There is some error from server.');
      })
    }

    // ------ FUNCTION FOR MAKING TREE VIEW USING NODES --------- //
    $scope.hierarchies_table_obj = {
      hierarchy_arr:[]
    };


    $scope.hierarchyTableLoading = false;
    $scope.hierarchy_data_from_java_server = [];
    $scope.get_last_node_table_preview = function($event){
      $scope.hierarchyTableLoading = true;
      if($event){
        $event.preventDefault();
      }
      var lastNodeIndex = $scope.chartViewModel.data.nodes.length-1;
      var post_data = [];

      $scope.current_node = $scope.chartViewModel.data.nodes[lastNodeIndex];
      $scope.last_current_node = $scope.chartViewModel.data.nodes[lastNodeIndex];
      $scope.pivot_table.tableName = angular.copy($scope.current_node.name);
      $scope.pivot_table.databaseName = angular.copy($scope.current_node.params.data_source_name);
      $scope.get_data_source_table_structure_and_preview($scope.current_node);

      if(!$scope.tree_view.length){
        for (var i = 0; i < $scope.chartViewModel.data.nodes.length; i++) {
          var get_connections = $.grep($scope.chartViewModel.data.connections,function(connection){ return connection.source.nodeID == $scope.chartViewModel.data.nodes[i].id });
          if(get_connections.length){
            var destination_node_id = get_connections[0].dest.nodeID;
            var found_dest =$.grep($scope.chartViewModel.data.nodes,function(node){ return node.id == destination_node_id});
          }
          for (var j = 0; j < $scope.chartViewModel.data.nodes[i].params.hierarchies.length; j++) {
            var hierarchy_obj = $scope.chartViewModel.data.nodes[i].params.hierarchies[j];
            if(found_dest && found_dest.length){
              hierarchy_obj.relational_column = found_dest[0].params.source_table_column;
            }
            hierarchy_obj.hierarchy_table = angular.copy($scope.chartViewModel.data.nodes[i].name);
            hierarchy_obj.value = '';
            hierarchy_obj.labelValue = angular.copy(hierarchy_obj.label);
            hierarchy_obj.label = "DESCRIPTION";
            hierarchy_obj.id = "ID";
            hierarchy_obj.Description = hierarchy_obj.label;
            hierarchy_obj.databaseName = angular.copy($scope.chartViewModel.data.nodes[i].params.data_source_name);
            hierarchy_obj.target_column = $scope.chartViewModel.data.nodes[i].params.hierarchies[j].fieldName;
            $scope.tree_view.push(angular.copy($scope.chartViewModel.data.nodes[i].params.hierarchies[j]));
            $scope.hierarchies_table_obj.databaseName = angular.copy($scope.chartViewModel.data.nodes[i].params.data_source_name);
            $scope.hierarchies_table_obj.hierarchy_arr = $scope.tree_view;
          }
        }
      }
      if($scope.hierarchies_table_obj){
        var post_data = $scope.hierarchies_table_obj;
        var url = '/'+$scope.database_config_connection+'/api_get_hierarchy_table/';
        var req = $http.post(url, post_data);
        req.success(function(data){
          if(data.status == 200){
            angular.copy(angular.fromJson(data.data),$scope.hierarchy_data_from_java_server);
            for (var i = 0; i < data.data.length; i++) {
              var found = $.grep($scope.tree_view,function(tree_obj){ return tree_obj.hierarchy_table == data.data[i].hierarchy_table});
              if(found.length){
                found[0].children = data.data[i].children;
              }
            };
            for (var i = 0; i < $scope.tree_view.length; i++) {
              $scope.add_parent_field_to_child($scope.tree_view[i]);
            }
            $scope.hierarchyTableLoading = false;
          }else{
            $scope.hierarchyTableLoading = false;
            $scope.error="There was some error in loading the hierarchy data from server. <br>Server said: " + data['msg'];
          }
        })
        req.error(function(err){
          $scope.hierarchyTableLoading = false;
          alert("There is some error from server !");
        })
      }
    }
    $scope.get_childrens = function(item){
      item.loading = true;
      var url='/'+$scope.database_config_connection+'/api_get_children/';
      var post_data = {
        databaseName:item.databaseName,
        tableName:"hierarchy_table",
        table_name:item.hierarchy_table,
        column_name:item.fieldName,
        value:item.value,
        column_name:item.target_column,
        fieldName:item.fieldName,
      }
      if($scope.chartViewModel.data.nodes){
        var database_info = $scope.chartViewModel.data.nodes[0].params;
        post_data.host = database_info.host;
        post_data.port = database_info.port;
        post_data.user = database_info.user;
        post_data.password = database_info.password;
      }
      var req=$http.post(url, post_data);
      req.success(function(data){
        console.log("Data is : ",data)
        if(data.status == 200){
          item.loading = false;
          angular.copy(angular.fromJson(data.data),item.children);
        }else{
          item.loading = false;
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      req.error(function(err){
        alert("There is some error from server ");
      })
    }
    $scope.current_filter = {};
    $scope.show_filter_data = function($event,item){
      if($event){
        $event.preventDefault();
      }
      $scope.current_filter = item;
      if(!$scope.current_filter.fields){
        $scope.loading_data_label = true;
        $scope.pivot_table.filters.include.push({
          "filter_by":item.labelValue,
          "columnName": "",
          "operation": "",
          "value": "",
        })
        var post_data = {
          databaseName:item.databaseName,
          tableName:item.hierarchy_table
        }
        $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_data_source_table_structure_and_preview/',data:post_data})
        .success(function(data){
          if(data.status == 200){
            data.tableStructure.map(function(column){
              column.field_name = column.fieldName;
              column.databaseName = item.databaseName;
              column.hierarchy_table = item.hierarchy_table;
            })
            $scope.loading_data_label = false;
            $scope.current_filter.fields = data.tableStructure;
            $("#filter-config-modal").modal("show");
          }else{
            $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
          }
        })
        .error(function(data){
          alert("There is some error from server ");
        });
      }else{
        $("#filter-config-modal").modal("show");
      }
    }
    $scope.get_filter_value = function(field,filter_obj){
      if(!$scope.flat_table_structure.length){
        angular.copy($scope.node_table_structure['node'+$scope.current_node.id],$scope.flat_table_structure);
      }
      var split_field = field.split(".");
      var flat_column_name = $scope.current_filter.hierarchy_table+"_"+split_field[1];
      var found_column =$.grep($scope.flat_table_structure,function(col_obj){ return col_obj.fieldName == flat_column_name});
      if(found_column.length){
        var actual_field = found_column[0].fieldName
      }else{
        var actual_field = field;
      }
      filter_obj.loading = true;
      // var found = $.grep($scope.current_filter.fields,function(field_obj){ return $scope.pivot_table.tableName+'.'+field_obj.fieldName == field });
      // if(found.length){
        // found = found[0];
        var post_data = {
          // databaseName:found.databaseName,
          databaseName:'test_database',
          // maintableName:found.hierarchy_table,
          maintableName:$scope.pivot_table.tableName,
          tableName:"filter_attributes",
          fieldName:actual_field
        }
        $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_filter_attributes/',data:post_data})
        .success(function(data){
          if(data.status == 200){
            data.data.map(function(column){
              column.attribute_value = column.ATTRIBUTE_VALUE;
            })
            filter_obj.values = data.data;
            filter_obj.loading = false;
          }else{
            $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
          }
        })
        .error(function(data){
          alert("There is some error from server ");
        });
      // }
    }
    // ------ ENDS CODE MAKING TREE VIEW USING NODES ------- //

    // ------ FUNCTION FOR MAKING TREE VIEW USING LOCAL DATABASE CONNECTIONS--------- //
    /*$scope.get_last_node_table_preview = function($event){
      var post_data = {
          host:"localhost",
          port:3306,
          user:"root",
          password:"",
          password:"biz@123",
          database:"ramp_db",
          table_name:"txn",
          socketPath:'',
          socketPath:'/var/run/mysqld/mysqld.sock',
      }
      $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_hierarchy_columns/',data:post_data})
      .success(function(data){
        if(data.status == 200){
          $scope.tree_view = data.data;
        }else{
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      .error(function(data){
        alert("There is some error from server ");
      });
    }
    $scope.get_childrens = function(item){
      console.log("Item is : ",item);
      $scope.fetching_hierarchy_data = true;
      $scope.table_view = [];
      $scope.table_column = [];
      var post_data = {
        host:"localhost",
        port:3306,
        user:"root",
        password:"",
        password:"biz@123",
        database:"ramp_db",
        table_name:item.table_name,
        column_name:item.target_column,
        value:item.value,
        fieldName:item.fieldName,
        socketPath:'',
        socketPath:'/var/run/mysqld/mysqld.sock',
      }
      $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_childrens/',data:post_data})
      .success(function(data){
        if(data.status == 200){
          angular.copy(angular.fromJson(data.data['rows']),item.children);
          angular.copy(angular.fromJson(data.data['table']),$scope.table_view);
          if($scope.table_view.length){
            var obj = $scope.table_view[0];
            $scope.table_column = Object.keys(obj);
          }
          $scope.fetching_hierarchy_data = false;
        }else{
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      .error(function(data){
        alert("There is some error from server ");
      });
    }
    $scope.get_data_for_spreadsheet = function(){
      var post_data = {
        host:"localhost",
        port:3306,
        user:"root",
        password:"",
        password:"biz@123",
        database:"ramp_db",
        socketPath:'',
        socketPath:'/var/run/mysqld/mysqld.sock',
        headers:$rootScope.gridOptions.columnDefs,
        rows_arr:$scope.rows_arr,
      }
      console.log("POST data is : ",post_data)
      $http({method:"POST",url:"/"+$scope.database_config_connection+"/api_get_data_for_spreadsheet/",data:post_data})
      .success(function(data){
        console.log("Getting data in add_columns_in_sheet",data);
        angular.copy(angular.fromJson(data.data),$rootScope.gridOptions.data);
      })
      .error(function(data){
        alert("There is some error.");
      })
    }
    $scope.add_to_filter = function($event,item){
      $event.preventDefault();
      item.opened = false;
      item.added = true;
      $scope.filter_item = item;
      var post_data = {
        host:"localhost",
        port:3306,
        user:"root",
        password:"",
        password:"biz@123",
        database:"ramp_db",
        table_name:item.table_name,
        socketPath:'',
        socketPath:'/var/run/mysqld/mysqld.sock',
      }
      $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_filter_columns/',data:post_data})
      .success(function(data){
        if(data.status == 200){
          $scope.filter_arr.push({fields:data.data,values:[],condition:"AND"});
        }else{
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      .error(function(data){
        alert("There is some error from server ");
      });
    }
    $scope.get_filter_value = function(field,index){
      if(field){
        var post_data = {
          host:"localhost",
          port:3306,
          user:"root",
          password:"",
          password:"biz@123",
          database:"ramp_db",
          table_name:field.table_name,
          column_name:field.field_name,
          socketPath:'',
          socketPath:'/var/run/mysqld/mysqld.sock',
        }
        $http({method:'POST',url:'/'+$scope.database_config_connection+'/api_get_filter_attributes/',data:post_data})
        .success(function(data){
          if(data.status == 200){
            $scope.filter_arr[index].values = data.data;
          }else{
            $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
          }
        })
        .error(function(data){
          alert("There is some error from server ");
        });
      }
    }*/
    // ------ ENDS CODE MAKING TREE VIEW USING LOCAL DATABASE CONNECTIONS ------- //
    $scope.add_parent_field_to_child = function(item){
      // PREPEND FILTER IN PIVOT FILTERS
      var found_pivot_filter = $.grep($scope.pivot_table.filters.include,function(filter){ return filter.filter_by == item.labelValue });
      if(found_pivot_filter.length){
        item.added = true;
        item.opened = false;
        $scope.filter_arr.push(item);
      }
      for (var i = 0; i < item.children.length; i++) {
        item.children[i].fieldName = item.fieldName;
        item.children[i].relational_column = item.relational_column;
        item.children[i].databaseName = item.databaseName;
        item.children[i].target_column = item.target_column;
        item.children[i].value = item.id;

        // PREPEND COLUMNS IN PIVOT COLUMNS
        var found_col_pivot = $.grep($scope.pivot_table.columns,function(col){ return col.columnName == "txn_master."+item.children[i].relational_column && col.levelValue == item.children[i].value});
        if($rootScope.gridOptions.columnDefs.length == 1 && found_col_pivot.length){
          $scope.pivot_table.columns.splice($scope.pivot_table.columns.indexOf(found_col_pivot[0]));
          $scope.add_headers_in_sheet(null,item.children[i])
        }

        // PREPEND ROWS IN PIVOT ROWS
        var found_row_pivot = $.grep($scope.pivot_table.rows,function(row){ return row.columnName == "txn_master."+item.children[i].relational_column && row.levelValue == item.children[i].value});
        if(!$rootScope.gridOptions.data.length && found_row_pivot.length){
          $scope.pivot_table.rows.splice($scope.pivot_table.rows.indexOf(found_row_pivot[0]));
          $scope.add_rows_in_sheet(null,item.children[i])
        }

        if(item.children[i].children.length){
          $scope.add_parent_field_to_child(item.children[i])
        }
      }
    }
    $scope.get_same_levels = function(arr,obj,arr_type){
      angular.forEach(arr, function(value, key) {
        if(value.children){
          var found_levels = $.grep(value.children,function(child){ return child[obj.target_column] == obj[obj.target_column]});
          if(found_levels.length && found_levels[0].id == obj.id){
            for (var i = 0; i < found_levels.length; i++) {
              if(arr_type == 'columns'){
                var col_obj = {};
                col_obj = {
                  "enableCellEdit": true,
                  "cellEditableCondition": true,
                  "editableCellTemplate": "ui-grid/cellEditor",
                  "enableCellEditOnFocus": false,
                  "enableColumnMoving": true,
                  "enablePinning": true,
                  "enableColumnResizing": true,
                  "allowCellFocus": true,
                  "type": "string"
                };
                col_obj.name = found_levels[i].labelValue;
                col_obj.ID = found_levels[i].id;
                col_obj.fieldName = found_levels[i].fieldName;
                $rootScope.gridOptions.columnDefs.push(col_obj);
              }else{
                $rootScope.gridOptions.data.push({columns:found_levels[i].labelValue});
              }
            }
          }else{
            $scope.get_same_levels(value.children,obj,arr_type);
          }
        }
      });
    }
    $scope.add_headers_in_sheet = function($event,item,pivot_json_type){
      $scope.loading_data_label = true;
      $rootScope.gridOptions.columnDefs = [];
      // $scope.added_cols = [];
      $rootScope.gridOptions.columnDefs = [first_column_object]
      $scope.get_same_levels($scope.tree_view,item,'columns');
      var found_parent = $.grep($scope.tree_view,function(tree_item){ return tree_item.fieldName == item.fieldName});
      if(found_parent.length){
        found_parent[0].opened = false;
        found_parent[0].added = true;
        var found_parent = angular.copy(found_parent[0]);
        found_parent.children = $rootScope.gridOptions.columnDefs;
        found_parent.name = found_parent.labelValue;
        $scope.added_cols.push(found_parent);
        var found_hierarchy_obj = $.grep($scope.hierarchy_data_from_java_server,function(tree_hierarchy_item){ return tree_hierarchy_item.hierarchy_table == found_parent.hierarchy_table});
        var pivot_col_obj =   {
          "columnName": $scope.pivot_table.tableName+"."+found_parent.relational_column,
          "labelColumnName": found_parent.hierarchy_table+".DESCRIPTION",
          "hierarchyColumnName": found_parent.hierarchy_table+"."+item.target_column,
          "levelValue":item.value,
          "hierarchy_obj":found_hierarchy_obj[0]
        }
        $scope.pivot_table.columns.push(pivot_col_obj);
        if(pivot_json_type == 'final'){
          $scope.send_pivot_json('final');
        }
        $scope.loading_data_label = false;
      }
    }
    $scope.remove_column = function($event,column,pivot_json_type){
      if(column.children){
        $scope.added_cols[0].children = [];
        $rootScope.gridOptions.columnDefs = [first_column_object]
      }else{
        $rootScope.gridOptions.columnDefs.splice($rootScope.gridOptions.columnDefs.indexOf(column),1);
      }
      var found_column = $.grep($scope.pivot_table.columns,function(col){ return col.columnName == $scope.pivot_table.tableName+"."+column.relational_column});
      if(found_column.length){
        $scope.pivot_table.columns.splice($scope.pivot_table.columns.indexOf(found_column[0]),1);
      }
      if($scope.added_cols[0].children.length <= 1){
        var found_parent = $.grep($scope.tree_view,function(tree_item){ return tree_item.fieldName == $scope.added_cols[0].fieldName});
        if(found_parent.length){
          found_parent[0].opened = false;
          found_parent[0].added = false;
          $scope.added_cols = [];
        }
      }
      if(pivot_json_type == 'final'){
        $scope.send_pivot_json('final');
      }
    }
    $scope.add_rows_in_sheet = function($event,item,pivot_json_type){
      $scope.loading_data_label = true;
      $rootScope.gridOptions.data = [];
      $scope.added_rows = [];
      $scope.get_same_levels($scope.tree_view,item,'rows');
      var found_parent = $.grep($scope.tree_view,function(tree_item){ return tree_item.fieldName == item.fieldName});
      if(found_parent.length){
        found_parent[0].opened = false;
        found_parent[0].added = true;
        var found_parent = angular.copy(found_parent[0]);
        found_parent.children = $rootScope.gridOptions.data;
        found_parent.columns = found_parent.labelValue;
        $scope.added_rows.push(found_parent);
        var found_hierarchy_obj = $.grep($scope.hierarchy_data_from_java_server,function(tree_hierarchy_item){ return tree_hierarchy_item.hierarchy_table == found_parent.hierarchy_table});
        var pivot_row_obj =   {
          "columnName": $scope.pivot_table.tableName+"."+found_parent.relational_column,
          "labelColumnName": found_parent.hierarchy_table+".DESCRIPTION",
          "hierarchyColumnName": found_parent.hierarchy_table+"."+item.target_column,
          "levelValue":item.value,
          "hierarchy_obj":found_hierarchy_obj[0]
        }
        $scope.pivot_table.rows.push(pivot_row_obj);
        if(pivot_json_type == 'final'){
          $scope.send_pivot_json('final');
        }
        $scope.loading_data_label = false;
      }
    }
    $scope.remove_row = function($event,row,pivot_json_type){
      if(row.children){
        $scope.added_rows[0].children = [];
        $rootScope.gridOptions.data = [];
      }else{
        $rootScope.gridOptions.data.splice($rootScope.gridOptions.data.indexOf(row),1);
      }
      var found_row = $.grep($scope.pivot_table.rows,function(row_obj){ return row_obj.columnName == $scope.pivot_table.tableName+"."+row.relational_column});
      if(found_row.length){
        $scope.pivot_table.rows.splice($scope.pivot_table.rows.indexOf(found_row[0]),1);
      }
      if(!$scope.added_rows[0].children.length){
        var found_parent = $.grep($scope.tree_view,function(tree_item){ return tree_item.fieldName == $scope.added_rows[0].fieldName});
        if(found_parent.length){
          found_parent[0].opened = false;
          found_parent[0].added = false;
          $scope.added_rows = [];
        }
      }
      if(pivot_json_type == 'final'){
        $scope.send_pivot_json('final');
      }
    }
    $scope.add_to_filter = function($event,item){
      $event.preventDefault();
      item.added = true;
      item.opened = false;
      $scope.filter_arr.push(item);
      $scope.show_filter_data(null,item);
    }
    $scope.remove_filter = function($event,item,pivot_json_type){
      $event.preventDefault();
      item.added = false;
      $scope.filter_arr.splice($scope.filter_arr.indexOf(item),1);
      for (var i = $scope.pivot_table.filters.include.length - 1; i >= 0; i--) {
        if($scope.pivot_table.filters.include[i].filter_by == item.labelValue){
          $scope.pivot_table.filters.include.splice(i,1);
        }
      }
      if(pivot_json_type == 'final'){
        $scope.send_pivot_json('final');
      }
    }
    $scope.change_filter_condition = function($event,filter){
      $event.preventDefault();
      if(filter.condition == 'AND'){
        filter.condition = 'OR';
      }else{
        filter.condition = 'AND';
      }
    }
    $scope.show_calculation_formula = function(){
      console.log($scope.node_table_structure['node'+$scope.current_node.id]);
      if(!$scope.flat_table_structure.length){
        angular.copy($scope.node_table_structure['node'+$scope.current_node.id],$scope.flat_table_structure);
        $scope.current_filter.fields = $scope.flat_table_structure;
      }
      $("#calculation-formula-modal").modal("show");
    }
    $scope.add_calculation_field = function(){
      $scope.pivot_table.calculation.calculation_formula.push({"operator":"","field":""})
    }
    $scope.remove_calculation_field = function(field){
      $scope.pivot_table.calculation.calculation_formula.splice($scope.pivot_table.calculation.calculation_formula.indexOf(field),1);
    }
    $scope.show_exclude_filter = function(){
      if(!$scope.flat_table_structure.length){
        angular.copy($scope.node_table_structure['node'+$scope.current_node.id],$scope.flat_table_structure);
        $scope.current_filter.fields = $scope.flat_table_structure;
      }
      $("#filter-exclude-modal").modal("show");
    }
    $scope.add_exclude_field = function(){
      $scope.pivot_table.filters.exclude.push({"columnName": "","operation": "","value": ""})
    }
    $scope.remove_exclude_field = function(field){
      $scope.pivot_table.filters.exclude.splice($scope.pivot_table.filters.exclude.indexOf(field),1);
    }
    $scope.add_filter_data = function(){
      $scope.pivot_table.filters.include.push({"filter_by":$scope.current_filter.labelValue,"columnName": "","operation": "","value": ""})
    }
    $scope.remove_filter_data = function(field){
      $scope.pivot_table.filters.include.splice($scope.pivot_table.filters.include.indexOf(field),1);
    }
  // ----------------- ENDS  CODE FINAL DATA PREVIEW USING NODE STRUCTURE --------------------------//

  // ----------------- START CODE FOR EDIT TEMPLATES & CHARTS --------------------------------------//
    $('#add-table-modal').on('shown.bs.modal', function () {
      $scope.get_last_node_table_preview();
    })
    $scope.save_table = function(current_selected_grid){
      if($rootScope.current_selected_div.table_data){
        $rootScope.current_selected_div.table_data.data = angular.copy($rootScope.gridOptions);
        $scope.send_pivot_json(current_selected_grid);
        $scope.update_dashboard(null,$rootScope.current_selected_div);
      }
      $("#add-table-modal").modal("hide");
    }
  // ----------------- ENDS CODE FOR EDIT TEMPLATES & CHARTS --------------------------------------//
});