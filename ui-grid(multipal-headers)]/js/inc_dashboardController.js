vis_engine.controller('DashboardCtrl', function ($scope,$rootScope,$http,echartService,uiGridConstants) {
  /*$rootScope.gridsterOpts.resizable.stop = function(event, $element, widget) { // Update dashboard on resize
    $scope.update_dashboard(null,widget); //Update json of dashboard
  }
  $rootScope.gridsterOpts.draggable.stop = function(event, $element, widget) { // Update dashboard on drag
    $scope.update_dashboard(null,widget); //Update json of dashboard
  }*/

  /*$http.get('https://cdn.rawgit.com/angular-ui/ui-grid.info/gh-pages/data/500_complex.json')
  .success(function(data) {
    data.forEach( function(row) {
      row.registered = Date.parse(row.registered);
    });
    $rootScope.gridOptions.data = data;
  });*/

  /*$scope.config = {
      title: 'Line Chart',
      subtitle: 'Line Chart Subtitle',
      debug: true,
      showXAxis: true,
      showYAxis: true,
      showLegend: true,
      stack: false,
      height:300,
  };

  $scope.multiple = [
    {
      "datapoints": [
        {
          "x": "food>dal",
          "y": 380
        },
        {
          "x": "food>rice",
          "y": 115
        },
        {
          "x": "garment>jeans",
          "y": 800
        },
        {
          "x": "garment>shirt",
          "y": 155
        },
        {
          "x": "garment>shoe",
          "y": 300
        },
        {
          "x": "toys>ball",
          "y": 25
        },
        {
          "x": "toys>bat",
          "y": 1500
        },
        {
          "x": "toys>car",
          "y": 75
        },
        {
          "x": "toys>gun",
          "y": 190
        }
      ]
    },
    {
      "datapoints": [
        {
          "x": "food>dal",
          "y": 19
        },
        {
          "x": "food>rice",
          "y": 23
        },
        {
          "x": "garment>jeans",
          "y": 90
        },
        {
          "x": "garment>shirt",
          "y": 11
        },
        {
          "x": "garment>shoe",
          "y": 30
        },
        {
          "x": "toys>ball",
          "y": 2
        },
        {
          "x": "toys>bat",
          "y": 15
        },
        {
          "x": "toys>car",
          "y": 10
        },
        {
          "x": "toys>gun",
          "y": 19
        }
      ]
    }
  ];
  
  $scope.multiple.forEach(function(obj,key){
    $rootScope.gridOptions.data[key] = {};
    obj.datapoints.forEach(function(row){
      $rootScope.gridOptions.data[key][row.x] = row.y;
    })
  })

  $scope.$watch('gridOptions.data', function(newVal) {
    $rootScope.gridOptions.data.forEach(function(obj,key){
      $scope.multiple[key] = {datapoints:[]}
      var obj_keys = Object.keys(obj);
      obj_keys.map(function(obj_key){
        $scope.multiple[key].datapoints.push({x:obj_key,y:obj[obj_key]});
      })
    })
  }, true);*/
  var nextGridID = 1;
  $scope.add_widget = function($event){
    $event.preventDefault();
    if($scope.chart_widgets.length){
      var last_widget_index = $scope.chart_widgets.length-1;
      nextGridID = $scope.chart_widgets[last_widget_index].id+1;
    }
    // $scope.chart_widgets.push({ size: { x: 3, y: 2 }, position: [0, 0],"sizeX": 6,"sizeY": 2 });
    $scope.chart_widgets.push({ id:nextGridID,size: { x: 2, y: 2 }, position: [0, 0] });
  }
  $scope.removeGridId = [];
  $scope.remove_widget = function(widget){
    $scope.removeGridId.push(widget.id);
    $scope.chart_widgets.splice($scope.chart_widgets.indexOf(widget), 1);
    if(!$scope.chart_widgets.length){
      $scope.chart_widgets.push({ id:nextGridID,size: { x: 2, y: 2 }, position: [0, 0] });
    }
    // $scope.update_dashboard(null,widget);
  }
  $scope.dashboard_list = [];
  $scope.get_dashboard_list = function(){
    $http.get('/dashboard/api_get_dashboard_list/').success(function(data){
      if(data['status'] == 200){
        angular.copy(angular.fromJson(data.data),$scope.dashboard_list);
        $scope.edit_gridster = false;
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
  }
  $scope.current_dashboard = {};
  $scope.chart_widgets = [];
  $scope.select_dashboard = function(){
    $scope.chart_widgets = [];
    $http.get("/dashboard/api_get_dashboard_data/"+dashboard_id).success(function(data){
      if(data.status == 200){
        $scope.current_dashboard=data['data'];
        $scope.chart_widgets = JSON.parse($scope.current_dashboard.chart_data);
        // if($rootScope.gridOptions){
          $scope.chart_widgets_spreadsheets = [];
          $scope.chart_widgets.forEach(function(chart_obj,chart_key){
             var gridOptions = {
              showGridFooter: true,
              showColumnFooter: true,
              enableFiltering: true,
              exporterMenuCsv: false,
              enableGridMenu: true,
              headerTemplate:'/ui-grid/header-template.html',
              columnDefs: [],
              data: [],
              onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
              }
            };
            if(chart_obj.chart_data){
              chart_obj.chart_data.chart_config.data.forEach(function(obj,key){
                gridOptions.data[key] = {};
                obj.datapoints.forEach(function(row){
                  gridOptions.columnDefs.push({name:row.x,aggregationType: uiGridConstants.aggregationTypes.sum});
                  gridOptions.data[key][row.x] = row.y;
                })
              })
            }
            $scope.chart_widgets_spreadsheets[chart_key] = angular.copy(gridOptions);
          })
        // }
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
  }

  

  $scope.$watch('chart_widgets_spreadsheets', function(newVal) {
    if($scope.chart_widgets.length){
      $scope.chart_widgets_spreadsheets.forEach(function(grid_obj,chart_key){
        grid_obj.data.forEach(function(obj,key){
          $scope.chart_widgets[chart_key].chart_data.chart_config.data[key] = {datapoints:[]}
          var obj_keys = Object.keys(obj);
          obj_keys.map(function(obj_key){
            if(obj_key != "$$hashKey"){
              $scope.chart_widgets[chart_key].chart_data.chart_config.data[key].datapoints.push({x:obj_key,y:obj[obj_key]});
            }
          })
        })
      })
    }
  }, true);

  $scope.$watch('chart_widgets', function(newVal) {
      // $scope.chart_widgets_spreadsheets = [];
      $scope.chart_widgets.forEach(function(chart_obj,chart_key){
        var gridOptions = {
          showGridFooter: true,
          showColumnFooter: true,
          enableFiltering: true,
          exporterMenuCsv: false,
          enableGridMenu: true,
          columnDefs: [],
          data: [],
          onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
          }
        };
        if(chart_obj.chart_data && chart_obj.chart_data.chart_config){
          chart_obj.chart_data.chart_config.data.forEach(function(obj,key){
            gridOptions.data[key] = {};
            obj.datapoints.forEach(function(row){
              gridOptions.columnDefs.push({name:row.x,aggregationType: uiGridConstants.aggregationTypes.sum});
              gridOptions.data[key][row.x] = row.y;
            })
          })
        }
        if(!$scope.chart_widgets_spreadsheets[chart_key] && chart_obj.chart_data && chart_obj.chart_data.chart_config){
          $scope.chart_widgets_spreadsheets[chart_key] = angular.copy(gridOptions);
        }
      })
  }, true);

  $scope.update_dashboard = function($event,div){
    if($event){
      $event.preventDefault();
    }
    // div.chart_data.edit_label = false;
    // div.chart_data.edit_desc = false;
    if($scope.removeGridId.length){
      var post_obj = {
        removeGridId:$scope.removeGridId,
        datasource_id:1
      }
      $http({method:"POST",url:"/database_config/api_delete_pivot_json/",data:post_obj})
      .success(function(data){
        if(data.status == 200){
          console.log("Delete pivot json successfully.");
        }
      });
    }
    $scope.current_dashboard.chart_data = $scope.chart_widgets;
    $http({method:"POST",url:"/dashboard/api_save_dashboard/",data:$scope.current_dashboard})
    .success(function(data){
      if(data.status == 200){
        $scope.current_dashboard=data['data'];
        $scope.chart_widgets = JSON.parse($scope.current_dashboard.chart_data);
        $scope.gridsterOpts.resizable = {
           enabled: false,
           handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
           start: function(event, $element, widget) {}, // optional callback fired when resize is started,
           resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
           stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
        },
        $scope.gridsterOpts.draggable = {
           enabled: false, // whether dragging items is supported
           handle: '.my-class', // optional selector for resize handle
           start: function(event, $element, widget) {}, // optional callback fired when drag is started,
           drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
           stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
        }
        $scope.edit_gridster = false;
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
    .error(function(data){
      $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
    })
  }

  //Chart popup related functions
  $scope.show_add_chart = function($event,div){
    $event.preventDefault();
    $scope.current_chart = {};
    $rootScope.measures_items = [];
    $rootScope.dimensions_items = [];
    $rootScope.current_selected_div = div;
    $rootScope.current_selected_div.related_reports = [
      {
        report_id:"",
        report_name:''
      }
    ];
    $rootScope.current_selected_div.chart_data = {};
    $("#add-charts-modal").modal({show:true,keyboard:false,backdrop:'static'});
  }

  $scope.show_add_table = function($event,div){
    $event.preventDefault();
    $rootScope.current_selected_grid = div.id;
    $rootScope.current_selected_div = div;
    $rootScope.current_selected_div.table_data = {};
    $scope.pivot_table = {
      "databaseName": "",
      "tableName": "",
      "calculation": {
        "calculation_name": "sale_value",
        "calculation":"",
        "calculation_formula": [
          {
            "operator": "",
            "field": ""
          },
        ]
      },
      "rows": [],
      "columns": [],
      "filters": {
        "include": [],
        "exclude": [
          {
            "columnName": "",
            "operation": "",
            "value": ""
          }
        ]
      }
    }
    $("#add-table-modal").modal({show:true,keyboard:false,backdrop:'static'});
  }

  $scope.show_add_number = function($event,div){
    $event.preventDefault();
    $rootScope.current_selected_div = div;
    $rootScope.current_selected_div.numeric_data = {};
    $("#modal-number-add").modal({ show: true, backdrop: 'static', keyboard: true });
  }
  $scope.show_add_customtext = function($event,div){ 
    $event.preventDefault();
    $rootScope.current_selected_div = div;
    $rootScope.current_selected_div.text_data = {};
    $("#modal-custom_text-add").modal({ show: true, backdrop: 'static', keyboard: true });
  }
  $scope.save_numbers = function($event){
    $event.preventDefault();
    $rootScope.current_selected_div.numeric_data.data = {};
    $("#modal-number-add").modal("hide");
  }
  $scope.save_text = function($event){ 
    $event.preventDefault();
    $("#modal-custom_text-add").modal("hide");
  }

  // SAVE CHARTS FROM POPUP.
  $scope.save_charts = function(){
    if($rootScope.current_selected_div.chart_data){
      $rootScope.current_selected_div.chart_data.chart_config = $scope.chart_config;
      $rootScope.current_selected_div.chart_data.chart_config.options.height=$rootScope.current_selected_div.height-20;//This height should be same as Widget height
      $rootScope.current_selected_div.chart_data.chart_config.options.width=$rootScope.current_selected_div.width; //This width should be same as Widget width
      $rootScope.current_selected_div.chart_data.chart_config.options.showLegend = false;
      $rootScope.current_selected_div.chart_data.chart_config.options.noToolbox = true;
      $rootScope.current_selected_div.chart = '<'+$rootScope.current_selected_div.chart_data.chart_config.type+'-chart config="div.chart_data.chart_config.options" data="div.chart_data.chart_config.data"></'+$rootScope.current_selected_div.chart_data.chart_config.type+'-chart>';
      $rootScope.current_selected_div.size = { x: 3, y: 2 };
      $rootScope.current_selected_div.sizeX = 6;
      $rootScope.current_selected_div.sizey = 2;
      $rootScope.current_selected_div.row = $rootScope.current_selected_div.row+1;
    }
    if($rootScope.current_selected_div.table_data){
      $rootScope.current_selected_div.table_data.data = angular.copy($rootScope.gridOptions);
    }
    // $scope.initialize_data();
    $("#add-charts-modal").modal("hide");
  }
  // SAVE CHARTS FROM POPUP.
  $scope.save_table = function(){
    if($rootScope.current_selected_div.table_data){
      $rootScope.current_selected_div.table_data.data = angular.copy($rootScope.gridOptions);
    }
    // $scope.initialize_data();
    $("#add-table-modal").modal("hide");
  }
  $scope.enable_gridster = function($event){
    $event.preventDefault();
    $scope.edit_gridster = true;
    $scope.gridsterOpts.resizable = {
       enabled: true,
       handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
       start: function(event, $element, widget) {}, // optional callback fired when resize is started,
       resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
    },
    $scope.gridsterOpts.draggable = {
       enabled: true, // whether dragging items is supported
       handle: '.my-class', // optional selector for resize handle
       start: function(event, $element, widget) {}, // optional callback fired when drag is started,
       drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
    }
  }

  $scope.add_more_report = function($event){
    $event.preventDefault();
    $scope.current_selected_div.related_reports.push({
      report_id:"",
      report_name:''
    });
  }
  $scope.remove_report = function($event,index){
    $event.preventDefault();
    $scope.current_selected_div.related_reports.splice(index,1);
  }
  $scope.set_related_report = function(report_id,report){
    if(report_id){
      var report_obj = $.grep($scope.dashboard_list,function(report){ return report.id == report_id})
      if(report_obj.length){
        report.report_name = report_obj[0].name;
      }
    }
  }

  // START CODE OF EDIT WIDGETS
  $scope.pivot_table = {
    "databaseName": "",
    "tableName": "",
    "calculation": {
      "calculation_name": "sale_value",
      "calculation":"",
      "calculation_formula": [
        {
          "operator": "",
          "field": ""
        }
      ]
    },
    "rows": [],
    "columns": [],
    "filters": {
      "include": [],
      "exclude": [
        {
          "columnName": "",
          "operation": "",
          "value": ""
        }
      ]
    }
  }
  $scope.show_edit_widgets_modal = function($event,div){
    $event.preventDefault();
    $rootScope.current_selected_div = div;
    if(div.table_data){
      $scope.get_pivot_json_for_edit_template(div);
    }else{
      $scope.edit_chart();
    }
  }
  $scope.get_pivot_json_for_edit_template = function(){
    $rootScope.current_selected_grid = $rootScope.current_selected_div.id;
    if($rootScope.current_selected_grid){
      $http.get('/database_config/api_get_data_sources/1/?grid_id='+$rootScope.current_selected_grid).success(function(data){
        if(data.status == 200){
          pivot_json = JSON.parse(data.data.pivot_json.pivot_json);
          pivot_json.id = data.data.pivot_json.id;
          $scope.pivot_table = pivot_json;
          $("#add-table-modal").modal({ show:true, keyboard:false, backdrop:'static' });
        }
      });
    }
  }
  $scope.edit_chart = function(){
    $scope.select_chart_type = $rootScope.current_selected_div.select_chart_type;
    $("#add-charts-modal").modal({ show:true, keyboard:false, backdrop:'static' });
  }
  // ENDS CODE OF EDIT WIDGETS
});