var vis_engine = angular.module('vis_engine',['ngAnimate', 'ui.bootstrap','angular-echarts','flowChart','gridster','ngFileUpload','ui.grid', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.selection', 'ui.grid.moveColumns', 'ui.grid.exporter', 'ui.grid.importer', /*'ui.grid.grouping'*/,'ui.grid.autoResize'])
.run(function($rootScope,$http,menuService,uiGridConstants) {
  $rootScope.current_selected_div = {};
  if(pagetype){
    $rootScope.pagetype = pagetype;
  }
  $rootScope.measures_items = [];
  $rootScope.dimensions_items = [];
  $rootScope.menu_items = menuService.get_menus(); // SET VARIABLE FOR MENUS
  $rootScope.gridsterOpts = {
    columns: 6, // the width of the grid, in columns
    pushing: true, // whether to push other items out of the way on move or resize
    floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
    swapping: false, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
    width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
    colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
    rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
    margins: [10, 10], // the pixel distance between each widget
    outerMargin: true, // whether margins apply to outer edges of the grid
    isMobile: false, // stacks the grid items if true
    mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
    mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
    minColumns: 1, // the minimum columns the grid must have
    minRows: 2, // the minimum height of the grid, in rows
    maxRows: 100,
    defaultSizeX: 2, // the default width of a gridster item, if not specifed
    defaultSizeY: 1, // the default height of a gridster item, if not specified
    minSizeX: 1, // minimum column width of an item
    maxSizeX: null, // maximum column width of an item
    minSizeY: 1, // minumum row height of an item
    maxSizeY: null, // maximum row height of an item
    resizable: {
       enabled: false,
       handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
       start: function(event, $element, widget) {}, // optional callback fired when resize is started,
       resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
    },
    draggable: {
       enabled: false, // whether dragging items is supported
       handle: '.my-class', // optional selector for resize handle
       start: function(event, $element, widget) {}, // optional callback fired when drag is started,
       drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
    }
  };

  var data = [];
  $rootScope.gridOptions = {
    showGridFooter: true,
    showColumnFooter: true,
    enableFiltering: true,
    exporterMenuCsv: false,
    enableGridMenu: true,
    headerTemplate:'/ui-grid/header-template.html',
    /*columnDefs: [
        { field: 'name', width: '13%' },
        { field: 'address.street',aggregationType: uiGridConstants.aggregationTypes.sum, width: '13%' },
        { field: 'age', aggregationType: uiGridConstants.aggregationTypes.avg, aggregationHideLabel: true, width: '13%' },
        { name: 'ageMin', field: 'age', aggregationType: uiGridConstants.aggregationTypes.min, width: '13%', displayName: 'Age for min' },
        { name: 'ageMax', field: 'age', aggregationType: uiGridConstants.aggregationTypes.max, width: '13%', displayName: 'Age for max' },
        { name: 'customCellTemplate', field: 'age', width: '14%', footerCellTemplate: '<div class="ui-grid-cell-contents" style="background-color: Red;color: White">custom template</div>' },
        { name: 'registered', field: 'registered', width: '20%', cellFilter: 'date', footerCellFilter: 'date', aggregationType: uiGridConstants.aggregationTypes.max }
    ],*/
    // data: data,
    onRegisterApi: function(gridApi) {
      $rootScope.gridApi = gridApi;
    }
  };
  //This function will be used for user menus permissions
  $rootScope.get_permissions = function(){
    $rootScope.pagetype = pagetype;
    $rootScope.menu_items = menuService.get_menus();
    $rootScope.permissions = [];
    if(role == 'admin'){ //if there is no client_id it means this is admin so allowed all permissions
      $rootScope.allowed = {view:1,add:1,edit:1,'delete':1,suspend:1};
    }else{
      $rootScope.allowed = {};
      $http.get('/users/api_get_permissions/'+userid).success(function(data){
        if(data['status'] == 200){
          angular.copy(angular.fromJson(data['data'].permissions),$rootScope.permissions);
          var found_permission = $.grep($rootScope.permissions, function(e){ return e.pagename == pagetype; });
          $rootScope.allowed = found_permission[0];
          for (var i = 0; i < $rootScope.menu_items.length; i++) {
            $rootScope.menu_items[i].hide = true;
            var found_func = $.grep($rootScope.permissions, function(e){ return e.functname == $rootScope.menu_items[i].menu; });
            if(found_func.length){
              $rootScope.menu_items[i].hide = false;
            }
            if($rootScope.menu_items[i].submenu && $rootScope.menu_items[i].submenu.length){
              for (var j = 0; j < $rootScope.menu_items[i].submenu.length; j++){
                $rootScope.menu_items[i].submenu[j].hide = true;
                var found_subfunc = $.grep($rootScope.permissions, function(e){ return e.functname == $rootScope.menu_items[i].submenu[j].functname; });
                if(found_subfunc.length){
                  $rootScope.menu_items[i].submenu[j].hide = false;
                }
              }
            }
          };
        }else{
          $rootScope.err_message=data['msg'];
        }
      })
    }
  }
  $rootScope.dashboard_list = [];
  $http.get('/dashboard/api_get_dashboard_list/').success(function(data){
    if(data['status'] == 200){
      angular.copy(angular.fromJson(data.data),$rootScope.dashboard_list);
      $rootScope.edit_gridster = false;
    }else{
      $rootScope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
    }
  })
  //--------End of the menus permissions function----------//

  $rootScope.enable_gridster = function($event){
    $event.preventDefault();
    $rootScope.gridsterOpts.resizable = {
       enabled: true,
       handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
       start: function(event, $element, widget) {}, // optional callback fired when resize is started,
       resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
    },
    $rootScope.gridsterOpts.draggable = {
       enabled: true, // whether dragging items is supported
       handle: '.my-class', // optional selector for resize handle
       start: function(event, $element, widget) {}, // optional callback fired when drag is started,
       drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
       stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
    }
  }

});
// DIRECTIVE FOR GENERATE HTML
vis_engine.directive('dynamic', function ($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
});
// DIRECTIVE FOR RESIZE DIV CONTENT
vis_engine.directive('resize', function ($window) {
  return function (scope, element,obj) {
    /*var parent_height = element.context.parentNode.parentNode.parentNode.offsetHeight;
    $(".grid").css("height",parent_height-100);*/
    element.height("98%");
    var w = element;
    scope.getWindowDimensions = function () {
      return { 'h': w.height(), 'w': w.width() };
    };
    scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
      scope[obj.arr][obj.resize].height = newValue.h;
      scope[obj.arr][obj.resize].width = newValue.w;
      if(scope[obj.arr][obj.resize].chart_data && scope[obj.arr][obj.resize].chart_data.chart_config){
        // console.log("Something coming : ",scope[obj.arr][obj.resize].chart_data)
        scope[obj.arr][obj.resize].chart_data.chart_config.options.height = newValue.h-50;
        scope[obj.arr][obj.resize].chart_data.chart_config.options.width = newValue.w-20;
        if(newValue.w > 500 &&scope[obj.arr][obj.resize].chart_data.type == 'pie'){
          scope[obj.arr][obj.resize].chart_data.chart_config.options.showLegend = true;
        }else{
          scope[obj.arr][obj.resize].chart_data.chart_config.options.showLegend = false;
        }
      }
      scope.style = function () {
        return { 
          'height': (newValue.h - 100) + 'px',
          'width': (newValue.w - 100) + 'px' 
        };
      };
    }, true);
  
    w.bind('resize', function () {
      scope.$apply();
    });
  }
})
// FILTER FOR CHECKING EMPTY OBJECT
vis_engine.filter('isEmpty', function () {
  var bar;
  return function (obj) {
      for (bar in obj) {
          if (obj.hasOwnProperty(bar)) {
              return false;
          }
      }
      return true;
  };
});
// Filter FOR EXCLUDING DUPLICATE OPTIONS
vis_engine.filter('exclude', [function () {
  return function(input,check_value,key){
    var newInput = [];
    if(input){
      for(var i = 0; i < input.length; i++){
        if(typeof input[i].params[key] !=='undefined' && input[i].params[key] !== check_value){
          newInput.push(input[i]);
        }
      }
      return newInput;
    }
  };
}]);
