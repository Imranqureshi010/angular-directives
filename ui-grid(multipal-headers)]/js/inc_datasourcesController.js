vis_engine.controller('DatasourcesCtrl', function ($scope,$http) {
  $scope.list = [
    {
      name: 'Years',
      children: [
        {
          name: '2013',
        },
        {
          name: '2014',
        },
        {
          name: '2015',
        }
      ]
    },
    {
      name: 'Branches',
      children: [
        {
          name: 'India',
          children: [
            {
              name: 'New Dehli',
            },
            {
              name: 'Mumbai',
            },
          ]
        },
        {
          name: 'USA',
          children: [
            {
              name: 'Alabama',
            },
            {
              name: 'Alaska',
            }
          ]
        }
      ]
    },
    {
      name: 'Products',
      children: [
        {
          name: 'Products 1',
        },
        {
          name: 'Products 2',
        }
      ]
    },
    {
      name: 'Devisions',
      children: [
        {
          name: 'Devisions 1',
        },
        {
          name: 'Devisions 2',
        }
      ]
    },
    {
      name: 'Revenue',
      children: [
       {
          name: '13,500',
        },
        {
          name: '19,999',
        },
        {
          name: '42,295',
        }
      ]
    }
  ];
  $scope.set_tab = function(tab) {
    $scope.current_tab = tab;
  };
  // ----------------- START CODE OF DATA SOURCES --------------------------//
  $scope.get_data_source_list = function(){
    $http.get('/database_config/api_get_data_sources_list').success(function(data){
      if(data.status == 200){
        $scope.all_data_source = data.data;
      }else{
        $scope.err_msg = "There is some error to get all data from server"+data.data;
      }
    })
  }
  $scope.show_delete_datasource_modal = function($event,datasource){
    $event.preventDefault();
    if(datasource){
      $scope.current_datasource = datasource;
      $('#delete-datasource-modal').modal({ show:true, backdrop:'static', keyboard:true });
    }
  }
  $scope.deleteDatasourceLabel = false;
  $scope.delete_data_source = function(){
    $scope.deleteDatasourceLabel = true;
    if($scope.current_datasource){
      $http({method:'POST', url:"/database_config/api_delete_data_sources/", data:$scope.current_datasource}).
        success(function (data) {
          if(data.status == 200){
            for (var i = 0; i < $scope.all_data_source.length; i++) {
              if($scope.all_data_source[i].id == $scope.current_datasource.id){
                $scope.all_data_source.splice(i,1);
              }
            };
            $scope.deleteDatasourceLabel = false;
            $('#delete-datasource-modal').modal('hide');
          }
        }).
        error(function (data, status, headers, config) {
          console.log("There is some error in delete data source !",data.data);
        }
      );
    }
  }
  $scope.clone_data = {};
  $scope.show_configure_clone_modal = function($event,data_source){
    $event.preventDefault();
    $scope.clone_data = {};
    $scope.datasource_clone_save_label = "Clone";
    $scope.current_clone_datasource = data_source;
    $('#clone-customer-modal').modal({ show:true, backdrop:'static', keyboard:true });
  }
  $scope.datasource_clone_save_label = "Clone";
  $scope.clone_datasource = function(){
    if($scope.clone_data){
      data = {
        datasource_id:$scope.current_clone_datasource.id,
        datasource_name:$scope.clone_data.clone_data_name
      }
      $scope.datasource_clone_save_label = "Submitting...";
      delete data.id;
      $http({method:'POST', url:"/database_config/api_clone_datasource/", data:data}).
        success(function (data) {
          if(data.status == 200){
            $scope.all_data_source.push(data.data);
            for (var i = 0; i < $scope.all_data_source.length; i++) {
              if($scope.all_data_source[i].id == data.data.id){
                $scope.all_data_source[i].active = true;
              }else{
                $scope.all_data_source[i].active = false;
              }
            };
            $('#clone-customer-modal').modal('hide');
          }
        }).
        error(function (data, status, headers, config) {
          console.log("There is some error in delete data source !",data.data);
        }
      );
    }
  }
  $scope.node_table_structure = {};
  $scope.node_table_preview = {};
  $scope.get_data_source_table_structure_and_preview = function(node_obj){
    if(node_obj.params.tableName){
      $scope.current_node.last_run_status='Working';
      $scope.current_node.last_run_message='Please Wait! We are trying to fetch table structure';
      var post_obj = {
        databaseName:node_obj.params.data_source_name,
        tableName:node_obj.params.tableName,
      }
      $http({method:'POST',url:'/database_config/api_get_data_source_table_structure_and_preview/',data:post_obj})
      .success(function(data){
        $scope.current_node.last_run_message=data.message;
        if(data.status == 200){
          $scope.current_node.last_run_status='Done';
          $scope.node_table_structure['node'+node_obj.id] = []
          $scope.node_table_preview['node'+node_obj.id] = []
          angular.copy(angular.fromJson(data.tableStructure),$scope.node_table_structure['node'+node_obj.id])
          angular.copy(angular.fromJson(data.data),$scope.node_table_preview['node'+node_obj.id])
        }else{
          $scope.current_node.last_run_status='Error';
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      .error(function(data){
        $scope.current_node.last_run_status='Error';
        // alert("There is some error in get_datanode_tables ");
      });  
    }
  }
  $scope.get_last_node_table_preview = function(dataSourceId){
    if(dataSourceId){
      $http.get('/database_config/api_get_data_sources/'+dataSourceId).success(function (data){
        if(data.status == 200){
          console.log('data.data is :-========',data.data);
          for (var i = 0; i < $scope.all_data_source.length; i++) {
            if($scope.all_data_source[i].id == dataSourceId){
              $scope.all_data_source[i].active = true;
            }else{
              $scope.all_data_source[i].active = false;
            }
          };
          var json_obj = JSON.parse(data.data.json_obj);
          var lastNodeIndex = json_obj.nodes.length-1;
          $scope.current_node = json_obj.nodes[lastNodeIndex];
          console.log('$scope.current_node is :-========',$scope.current_node);
          $scope.get_data_source_table_structure_and_preview($scope.current_node);
        }else{
          Console.log("There is some error in get data of datasource!",data.err);
        }
      });
    }
  }
  // ----------------- ENDS CODE OF DATA SOURCES --------------------------//
});