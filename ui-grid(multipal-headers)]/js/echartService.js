vis_engine.service('echartService', function(){
    this.piechart= function(data){
    	var chart_obj = {}
      chart_obj.options = get_options(data);
      chart_obj.options.showLegend = true;
      chart_obj.data = data.data;
      return chart_obj;
    },
    this.barchart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.areachart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.linechart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.donutchart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.options.showLegend = true;
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.gaugechart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.scatterchart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.lasagna_pie_chart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.options.showLegend = true;
        chart_obj.options.support_type = 'lasagna';
        chart_obj.data = data.data;
        return chart_obj;
    },
    this.nested_pie_chart= function(data){
        var chart_obj = {}
        chart_obj.options = get_options(data);
        chart_obj.options.showLegend = true;
        chart_obj.options.radius = [[0,70],[100,140]];
        chart_obj.data = data.data;
        return chart_obj;
    }
   
});

function get_options(data){
  return {
    title: '',
    subtitle: '',
    label:true,
    width:data.width?data.width:'',
    debug: true,
    showXAxis: true,
    showYAxis: true,
    showLegend: data.showLegend?data.showLegend:false,
    stack: false,
    height:data.height?data.height:'',
    calculable : false,
    toolbox: {
      show : true,
      padding : [0,20,0,0],
      feature : {
        mark : {show: true},
        dataView : {show: true, readOnly: false},
        magicType : {show: true, type: [/*'line','bar'*/]},
        restore : {show: true},
        saveAsImage : {show: true}
      }
    },
  };
}