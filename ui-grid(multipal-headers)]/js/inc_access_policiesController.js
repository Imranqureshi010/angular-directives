vis_engine.controller('Access_policiesCtrl', function ($scope,$http,menuService){
  $scope.accesslevels = [];
  $scope.new_accesslevels = {
    levelname:'',
    permissions:menuService.get_menus()
  };
  $scope.current_accesslevel = {};
  $scope.get_accesslevels_list = function(){
    $http.get('/access_policies/api_accesslevels_list/').success(function(data){
      if(data['status']==200){
        angular.copy(angular.fromJson(data['data']), $scope.accesslevels);
      }else{
        $scope.error = "There is some error in get accesslevels list"+data['msg'];
      }
    });
  }
  $scope.dontHavePermission  = function($event){
    $event.preventDefault();
    alert("You don't have permission to add a new system role !");
  }
  $scope.show_add_accesslevel = function($event){
    $event.preventDefault();
    $scope.new_accesslevels = {
        levelname:'',
        permissions:menuService.get_menus()
      };
    $scope.current_accesslevel = {};
    $scope.levelNotAvailable = false;
    $scope.save_accesslevel_label = "Save";
    $('#modal-system_role-add').modal({ show: true, backdrop: 'static', keyboard: true });
  }
  $scope.show_edit_accesslevel = function($event,accesslevel){
    $event.preventDefault();
    $scope.save_accesslevel_label = "Save";
    $scope.new_accesslevels = {};
    $scope.levelNotAvailable = false;
    $scope.new_accesslevels = JSON.parse(accesslevel.json_obj); 
    $scope.new_accesslevels.levelid = accesslevel.levelid;
    $('#modal-system_role-add').modal({ show: true, backdrop: 'static', keyboard: true });
  }
  $scope.delete_confirm = function($event,accesslevel){
    $event.preventDefault();
    $scope.current_accesslevel = accesslevel;
    $('#modal-system_role-delete').modal({ show: true, backdrop: 'static', keyboard: true });
  }
  $scope.save_accesslevel = function(){
    if($scope.new_accesslevels){
      $scope.save_accesslevel_label = "Submitting...";
      $http({method:"POST", url:"/access_policies/api_save_accesslevel/", data:$scope.new_accesslevels}).
      success(function(data){
        if(data.status == 200){
          if($scope.new_accesslevels.levelid){
            for (var i = 0, ii = $scope.accesslevels.length; i < ii; i++) {
              if ($scope.new_accesslevels.levelid == $scope.accesslevels[i].levelid) {
                $scope.accesslevels[i] = data['data'][0];
                break;
              }
            }
          }else{
            $scope.accesslevels.push(data['data']);
          }
          $('#modal-system_role-add').modal('hide');
        }else{
          $scope.error="There was some error in save accesslevel" + data['msg'];
        }
      })
      .error(function(data){
        $scope.error="There was some error in save accesslevel" + data['msg'];
      });
    }
  }
  $scope.delete_accesslevel = function(){
    if($scope.current_accesslevel){
      $http({method:'POST', url:"/access_policies/api_delete/", data:$scope.current_accesslevel}).
        success(function (data) {
          if(data['status']=="200"){
            for (var i = 0, ii = $scope.accesslevels.length; i < ii; i++) {
              if ($scope.current_accesslevel === $scope.accesslevels[i]) {
                $scope.accesslevels.splice(i, 1);
                $("#modal-system_role-delete").modal('hide');
                break;
              }
            }
          }else{
            $scope.error="There was some error in delete accesslevel" + data['msg'];
          }
        }).
        error(function (data, status, headers, config) {
          $scope.error="There was some error in delete accesslevel" + data['msg'];
        }
      );
    }
  }
  $scope.reset_permissions = function(menu){
    if(menu.view == 0){
      menu.add = 0;
      menu.edit = 0;
      menu.delete = 0;
    }
  }
  $scope.levelNotAvailable = false;
  $scope.check_levelname = function(levelid,levelname){
    $http.get('/access_policies/api_accesslevels_list/').success(function(data){
      if(data['status']==200){
        $scope.load_error=false;
        var found = $.grep(data.data, function(e){ return e.levelname == levelname && e.levelid != levelid });
        if(found.length){
          $scope.levelNotAvailable = true;
        }else{
          $scope.levelNotAvailable = false;
        }
      }else{
        $scope.load_error=true;
        $scope.err_message=data['msg'];
      }
    });
  }
});