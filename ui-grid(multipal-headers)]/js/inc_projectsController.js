vis_engine.controller('ProjectsCtrl',function ($scope,$rootScope,$http,uiGridConstants,$timeout){
  $http.get('https://cdn.rawgit.com/angular-ui/ui-grid.info/gh-pages/data/500_complex.json')
  .success(function(data) {
    data.forEach( function(row) {
      row.registered = Date.parse(row.registered);
    });
    $scope.gridOptions.data = data;
  });
	$scope.projects = [];
  $scope.new_projects = {};
  $scope.current_projects = {};
  $scope.get_projects_list = function(){
  	$http.get('/projects/api_projects_list/').success(function(data){
  		if(data.status == 200){
  			angular.copy(angular.fromJson(data.data), $scope.projects);
  		}else{
  			console.log("There is some error in get all projects",data['msg']);
  		}
  	});
  }
  $scope.show_add_modal = function($event){
    $event.preventDefault();
    $scope.node_table_structure = {};
    $scope.node_table_preview = {};
    $scope.new_projects = {dimensions:[]};
    $scope.save_projects_label = "save";
    $('#modal-project-add').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.show_edit_modal = function($event,project){
    $event.preventDefault();
    $scope.node_table_structure = {};
    $scope.node_table_preview = {};
    $scope.save_projects_label = "save";
    $scope.new_projects = {};
    if(project){
      $scope.get_datasource_json(project.datasourceid);
      $timeout(function() {
        project.dimensions = JSON.parse(project.json_obj);
        if($scope.node_table_structure['node'+$scope.current_node.id]){
          for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
            var found = $.grep(project.dimensions, function(e){ return e.fieldName == $scope.node_table_structure['node'+$scope.current_node.id][i].fieldName });
            if(found.length){
              $scope.node_table_structure['node'+$scope.current_node.id][i].added = true;
            }
          };
        }
        angular.copy(angular.fromJson(project),$scope.new_projects);
      }, 1000)
      angular.copy(angular.fromJson(project),$scope.new_projects);
    }
    $('#modal-project-add').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.show_delete_modal = function($event,project){
    $event.preventDefault();
    $scope.current_projects = project;
    $('#modal-project-delete').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.save_projects = function(){
  	if($scope.new_projects){
      
      if($scope.dummy_dimensions){
        $scope.new_projects.json_obj = $scope.dummy_dimensions;
      }
  		$http({ method:'POST', url:'/projects/api_save_projects/', data:$scope.new_projects}).
  		success(function(data){
  			if(data.status == 200){
  				if($scope.new_projects.projectid){
  					for (var i = $scope.projects.length - 1; i >= 0; i--) {
  						if($scope.projects[i].projectid == $scope.new_projects.projectid){
  							console.log('data[0]  is:', data['data']);
  							$scope.projects[i] = data['data'];
  						}
  					};
  				}else{
  					$scope.projects.push(data.data);
  				}
  				$('#modal-project-add').modal('hide');
  			}else{
  				$scope.error = "There is some in save projects :" + data['msg'];
  			}
  		})
  		.error(function(data){
  			$scope.error = "There is some error in save new Projects" + data;
  		});
  	}
  }
  $scope.delete_projects = function(){
  	if($scope.current_projects){
  		$http({ method:'POST', url:'/projects/api_delete/', data:$scope.current_projects})
  		.success(function (data){
  			if(data.status == 200){
  				index = $scope.projects.indexOf($scope.current_projects);
  				$scope.projects.splice(index,1);
  				console.log('Delete projects successfully');
  				$('#modal-project-delete').modal('hide');
  			}else{
  				console.log("There is some error in delete projects :" + data['msg']);
  			}
  		})
  		.error(function (data){
  			$scope.error = "There is some error in delete projects :" + data;
  		});
  	}
  }
  // ------------------START CODE OF DATASOURCE------------------------------//
  $scope.all_data_source = [];
  $scope.get_data_source_list = function(){
    $http.get('/database_config/api_get_data_sources_list').success(function(data){
      if(data.status == 200){
        $scope.all_data_source = data.data;
      }else{
        $scope.err_msg = "There is some error to get all data from server"+data.data;
      }
    })
  }
  $scope.get_datasource_json = function(dataSourceId){
    if(dataSourceId){
      $http.get('/datasets/api_get_datasource_json/'+dataSourceId).success(function(data){
        if(data.status == 200){
          var json = JSON.parse(data.data.json_obj);
          var length = json.nodes.length-1;
          $scope.current_node = json.nodes[0];
          $scope.get_data_source_table_structure_and_preview($scope.current_node);
        }
      });
    }
  }
  $scope.node_table_structure = {};
  $scope.node_table_preview = {};
  $scope.get_data_source_table_structure_and_preview = function(node_obj){
    if(node_obj.params.tableName){
      $scope.current_node.last_run_status='Working';
      $scope.current_node.last_run_message='Please Wait! We are trying to fetch table structure';
      var post_obj = {
        databaseName:node_obj.params.data_source_name,
        tableName:node_obj.params.tableName,
      }
      $http({method:'POST',url:'/database_config/api_get_data_source_table_structure_and_preview/',data:post_obj})
      .success(function(data){
        $scope.current_node.last_run_message=data.message;
        // console.log('table structure is :==========',data);
        if(data.status == 200){
          $scope.current_node.last_run_status='Done';
          $scope.node_table_structure['node'+node_obj.id] = []
          $scope.node_table_preview['node'+node_obj.id] = []
          angular.copy(angular.fromJson(data.tableStructure),$scope.node_table_structure['node'+node_obj.id])
          angular.copy(angular.fromJson(data.data),$scope.node_table_preview['node'+node_obj.id])
        }else{
          $scope.current_node.last_run_status='Error';
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
        }
      })
      .error(function(data){
        $scope.current_node.last_run_status='Error';
        alert("There is some error in get_datanode_tables ");
      });  
    }
  }

  // START CODE OF SET ACTUAL DATA FOR USED DIMENSIONS IN POPUP
    $scope.set_used_dimensions = function(select_dimensions,dimensions){
      if(select_dimensions){
        $scope.new_projects.dimensions.push(dimensions);
        for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
          if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == dimensions.fieldName){
            $scope.node_table_structure['node'+$scope.current_node.id][i].added = true;
          }
        };
      }else{
        var index = $scope.new_projects.dimensions.indexOf(dimensions);
        $scope.new_projects.dimensions.splice(index,1);
        for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
          if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == dimensions.fieldName){
            $scope.node_table_structure['node'+$scope.current_node.id][i].added = false;
          }
        };
      }
    }
    $scope.remove_used_dimensions = function(dimensions){
      var index = $scope.new_projects.dimensions.indexOf(dimensions);
      $scope.new_projects.dimensions.splice(index,1);
      for (var i = 0; i < $scope.node_table_structure['node'+$scope.current_node.id].length; i++) {
        if($scope.node_table_structure['node'+$scope.current_node.id][i].fieldName == dimensions.fieldName){
          $scope.node_table_structure['node'+$scope.current_node.id][i].added = false;
        }
      };
    }
  // ENDS CODE OF SET ACTUAL DATA FOR USED DIMENSIONS IN POPUP


  // START CODE OF SET DUMMY DATA FOR USED DIMENSIONS IN POPUP
    $scope.get_corresponding_table = function($event,filed){
      $event.preventDefault();
      if(filed){
        var index = $scope.node_table_structure['node'+$scope.current_node.id].indexOf(filed);
        $scope.node_table_structure['node'+$scope.current_node.id][index].add_properties = true;
        $scope.node_table_structure['node'+$scope.current_node.id][index].add_used_property = true;
        $scope.node_table_structure['node'+$scope.current_node.id][index].properties = $scope.node_table_structure['node'+$scope.current_node.id];

        // QUERY FOR GET CORRESPONDING TABLE STRUCTURE BASED ON FIELD NAME ...
        /*select TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME,
        REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME from KEY_COLUMN_USAGE
        where TABLE_SCHEMA = "test_delete" and TABLE_NAME = "orders" and COLUMN_NAME = 'customer_id'
        and referenced_column_name is NOT null*/
      }
    }
    $scope.remove_added_dimensions = function(index,field){
      $scope.dummy_dimensions[index].add_properties = false;
      $scope.dummy_dimensions[index].added = false;
      for (var i = 0; i < field.properties.length; i++) {
        field.properties[i].added = false;
      };
    }
    $scope.dummy_dimensions = [
      {
        fieldName: "user_id",
        fieldType: "bignit(21)",
        properties:[
          {
            fieldName: "id",
            fieldType: "int(11)"
          },
          {
            fieldName: "name",
            fieldType: "varchar(255)"
          },
          {
            fieldName: "gender",
            fieldType: "varchar(255)"
          }
        ]
      },
      {
        fieldName: "customer_id",
        fieldType: "varchar(255)",
        properties:[
          {
            fieldName: "id",
            fieldType: "int(11)"
          },
          {
            fieldName: "customer_name",
            fieldType: "varchar(255)"
          },
          {
            fieldName: "gender",
            fieldType: "varchar(255)"
          }
        ]
      },
      {
        fieldName: "Amount",
        fieldType: "int(11)",
        properties:[
          {
            fieldName: "id",
            fieldType: "int(11)"
          },
          {
            fieldName: "amount_type",
            fieldType: "varchar(255)"
          }
        ]
      },
    ]
  // ENDS CODE OF SET DUMMY DATA FOR USED DIMENSIONS IN POPUP

  // ------------------ENDS CODE OF DATASOURCE------------------------------//
});