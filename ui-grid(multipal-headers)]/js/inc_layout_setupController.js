vis_engine.controller('Layout_setupCtrl',function ($scope,$rootScope,$http,echartService){
  /*$scope.chart_widgetsMap = {
    sizeX: 'item.size.x',
    sizeY: 'item.size.y',
    row: 'item.position[0]',
    col: 'item.position[1]',
    minSizeY: 'item.minSizeY',
    maxSizeY: 'item.maxSizeY',
  };*/
  $scope.chart_widgets = [
    { size: { x: 3, y: 2 }, position: [0, 0] },
    { size: { x: 2, y: 2 }, position: [0, 0] },
    { size: { x: 2, y: 2 }, position: [0, 0] },
  ];

  $scope.add_widget = function(){
    $scope.chart_widgets.push({ size: { x: 2, y: 2 }, position: [0, 0] });
  }
  $scope.remove_widget = function(widget){
    $scope.chart_widgets.splice($scope.chart_widgets.indexOf(widget), 1);
  }
  if(dataset_id){
    $scope.dataset_id = dataset_id;
  }
  $scope.current_div = {};
  $scope.dataset_charts = [];
  $scope.show_chart_list = function(div){
    $scope.chart_config = "";
    $scope.chart = "";
    $scope.current_div = div;
    $http.get('/datasets/api_get_dataset_chart_list/'+$scope.dataset_id).success(function(data){
      if(data.status == 200){
        angular.copy(angular.fromJson(data.data),$scope.dataset_charts)
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
    $("#add-chart-modal").modal("show");
  }
  $scope.datasets = [];
  $scope.show_dataset_list = function(){
    $http.get('/datasets/api_get_dataset_list/').success(function(data){
      if(data.status == 200){
        angular.copy(angular.fromJson(data.data),$scope.datasets)
      }else{
        $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
      }
    })
  }
  $scope.current_selected_chart = {};
  $scope.show_chart_preview = function(dataset){
    if(dataset){
      $scope.current_selected_chart = dataset;
      $scope.chart_config = dataset.chart_config;
      $scope.chart_config.options.noToolbox = true;
      // $scope.chart_config.options.height = 300;
      $scope.chart = '<'+$scope.chart_config.type+'-chart config="chart_config.options" data="chart_config.data"></'+$scope.chart_config.type+'-chart>'
    }
  }
  $scope.add_chart_in_div = function(){
    $scope.current_div.chart_data = $scope.current_selected_chart;
    $scope.current_div.chart_data.chart_config.options.height=$scope.current_div.height-20;//This height should be same as Widget height
    $scope.current_div.chart_data.chart_config.options.width=$scope.current_div.width; //This width should be same as Widget width
    $scope.current_div.chart_data.chart_config.options.showLegend = false;
    $scope.current_div.chart_data.chart_config.options.noToolbox = true;
    $scope.current_div.chart = '<'+$scope.chart_config.type+'-chart config="div.chart_data.chart_config.options" data="div.chart_data.chart_config.data"></'+$scope.chart_config.type+'-chart>';
    $("#add-chart-modal").modal("hide");
  }
  $scope.save_dashboard = function(){
    $scope.new_dashboard.chart_data = $scope.chart_widgets;
    $http({method:"POST",url:"/dashboard/api_save_dashboard/",data:$scope.new_dashboard}).
    success(function(data){
      location.href = '/dashboard/view/'+data['data'].id;
    })
    .error(function(data){
      alert("There is some error on dashboard save")
    })
  }
});