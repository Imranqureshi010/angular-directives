vis_engine.service('node_paramService', function(){
  this.get_database_types = function(){
    return [
      {
        db_cd:"mysql",
        db_label:"MySQL",
        db_type:"rdbms",
        db_desc:"Configure and Connect to your MySql database.",
        db_img:"icon_mysql.png",
        db_class:"img-circle",
        disable:false,
      },
      {
        db_cd:"csv",
        db_label:"CSV files",
        db_type:"file",
        db_desc:"Import a csv file and get the list of data.",
        db_img:"icon_csv.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"db2",
        db_label:"DB2",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"sybase",
        db_label:"Sybase",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"sap",
        db_label:"Sap",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"sqlite",
        db_label:"Sqlite",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"h2",
        db_label:"H2",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"PostgreSQL",
        db_label:"PostgreSQL",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_postgresql.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"Oracle DB",
        db_label:"Oracle DB",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_oracle.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"SQL Server",
        db_label:"SQL Server",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_sqlserver.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"Apache Hive",
        db_label:"Apache Hive",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_hive.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"Embedded DBs",
        db_label:"Embedded DBs",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_embedded_dbs.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"nosql database",
        db_label:"NoSQL Database",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_nosql_dbs.png",
        db_class:"img-thumbnail",
        disable:true,
      },
      {
        db_cd:"CouchDB",
        db_label:"CouchDB",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_couchdb.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"MongoDB",
        db_label:"MongoDB",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_mongodb.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"HBase",
        db_label:"HBase",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_hbase.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"Cassandra",
        db_label:"Cassandra",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_cassandra.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"ElasticSearch",
        db_label:"ElasticSearch",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_elasticsearch.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"business apps",
        db_label:"Business Apps",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_business_apps.png",
        db_class:"img-thumbnail",
        disable:true,
      },
      {
        db_cd:"Salesforce.com",
        db_label:"Salesforce.com",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_salesforce.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"SugarCRM",
        db_label:"SugarCRM",
        db_type:"rdbms",
        db_desc:"",
        db_img:"icon_sugarcrm.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"data files formates",
        db_label:"Data Files + Formates",
        db_type:"file",
        db_desc:"",
        db_img:"icon_data_files.png",
        db_class:"img-thumbnail",
        disable:true,
      },
      {
        db_cd:"xls",
        db_label:"Spreadsheets",
        db_type:"file",
        db_desc:"Import a xls file and get the list of data.",
        db_img:"icon_excel.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"XML files",
        db_label:"XML Files",
        db_type:"file",
        db_desc:"Import a xml file and get the list of data.",
        db_img:"icon_xml.png",
        db_class:"img-circle",
        disable:true,
      },
      {
        db_cd:"JSON files",
        db_label:"JSON Files",
        db_type:"file",
        db_desc:"Import a json file and get the list of data.",
        db_img:"icon_json.png",
        db_class:"img-circle",
        disable:true,
      }
    ]
  },
  this.get_operation_types = function(){
    return [
      {
        oper_cd:"join",
        oper_label:"Join",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"split",
        oper_label:"Split",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"merge",
        oper_label:"Merge",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"aggregate",
        oper_label:"Aggregate",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"concatenate",
        oper_label:"Concatenate",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"reformat",
        oper_label:"Reformat",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"copy-data",
        oper_label:"Copy Data",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"duplicate",
        oper_label:"Duplicate",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"pivot",
        oper_label:"Pivot",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"partition",
        oper_label:"Partition",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
      {
        oper_cd:"sorting",
        oper_label:"Sorting",
        oper_icon:"fa-bitbucket",
        disable:false,
      },
    ]
  },
  this.get_params= function(obj,type){
    var params={};
    switch(type){
      case 'mysql':
        params={
          node_name:obj.db_label,
          type:obj.db_cd,
          /*host:"ensembldb.ensembl.org",
          port:3306,
          user:"anonymous",
          password:"",*/
          host:"192.168.0.4",
          port:3306,
          user:"hduser",
          password:"hadoop@123",
        };    
        break;
      case 'join':
        params={
          node_name:obj.oper_label,
          tableName:'',
          type:obj.oper_cd,
        };    
        break;
      case 'split':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          table_columns:[{column_name:''}],
        };    
        break;
      case 'merge':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          merge_table:'',
        };    
        break;
      case 'aggregate':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          aggregate_columns:[{
            column_name:'',
            operator:'',
            new_column_name:'',
          }],
          group_by_columns:[{
            column_name:'',
          }]
        };    
        break;
      case 'concatenate':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          concat_columns:[{
            column_name:'',
            custom_text:''
          }],
          new_field_name:''
        };    
        break;
      case 'reformat':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          drop_conditions:[{
            column_name:'',
            condition:''
          }]
        };    
        break;
      case 'copy-data':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
        };    
        break; 
      case 'duplicate':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
        };    
        break;
      case 'pivot':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          pivot_columns:[{
            column_name:''
          }],
          pivot_rows:[{
            column_name:''
          }],
          pivot_aggregate_columns:[{
            column_name:''
          }]
        };    
        break;
      case 'partition':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          partition_column:'',
          new_table_prefix:'',
        };    
        break;
      case 'sorting':
        params={
          node_name:obj.oper_label,
          type:obj.oper_cd,
          tableName:'',
          sort_columns:[{
            column_name:'',
            sort_order:''
          }]
        };    
        break;
    }
    return params;
  },
  this.get_table_structure = function(){
    return {
      "node1": [
        {
          "fieldName": "density_feature_id",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "density_type_id",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "seq_region_id",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "seq_region_start",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "seq_region_end",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "density_value",
          "fieldType": "REAL"
        }
      ],
      "node2": [
        {
          "fieldName": "density_type_id",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "analysis_id",
          "fieldType": "SMALLINT"
        },
        {
          "fieldName": "block_size",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "region_features",
          "fieldType": "INTEGER"
        },
        {
          "fieldName": "value_type",
          "fieldType": "CHAR"
        }
      ]
    }
  },
  this.get_table_preview = function(){
    return {
      "node1": [
        {
          "row": [
            6802931,
            42,
            2771,
            2001,
            2537,
            100
          ]
        },
        {
          "row": [
            6802930,
            42,
            2771,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802929,
            42,
            2771,
            1,
            1000,
            38.7
          ]
        },
        {
          "row": [
            6802928,
            42,
            4116,
            1001,
            2000,
            95.7
          ]
        },
        {
          "row": [
            6802927,
            42,
            4116,
            1,
            1000,
            99.4
          ]
        },
        {
          "row": [
            6802926,
            42,
            1067,
            2001,
            2622,
            100
          ]
        },
        {
          "row": [
            6802925,
            42,
            1067,
            1001,
            2000,
            75.6
          ]
        },
        {
          "row": [
            6802924,
            42,
            1067,
            1,
            1000,
            64
          ]
        },
        {
          "row": [
            6802923,
            42,
            230,
            2001,
            2882,
            99.8866
          ]
        },
        {
          "row": [
            6802922,
            42,
            230,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802921,
            42,
            230,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802920,
            42,
            1786,
            2001,
            2939,
            100
          ]
        },
        {
          "row": [
            6802919,
            42,
            1786,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802918,
            42,
            1786,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802917,
            42,
            2026,
            2001,
            2972,
            87.1399
          ]
        },
        {
          "row": [
            6802916,
            42,
            2026,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802915,
            42,
            2026,
            1,
            1000,
            52.3
          ]
        },
        {
          "row": [
            6802914,
            42,
            1437,
            2001,
            2993,
            99.6979
          ]
        },
        {
          "row": [
            6802913,
            42,
            1437,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802912,
            42,
            1437,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802911,
            42,
            2730,
            3001,
            3012,
            58.3333
          ]
        },
        {
          "row": [
            6802910,
            42,
            2730,
            2001,
            3000,
            90.3
          ]
        },
        {
          "row": [
            6802909,
            42,
            2730,
            1001,
            2000,
            82
          ]
        },
        {
          "row": [
            6802908,
            42,
            2730,
            1,
            1000,
            99.8
          ]
        },
        {
          "row": [
            6802907,
            42,
            4608,
            2001,
            3000,
            14.9
          ]
        },
        {
          "row": [
            6802906,
            42,
            4608,
            1001,
            2000,
            66.2
          ]
        },
        {
          "row": [
            6802905,
            42,
            4608,
            1,
            1000,
            94.1
          ]
        },
        {
          "row": [
            6802904,
            42,
            684,
            3001,
            3044,
            100
          ]
        },
        {
          "row": [
            6802903,
            42,
            684,
            2001,
            3000,
            96.7
          ]
        },
        {
          "row": [
            6802902,
            42,
            684,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802901,
            42,
            684,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802900,
            42,
            151,
            3001,
            3084,
            100
          ]
        },
        {
          "row": [
            6802899,
            42,
            151,
            2001,
            3000,
            97.1
          ]
        },
        {
          "row": [
            6802898,
            42,
            151,
            1001,
            2000,
            43
          ]
        },
        {
          "row": [
            6802897,
            42,
            151,
            1,
            1000,
            87.2
          ]
        },
        {
          "row": [
            6802896,
            42,
            1975,
            3001,
            3176,
            82.3864
          ]
        },
        {
          "row": [
            6802895,
            42,
            1975,
            2001,
            3000,
            24.3
          ]
        },
        {
          "row": [
            6802894,
            42,
            1975,
            1001,
            2000,
            56.4
          ]
        },
        {
          "row": [
            6802893,
            42,
            1975,
            1,
            1000,
            47
          ]
        },
        {
          "row": [
            6802892,
            42,
            1329,
            3001,
            3233,
            47.2103
          ]
        },
        {
          "row": [
            6802891,
            42,
            1329,
            2001,
            3000,
            74.2
          ]
        },
        {
          "row": [
            6802890,
            42,
            1329,
            1001,
            2000,
            39.9
          ]
        },
        {
          "row": [
            6802889,
            42,
            1329,
            1,
            1000,
            78.8
          ]
        },
        {
          "row": [
            6802888,
            42,
            3006,
            3001,
            3234,
            97.8633
          ]
        },
        {
          "row": [
            6802887,
            42,
            3006,
            2001,
            3000,
            97.3
          ]
        },
        {
          "row": [
            6802886,
            42,
            3006,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802885,
            42,
            3006,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802884,
            42,
            3446,
            3001,
            3353,
            100
          ]
        },
        {
          "row": [
            6802883,
            42,
            3446,
            2001,
            3000,
            97
          ]
        },
        {
          "row": [
            6802882,
            42,
            3446,
            1001,
            2000,
            71.9
          ]
        },
        {
          "row": [
            6802881,
            42,
            3446,
            1,
            1000,
            13.6
          ]
        },
        {
          "row": [
            6802880,
            42,
            4311,
            3001,
            3381,
            100
          ]
        },
        {
          "row": [
            6802879,
            42,
            4311,
            2001,
            3000,
            100
          ]
        },
        {
          "row": [
            6802878,
            42,
            4311,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802877,
            42,
            4311,
            1,
            1000,
            70.9
          ]
        },
        {
          "row": [
            6802876,
            42,
            3807,
            3001,
            3381,
            27.0341
          ]
        },
        {
          "row": [
            6802875,
            42,
            3807,
            2001,
            3000,
            84.3
          ]
        },
        {
          "row": [
            6802874,
            42,
            3807,
            1001,
            2000,
            40.4
          ]
        },
        {
          "row": [
            6802873,
            42,
            3807,
            1,
            1000,
            88.1
          ]
        },
        {
          "row": [
            6802872,
            42,
            905,
            3001,
            3422,
            20.1422
          ]
        },
        {
          "row": [
            6802871,
            42,
            905,
            2001,
            3000,
            60.9
          ]
        },
        {
          "row": [
            6802870,
            42,
            905,
            1001,
            2000,
            76.3
          ]
        },
        {
          "row": [
            6802869,
            42,
            905,
            1,
            1000,
            52.1
          ]
        },
        {
          "row": [
            6802868,
            42,
            409,
            3001,
            3436,
            100
          ]
        },
        {
          "row": [
            6802867,
            42,
            409,
            2001,
            3000,
            100
          ]
        },
        {
          "row": [
            6802866,
            42,
            409,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802865,
            42,
            409,
            1,
            1000,
            99.8
          ]
        },
        {
          "row": [
            6802864,
            42,
            2312,
            3001,
            3466,
            100
          ]
        },
        {
          "row": [
            6802863,
            42,
            2312,
            2001,
            3000,
            100
          ]
        },
        {
          "row": [
            6802862,
            42,
            2312,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802861,
            42,
            2312,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802860,
            42,
            4062,
            3001,
            3473,
            80.9725
          ]
        },
        {
          "row": [
            6802859,
            42,
            4062,
            2001,
            3000,
            15.2
          ]
        },
        {
          "row": [
            6802858,
            42,
            4062,
            1001,
            2000,
            23.7
          ]
        },
        {
          "row": [
            6802857,
            42,
            3550,
            3001,
            3492,
            100
          ]
        },
        {
          "row": [
            6802856,
            42,
            3550,
            2001,
            3000,
            95.3
          ]
        },
        {
          "row": [
            6802855,
            42,
            3550,
            1001,
            2000,
            56.4
          ]
        },
        {
          "row": [
            6802854,
            42,
            3550,
            1,
            1000,
            38.9
          ]
        },
        {
          "row": [
            6802853,
            42,
            1465,
            3001,
            3514,
            27.2374
          ]
        },
        {
          "row": [
            6802852,
            42,
            1465,
            2001,
            3000,
            2.4
          ]
        },
        {
          "row": [
            6802851,
            42,
            1465,
            1,
            1000,
            41.9
          ]
        },
        {
          "row": [
            6802850,
            42,
            3308,
            3001,
            3525,
            84
          ]
        },
        {
          "row": [
            6802849,
            42,
            3308,
            2001,
            3000,
            98.8
          ]
        },
        {
          "row": [
            6802848,
            42,
            3308,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802847,
            42,
            3308,
            1,
            1000,
            100
          ]
        },
        {
          "row": [
            6802846,
            42,
            135,
            3001,
            3527,
            99.8102
          ]
        },
        {
          "row": [
            6802845,
            42,
            135,
            2001,
            3000,
            100
          ]
        },
        {
          "row": [
            6802844,
            42,
            135,
            1001,
            2000,
            93.3
          ]
        },
        {
          "row": [
            6802843,
            42,
            135,
            1,
            1000,
            85.6
          ]
        },
        {
          "row": [
            6802842,
            42,
            1953,
            3001,
            3560,
            99.8214
          ]
        },
        {
          "row": [
            6802841,
            42,
            1953,
            2001,
            3000,
            96.7
          ]
        },
        {
          "row": [
            6802840,
            42,
            1953,
            1001,
            2000,
            76.3
          ]
        },
        {
          "row": [
            6802839,
            42,
            1953,
            1,
            1000,
            97.1
          ]
        },
        {
          "row": [
            6802838,
            42,
            664,
            3001,
            3571,
            100
          ]
        },
        {
          "row": [
            6802837,
            42,
            664,
            2001,
            3000,
            98.9
          ]
        },
        {
          "row": [
            6802836,
            42,
            664,
            1,
            1000,
            22
          ]
        },
        {
          "row": [
            6802835,
            42,
            19,
            3001,
            3581,
            100
          ]
        },
        {
          "row": [
            6802834,
            42,
            19,
            2001,
            3000,
            100
          ]
        },
        {
          "row": [
            6802833,
            42,
            19,
            1001,
            2000,
            100
          ]
        },
        {
          "row": [
            6802832,
            42,
            19,
            1,
            1000,
            100
          ]
        }
      ],
      "node2": [
        {
          "row": [
            45,
            99,
            0,
            150,
            "sum"
          ]
        },
        {
          "row": [
            44,
            98,
            0,
            150,
            "sum"
          ]
        },
        {
          "row": [
            42,
            96,
            1000,
            0,
            "ratio"
          ]
        },
        {
          "row": [
            41,
            97,
            0,
            150,
            "ratio"
          ]
        },
        {
          "row": [
            43,
            96,
            0,
            150,
            "ratio"
          ]
        }
      ]
    }
  },
  this.get_table_list = function(){
    return {
      "node1": [
        {
          "tableName": "alt_allele"
        },
        {
          "tableName": "analysis"
        },
        {
          "tableName": "analysis_description"
        },
        {
          "tableName": "assembly"
        },
        {
          "tableName": "assembly_exception"
        },
        {
          "tableName": "attrib_type"
        },
        {
          "tableName": "coord_system"
        },
        {
          "tableName": "density_feature"
        },
        {
          "tableName": "density_type"
        },
        {
          "tableName": "ditag"
        },
        {
          "tableName": "ditag_feature"
        },
        {
          "tableName": "dna"
        },
        {
          "tableName": "dna_align_feature"
        },
        {
          "tableName": "dnac"
        },
        {
          "tableName": "exon"
        },
        {
          "tableName": "exon_stable_id"
        },
        {
          "tableName": "exon_transcript"
        },
        {
          "tableName": "external_db"
        },
        {
          "tableName": "external_synonym"
        },
        {
          "tableName": "gene"
        },
        {
          "tableName": "gene_archive"
        },
        {
          "tableName": "gene_attrib"
        },
        {
          "tableName": "gene_stable_id"
        },
        {
          "tableName": "go_xref"
        },
        {
          "tableName": "identity_xref"
        },
        {
          "tableName": "interpro"
        },
        {
          "tableName": "karyotype"
        },
        {
          "tableName": "map"
        },
        {
          "tableName": "mapping_session"
        },
        {
          "tableName": "marker"
        },
        {
          "tableName": "marker_feature"
        },
        {
          "tableName": "marker_map_location"
        },
        {
          "tableName": "marker_synonym"
        },
        {
          "tableName": "meta"
        },
        {
          "tableName": "meta_coord"
        },
        {
          "tableName": "misc_attrib"
        },
        {
          "tableName": "misc_feature"
        },
        {
          "tableName": "misc_feature_misc_set"
        },
        {
          "tableName": "misc_set"
        },
        {
          "tableName": "object_xref"
        },
        {
          "tableName": "oligo_array"
        },
        {
          "tableName": "oligo_feature"
        },
        {
          "tableName": "oligo_probe"
        },
        {
          "tableName": "peptide_archive"
        },
        {
          "tableName": "prediction_exon"
        },
        {
          "tableName": "prediction_transcript"
        },
        {
          "tableName": "protein_align_feature"
        },
        {
          "tableName": "protein_feature"
        },
        {
          "tableName": "qtl"
        },
        {
          "tableName": "qtl_feature"
        },
        {
          "tableName": "qtl_synonym"
        },
        {
          "tableName": "regulatory_factor"
        },
        {
          "tableName": "regulatory_factor_coding"
        },
        {
          "tableName": "regulatory_feature"
        },
        {
          "tableName": "regulatory_feature_object"
        },
        {
          "tableName": "regulatory_search_region"
        },
        {
          "tableName": "repeat_consensus"
        },
        {
          "tableName": "repeat_feature"
        },
        {
          "tableName": "seq_region"
        },
        {
          "tableName": "seq_region_attrib"
        },
        {
          "tableName": "simple_feature"
        },
        {
          "tableName": "stable_id_event"
        },
        {
          "tableName": "supporting_feature"
        },
        {
          "tableName": "transcript"
        },
        {
          "tableName": "transcript_attrib"
        },
        {
          "tableName": "transcript_stable_id"
        },
        {
          "tableName": "transcript_supporting_feature"
        },
        {
          "tableName": "translation"
        },
        {
          "tableName": "translation_attrib"
        },
        {
          "tableName": "translation_stable_id"
        },
        {
          "tableName": "unconventional_transcript_association"
        },
        {
          "tableName": "unmapped_object"
        },
        {
          "tableName": "unmapped_reason"
        },
        {
          "tableName": "xref"
        }
      ],
      "node2": [
        {
          "tableName": "alt_allele"
        },
        {
          "tableName": "analysis"
        },
        {
          "tableName": "analysis_description"
        },
        {
          "tableName": "assembly"
        },
        {
          "tableName": "assembly_exception"
        },
        {
          "tableName": "attrib_type"
        },
        {
          "tableName": "coord_system"
        },
        {
          "tableName": "density_feature"
        },
        {
          "tableName": "density_type"
        },
        {
          "tableName": "ditag"
        },
        {
          "tableName": "ditag_feature"
        },
        {
          "tableName": "dna"
        },
        {
          "tableName": "dna_align_feature"
        },
        {
          "tableName": "dnac"
        },
        {
          "tableName": "exon"
        },
        {
          "tableName": "exon_stable_id"
        },
        {
          "tableName": "exon_transcript"
        },
        {
          "tableName": "external_db"
        },
        {
          "tableName": "external_synonym"
        },
        {
          "tableName": "gene"
        },
        {
          "tableName": "gene_archive"
        },
        {
          "tableName": "gene_attrib"
        },
        {
          "tableName": "gene_stable_id"
        },
        {
          "tableName": "go_xref"
        },
        {
          "tableName": "identity_xref"
        },
        {
          "tableName": "interpro"
        },
        {
          "tableName": "karyotype"
        },
        {
          "tableName": "map"
        },
        {
          "tableName": "mapping_session"
        },
        {
          "tableName": "marker"
        },
        {
          "tableName": "marker_feature"
        },
        {
          "tableName": "marker_map_location"
        },
        {
          "tableName": "marker_synonym"
        },
        {
          "tableName": "meta"
        },
        {
          "tableName": "meta_coord"
        },
        {
          "tableName": "misc_attrib"
        },
        {
          "tableName": "misc_feature"
        },
        {
          "tableName": "misc_feature_misc_set"
        },
        {
          "tableName": "misc_set"
        },
        {
          "tableName": "object_xref"
        },
        {
          "tableName": "oligo_array"
        },
        {
          "tableName": "oligo_feature"
        },
        {
          "tableName": "oligo_probe"
        },
        {
          "tableName": "peptide_archive"
        },
        {
          "tableName": "prediction_exon"
        },
        {
          "tableName": "prediction_transcript"
        },
        {
          "tableName": "protein_align_feature"
        },
        {
          "tableName": "protein_feature"
        },
        {
          "tableName": "qtl"
        },
        {
          "tableName": "qtl_feature"
        },
        {
          "tableName": "qtl_synonym"
        },
        {
          "tableName": "regulatory_factor"
        },
        {
          "tableName": "regulatory_factor_coding"
        },
        {
          "tableName": "regulatory_feature"
        },
        {
          "tableName": "regulatory_feature_object"
        },
        {
          "tableName": "regulatory_search_region"
        },
        {
          "tableName": "repeat_consensus"
        },
        {
          "tableName": "repeat_feature"
        },
        {
          "tableName": "seq_region"
        },
        {
          "tableName": "seq_region_attrib"
        },
        {
          "tableName": "simple_feature"
        },
        {
          "tableName": "stable_id_event"
        },
        {
          "tableName": "supporting_feature"
        },
        {
          "tableName": "transcript"
        },
        {
          "tableName": "transcript_attrib"
        },
        {
          "tableName": "transcript_stable_id"
        },
        {
          "tableName": "transcript_supporting_feature"
        },
        {
          "tableName": "translation"
        },
        {
          "tableName": "translation_attrib"
        },
        {
          "tableName": "translation_stable_id"
        },
        {
          "tableName": "unconventional_transcript_association"
        },
        {
          "tableName": "unmapped_object"
        },
        {
          "tableName": "unmapped_reason"
        },
        {
          "tableName": "xref"
        }
      ]
    }
  },
  this.get_database_list = function(){
    return {
      "node1": [
        {
          "databaseName": "aedes_aegypti_core_48_1b"
        },
        {
          "databaseName": "aedes_aegypti_core_49_1b"
        },
        {
          "databaseName": "aedes_aegypti_core_50_1c"
        },
        {
          "databaseName": "aedes_aegypti_core_51_1c"
        },
        {
          "databaseName": "aedes_aegypti_core_52_1d"
        },
      ],
      "node2": [
        {
          "databaseName": "aedes_aegypti_core_48_1b"
        },
        {
          "databaseName": "aedes_aegypti_core_49_1b"
        },
        {
          "databaseName": "aedes_aegypti_core_50_1c"
        },
        {
          "databaseName": "aedes_aegypti_core_51_1c"
        },
        {
          "databaseName": "aedes_aegypti_core_52_1d"
        },
      ]
    }
  },
  this.get_nodes_list = function(){
    return [
      {
        "name": "density_feature",
        "code": "MySQL",
        "type": "mysql",
        "last_run_status": "Working",
        "dirty": true,
        "id": 1,
        "x": 20,
        "y": 20,
        "icon": "/images/icon_mysql.png",
        "params": {
          "node_name": "MySQL",
          "type": "mysql",
          "host": "ensembldb.ensembl.org",
          "port": 3306,
          "user": "anonymous",
          "password": "",
          "data_source_name": "testing_database",
          "database_token": "mysql_1461658310178_0000",
          "databaseName": "aedes_aegypti_core_48_1b",
          "tableName": "density_feature"
        },
        "outputConnectors": [
          {
            "name": ""
          }
        ],
        "width": 125,
        "error_message": "",
        "has_error": false,
        "last_run_message": "Please Wait! We are trying to fetch table structure"
      },
      {
        "name": "density_type",
        "code": "MySQL",
        "type": "mysql",
        "last_run_status": "Working",
        "dirty": true,
        "id": 2,
        "x": 180,
        "y": 20,
        "icon": "/images/icon_mysql.png",
        "params": {
          "node_name": "MySQL",
          "type": "mysql",
          "host": "ensembldb.ensembl.org",
          "port": 3306,
          "user": "anonymous",
          "password": "",
          "data_source_name": "testing_database",
          "database_token": "mysql_1461658310178_0000",
          "databaseName": "aedes_aegypti_core_49_1b",
          "tableName": "density_type"
        },
        "outputConnectors": [
          {
            "name": ""
          }
        ],
        "width": 125,
        "error_message": "",
        "has_error": false,
        "last_run_message": "Please Wait! We are trying to fetch table structure"
      }
    ]
  }
});
