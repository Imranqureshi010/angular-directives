vis_engine.controller('UsersCtrl', function ($scope,$http,Upload) {
  // START CODE FOR SAVE USER //
  $scope.users = [];
  $scope.new_user = {};
  $scope.current_user = {};
  $scope.get_users_list = function(){
    $http.get('/users/api_users_list/?active=true').success(function(data){
      if(data['status']==200){
        angular.copy(angular.fromJson(data['data']), $scope.users);
      }else{
        $scope.error = "There is some error in get Users list"+data['msg'];
      }
    });
  }
  $scope.dontHavePermission  = function($event){  
    $event.preventDefault();
    alert("You don't have permission to add a new user !");
  }
  $scope.accesslevels = [];
  $scope.get_accesslevels_list = function(){
    $http.get('/access_policies/api_accesslevels_list/').success(function(data){
      if(data['status']==200){
        angular.copy(angular.fromJson(data['data']), $scope.accesslevels);
      }else{
        $scope.error = "There is some error in get accesslevels list"+data['msg'];
      }
    });
  }
  $scope.emailNotAvailable = false;
  $scope.check_email = function(id,email){
    $http.get('/users/api_users_list/').success(function(data){
      if(data['status']==200){
        $scope.load_error=false;
        var found = $.grep(data.data, function(e){ return e.email == email && e.id != id });
        if(found.length){
          $scope.emailNotAvailable = true;
        }else{
          $scope.emailNotAvailable = false;
        }
      }else{
        $scope.load_error=true;
        $scope.err_message=data['msg'];
      }
    });
  }
  $scope.usernameNotAvailable = false;
  $scope.check_username = function(id,username){
    $http.get('/users/api_users_list/').success(function(data){
      if(data['status']==200){
        $scope.load_error=false;
        var found = $.grep(data.data, function(e){ return e.username == username && e.id != id });
        if(found.length){
          $scope.usernameNotAvailable = true;
        }else{
          $scope.usernameNotAvailable = false;
        }
      }else{
        $scope.load_error=true;
        $scope.err_message=data['msg'];
      }
    });
  }
  $scope.show_add_modal = function($event){
    $event.preventDefault();
    $scope.new_user = {};
    $scope.save_user_label = "save";
    $('#modal-new-user-add').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.show_edit_modal = function($event,user){
    $event.preventDefault();
    $scope.save_user_label = "save";
    console.log('users is :',user);
    delete user.password;
    angular.copy(angular.fromJson(user),$scope.new_user);
    $('#modal-new-user-add').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.delete_confirm = function($event,user){
    $event.preventDefault();
    $scope.current_user = user;
    $('#modal-user-delete').modal({show:true, backdrop:'static', keyboard:true});
  }
  $scope.save_user = function(){
    if($scope.new_user){
      console.log('$scope.new_user is :=========',$scope.new_user);
      $scope.save_user_label = "Submitting...";
      $http({method:"POST", url:"/users/api_save_user/", data:$scope.new_user}).success(function(data){
      console.log('user data is :=========',data);
        if(data.status == 200){
          if($scope.file){
            console.log('$scope.file is:',$scope.file);
            var post_data={file: $scope.file};
            var up=Upload.upload({
              url: '/users/api_save_photo/'+data['data'].id,
              data: post_data
            })
            up.then(function (resp) {
              console.log("File Uploaded Successfully : ",resp)
                if($scope.new_user.id){
                  for (var i = 0, ii = $scope.users.length; i < ii; i++) {
                    if ($scope.new_user.id == $scope.users[i].id) {
                      $scope.users[i] = data['data'];
                      break;
                    }
                  }
                }else{
                  $scope.new_users={};
                  $scope.users.push(data['data']);
                }
                $('#modal-new-user-add').modal('hide');
            }, function (resp){
              console.log('File Has An Error: ' + resp.status);
              $scope.form_submitting = false;
            }, function (evt) {
              console.log('File Progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
          }else{
            if($scope.accesslevels){
              for (var i = $scope.accesslevels.length - 1; i >= 0; i--) {
                if($scope.accesslevels[i].levelid == data['data'].system_access_policy){
                  data['data'].levelname = $scope.accesslevels[i].levelname;
                  data['data'].system_access_policy = $scope.accesslevels[i].levelid;
                }
              };
            }
            if($scope.new_user.id){
              for (var i = 0, ii = $scope.users.length; i < ii; i++) {
                if ($scope.new_user.id == $scope.users[i].id) {
                  $scope.users[i] = data['data'];
                  break;
                }
              }
            }else{
              $scope.users.push(data['data']);
            }
            $('#modal-new-user-add').modal('hide');
          }
        }else{
          $scope.error = "There was same error in save user"+data['msg']; 
        }
      });
    }
  }
  $scope.delete_user = function(){
    if($scope.current_user){
      $http({method:'POST', url:"/users/api_delete/", data:$scope.current_user}).
        success(function (data) {
          if(data['status']=="200"){
            for (var i = 0, ii = $scope.users.length; i < ii; i++) {
              if ($scope.current_user === $scope.users[i]) {
                $scope.users.splice(i, 1);
                $("#modal-user-delete").modal('hide');
                break;
              }
            }
          }else{
            $scope.error="There was some error in loading the data from server. <br>Server said: " + data['msg'];
          }
        }).
        error(function (data, status, headers, config) {
          $scope.error="There was some error in loading the data from server. <br>Server said: " + data;
        }
      );
    }
  }
  // END CODE FOR SAVE USER // 
});