app.controller('MainCtrl', function($scope,ChartService) {
	// $scope.line_chart = ChartService.lineChart();
	// $scope.scatter_chart = ChartService.scatterChart();
	$scope.csv_data = {
  "km_data": [
    {
      "sno": "\"1\"",
      "cluster": "1",
      "size": "2096734",
      "spread": "28181005.7842744",
      "imp_vars": [
        {
          "y": "0.60",
          "x": "Total ViewedDuration of SMARTPHONE  of Last 3 Months"
        },
        {
          "y": "0.30",
          "x": "Count ViewedDuration of SMARTPHONE  of Last 3 Months"
        },
        {
          "y": "92.57",
          "x": "Total ViewedDuration   of Last 3 Months"
        },
        {
          "y": "6.05",
          "x": "Count ViewedDuration of SetTopBox  of Last 3 Months"
        },
        {
          "y": "0.49",
          "x": "Total ViewedDuration of SetTopBox  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "1",
          "accesscard_id": "5c2ab4a6dab42b3969c8bf7aeeae8e3f6156c49e32a3f51c8e7db51b89dda271144017"
        },
        {
          "s_no": "2",
          "accesscard_id": "64055ea983f443ff7f498ce4041c2d94105f34247bd64ed50d41d549e8e7bc20866944"
        },
        {
          "s_no": "3",
          "accesscard_id": "8afcb3ae4c3deeb774d2d1818abb10dc30cd6c72b7b2b77cb9e958bd249a14c2737284"
        },
        {
          "s_no": "4",
          "accesscard_id": "9cf94734292bc33f13354ff764e71e9b8c1bdeb6f07908d01e337b18f13168b0764119"
        },
        {
          "s_no": "5",
          "accesscard_id": "4172add4e845825d5b58032fc0421f7b6ed8becc99b4a8d06b700bda2e200a29777472"
        },
        {
          "s_no": "6",
          "accesscard_id": "4915897ea3731ff0467421d8c135d0f76f917cabab5ee774c82fc6a636abad08455169"
        },
        {
          "s_no": "7",
          "accesscard_id": "71f6e83a81f68f5bbff275cd04e198ffd02d3fd654e1823cc2b00998b90c628c630192"
        },
        {
          "s_no": "8",
          "accesscard_id": "ba50e46c8385edcdca1afed7c0237277d038df07a7c5104194a3017563135109822072"
        },
        {
          "s_no": "9",
          "accesscard_id": "b1405bf3fb5aef3fa8bf75c19ce68fe7d317cca9b8bce9cb7ca2b385e75ed949902438"
        },
        {
          "s_no": "10",
          "accesscard_id": "e5af069f9b4025b5bc9e7ad3659aa8e9231dd5bdf0e7facc1d4c40449e9799d8943450"
        }
      ]
    },
    {
      "sno": "\"2\"",
      "cluster": "2",
      "size": "18",
      "spread": "1984981.81211479",
      "imp_vars": [
        {
          "y": "100.00",
          "x": "Total ViewedDuration of Adult  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "11",
          "accesscard_id": "2b7cf9b825c0ffd3075f700df8a6fa180f9b2ff4da43700ce479efb3361a9608565862"
        },
        {
          "s_no": "12",
          "accesscard_id": "581501bf23fca44b5dfbfd7b57714d6cabb5c5c20a882a213f0c2b113cc53c79454544"
        },
        {
          "s_no": "13",
          "accesscard_id": "7305462507b47af1bb0bcfcfc264c758daa8262ec3fc110a76044db9c936c24e672376"
        },
        {
          "s_no": "14",
          "accesscard_id": "af476734d96e320386f0dddb908d8b58fff35f6f80c7ccabc27adea38eef30ac834424"
        },
        {
          "s_no": "15",
          "accesscard_id": "d1fe9b39a630887f71965df14c0e5011cfdbe50b75fc84870cfa62a00dbae77b788858"
        },
        {
          "s_no": "16",
          "accesscard_id": "ad9a00c76d030ee8a9e148a50f2c776ccd4fea34d76ba61179edc1ead8a41ea3390352"
        }
      ]
    },
    {
      "sno": "\"3\"",
      "cluster": "3",
      "size": "5",
      "spread": "611129.503375185",
      "imp_vars": [
        {
          "y": "0.00",
          "x": "Count ViewedDuration of Purchase  of Last 3 Months"
        },
        {
          "y": "2.16",
          "x": "Total ViewedDuration of DOTCOM  of Last 3 Months"
        },
        {
          "y": "2.02",
          "x": "Total ViewedDuration of Thursday  of Last 3 Months"
        },
        {
          "y": "25.18",
          "x": "Total Duration of Family Movies in last 3 months"
        },
        {
          "y": "0.01",
          "x": "Count ViewedDuration of EPISODE Late Evening  of Last 3 Months"
        },
        {
          "y": "68.31",
          "x": "Total ViewedDuration of Movies  of Last 3 Months"
        },
        {
          "y": "2.31",
          "x": "Count ViewedDuration of Movies  of Last 3 Months"
        },
        {
          "y": "0.02",
          "x": "Total ViewedDuration of Movies G  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "17",
          "accesscard_id": "1f49356e5ea6a5053d4b814f273b99bf4e9dcda8308c641f011d7e617daf3b2d721235"
        },
        {
          "s_no": "18",
          "accesscard_id": "d01e03a3a721ae655364844061ea40823beeffa89adf5dd415affbf86f312cc5701991"
        },
        {
          "s_no": "19",
          "accesscard_id": "5a1c6f370ba0ab667db406e6894d81b24ec7d73d65a856ce778753674118fa3a496025"
        },
        {
          "s_no": "20",
          "accesscard_id": "5fd4a13564605991392b50e10bc211515ea152ba52b460ceb1cc7f4b1ed52e1c932733"
        },
        {
          "s_no": "21",
          "accesscard_id": "8d1d5ea196c6337372d02d7523a136bcfa0600558d140ff05b0dd51c2cac2d3278902"
        },
        {
          "s_no": "22",
          "accesscard_id": "91b8ac3432f3eb62fb327b7b7fa7fe339763cabbff391bdddadebec44d365d3b725088"
        },
        {
          "s_no": "23",
          "accesscard_id": "41fc5e4bf25da7a064f210488ea68caa3bb6edb7c49e09154fab82c9a88080fe493289"
        }
      ]
    },
    {
      "sno": "\"4\"",
      "cluster": "4",
      "size": "45",
      "spread": "1628975.07619973",
      "imp_vars": [
        {
          "y": "5.29",
          "x": "Total ViewedDuration of ANDROID PHONE  of Last 3 Months"
        },
        {
          "y": "10.28",
          "x": "Count ViewedDuration of SMARTPHONE  of Last 3 Months"
        },
        {
          "y": "63.18",
          "x": "Total ViewedDuration of SMARTPHONE  of Last 3 Months"
        },
        {
          "y": "2.19",
          "x": "Count ViewedDuration of IPHONE  of Last 3 Months"
        },
        {
          "y": "18.04",
          "x": "Total ViewedDuration of IPHONE  of Last 3 Months"
        },
        {
          "y": "1.01",
          "x": "Total ViewedDuration   of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "24",
          "accesscard_id": "0c9dc0a78f96bd1594ce822537e2ab3b51968e72332624d0594cf82486c32b47376550"
        },
        {
          "s_no": "25",
          "accesscard_id": "32a1bf4b32890a24536e5d18fd54348087a225a9513c59c57938ec993d4c0853180942"
        },
        {
          "s_no": "26",
          "accesscard_id": "cf669e64f3a7b555acd566856ab50c976a754c7145eeba7a4fb82140473b109081722"
        },
        {
          "s_no": "27",
          "accesscard_id": "d11b65c52d7f1ed31614f57a9a6fd7a063d044935aaf7d62910f6fc3e186f161116055"
        },
        {
          "s_no": "28",
          "accesscard_id": "d4951279105aac00abf8285e94c95aaf74b1b53697ea184a589819a39f0c22bb867453"
        },
        {
          "s_no": "29",
          "accesscard_id": "0b855ffbe7945646270478dd08b2a7660ce60a310755e5039af65bc9556cea63263924"
        },
        {
          "s_no": "30",
          "accesscard_id": "5a4f16b899c0fada7a6c5c33efee220eafc14ed66cffce0acd374fb90d9b4958561648"
        },
        {
          "s_no": "31",
          "accesscard_id": "87d6afa25940f52dc009dcad23c60331862a0808e1b026908835446df13b1f65632561"
        },
        {
          "s_no": "32",
          "accesscard_id": "798600f8e441270f4a98ec159a3235bb8bfef5b48e941d84805e630dad92f242377550"
        },
        {
          "s_no": "33",
          "accesscard_id": "4708a44b334ea791d5e49be0da547200835745b09f6d0fe93c3cb818c15c47a4896918"
        }
      ]
    },
    {
      "sno": "\"5\"",
      "cluster": "5",
      "size": "582",
      "spread": "4075974.96235155",
      "imp_vars": [
        {
          "y": "26.60",
          "x": "Total ViewedDuration of DOTCOM  of Last 3 Months"
        },
        {
          "y": "73.40",
          "x": "Count ViewedDuration of DOTCOM  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "34",
          "accesscard_id": "28c068132c94de28d0fd4259214435ec8ed73cb587fa58a85a8eee84c6a3e165211631"
        },
        {
          "s_no": "35",
          "accesscard_id": "4442341c8c5784350d4d7452d4179945c3e4ff55edf8574c0404cbb284e0539d668032"
        },
        {
          "s_no": "36",
          "accesscard_id": "7458fa82a22ad430fdc886eb4dfa17e6e848080074fb4277f2bf8b9f8ba340b451853"
        },
        {
          "s_no": "37",
          "accesscard_id": "61edeb33a5254c9438439f3dfb3714f89e5fef29eac6c910c4cc235453f3c434624469"
        }
      ]
    },
    {
      "sno": "\"6\"",
      "cluster": "6",
      "size": "222",
      "spread": "1659051.4899693",
      "imp_vars": [
        {
          "y": "100.00",
          "x": "Total ViewedDuration of ANDROID TABLET  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "38",
          "accesscard_id": "2cd30177afedbc3cfd1ff5f8e7a84f7887cbc9ff00dc0abe3616c168bdca896c5992"
        },
        {
          "s_no": "39",
          "accesscard_id": "8306790843d17e9c720ec1b88af7ffebe8f75cc9589b021fd266f3536362f724667849"
        }
      ]
    },
    {
      "sno": "\"7\"",
      "cluster": "7",
      "size": "6",
      "spread": "517251.525033387",
      "imp_vars": [
        {
          "y": "0.71",
          "x": "Total ViewedDuration of SetTopBox  of Last 3 Months"
        },
        {
          "y": "0.15",
          "x": "No of times Weekend Views"
        },
        {
          "y": "19.12",
          "x": "Count ViewedDuration of SetTopBox  of Last 3 Months"
        },
        {
          "y": "80.02",
          "x": "Total ViewedDuration   of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "40",
          "accesscard_id": "55e759d27ee9bfa1ffce73555a6a98c5c7e26a7cbf94f7cd2eb004434fa912d9715533"
        },
        {
          "s_no": "41",
          "accesscard_id": "979e80013e646509ffdfc2c04005d639319e40a2b082f678d29ba64bcabf1c34246695"
        },
        {
          "s_no": "42",
          "accesscard_id": "d28de5f3a6c80e292344a942face4774ea5b028c3545550c6ecbcdebfd43ae98420574"
        },
        {
          "s_no": "43",
          "accesscard_id": "34375930"
        },
        {
          "s_no": "44",
          "accesscard_id": "413ca3121253193a5d98c368aaf25d96405b354c60dae35d7fc0760f31bea4b9847351"
        },
        {
          "s_no": "45",
          "accesscard_id": "4c8a067ea36759193584159e03e6a6e47f2b73df0cbf4a076b84913b01723fac170527"
        },
        {
          "s_no": "46",
          "accesscard_id": "56239186"
        },
        {
          "s_no": "47",
          "accesscard_id": "488c8b961b7128160731db8f665b2f392d51a36f81919379889d4c2940d70587597705"
        },
        {
          "s_no": "48",
          "accesscard_id": "b3ce33d01b28e2541df2f272e87153e81505199d402ac1828ba57ab13da39000671646"
        },
        {
          "s_no": "49",
          "accesscard_id": "f72dbfa720def39ee2a34d37e7c54db4ea028f3b365f226d97b3d2076732c37e678111"
        }
      ]
    },
    {
      "sno": "\"8\"",
      "cluster": "8",
      "size": "545",
      "spread": "1942509.29153819",
      "imp_vars": [
        {
          "y": "5.40",
          "x": "Total ViewedDuration of SetTopBox  of Last 3 Months"
        },
        {
          "y": "4.16",
          "x": "Count ViewedDuration of TABLET  of Last 3 Months"
        },
        {
          "y": "7.28",
          "x": "Total ViewedDuration   of Last 3 Months"
        },
        {
          "y": "82.87",
          "x": "Count ViewedDuration of SetTopBox  of Last 3 Months"
        },
        {
          "y": "0.30",
          "x": "No of times Children Programs in last 3 months"
        }
      ],
      "sample_data": [
        {
          "s_no": "50",
          "accesscard_id": "11159d981884bc337ec3c5af1531f79c872c01a97995afb326af2daee1bc9c60917973"
        },
        {
          "s_no": "51",
          "accesscard_id": "64c84582fba63ecb1fb358892a47908425afdb649909abac8f5aeb4a866247f4315010"
        },
        {
          "s_no": "52",
          "accesscard_id": "02079c0504c0f8b7efa8d6d3d4ce095b5ef4662b5c00ecee41ece1b3ff7e5cbf844734"
        },
        {
          "s_no": "53",
          "accesscard_id": "a08160a8712520a8dbd824f079f6ac3b6dc3f6618fe9100a654b606d54f953ae212860"
        },
        {
          "s_no": "54",
          "accesscard_id": "b72d1582c542513798cf6dc8638e101851ad9cb5c1d9bf7ef5182b5cec07d7c8711413"
        },
        {
          "s_no": "55",
          "accesscard_id": "fd20f162fa70362e25295dbbcd32788fb85b4d064223576f7e721ccf8bcd669d183982"
        },
        {
          "s_no": "56",
          "accesscard_id": "8fbba5e9dd1989923b7b8146690357f5810787c0d17036a34d62b3cd5485647f887438"
        },
        {
          "s_no": "57",
          "accesscard_id": "a085afea8a09a1e880b1998ca3d36562b5b56f9dd0108ee4b6ed43032bc807df198475"
        },
        {
          "s_no": "58",
          "accesscard_id": "ec12487d0498bf41f5b0a55b20a764cf803b4e55f7cfa0625f3dffe57b65af40705422"
        },
        {
          "s_no": "59",
          "accesscard_id": "f684feeb745fbe7c650a576572d4a7380bb9ccfd65ee3653023adb13e346922338143"
        }
      ]
    },
    {
      "sno": "\"9\"",
      "cluster": "9",
      "size": "1378",
      "spread": "4138513.6789533",
      "imp_vars": [
        {
          "y": "0.44",
          "x": "Total ViewedDuration of TV  of Last 3 Months"
        },
        {
          "y": "0.12",
          "x": "Count ViewedDuration of Adult  of Last 3 Months"
        },
        {
          "y": "0.02",
          "x": "Count ViewedDuration of TABLET  of Last 3 Months"
        },
        {
          "y": "0.40",
          "x": "Total ViewedDuration of Adult  of Last 3 Months"
        },
        {
          "y": "0.03",
          "x": "Total ViewedDuration of Thursday  of Last 3 Months"
        },
        {
          "y": "0.02",
          "x": "Count ViewedDuration of Thursday  of Last 3 Months"
        },
        {
          "y": "98.44",
          "x": "No of times weekdays views"
        },
        {
          "y": "0.27",
          "x": "Count ViewedDuration of Purchase  of Last 3 Months"
        },
        {
          "y": "0.05",
          "x": "Total ViewedDuration of PlaybackEvent  of Last 3 Months"
        },
        {
          "y": "0.19",
          "x": "Total ViewedDuration of Sports  of Last 3 Months"
        },
        {
          "y": "0.03",
          "x": "Count ViewedDuration of EPISODE Late Night  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "60",
          "accesscard_id": "0951ba7ebea34510e8b838117d1b1717219ae75ec2173151b7ed9ea81e3e74df74066"
        },
        {
          "s_no": "61",
          "accesscard_id": "77adc4e64016aa2238f467fb4191164899cab44f836671abc99144fc26eb5485745303"
        },
        {
          "s_no": "62",
          "accesscard_id": "14f5cf9f77e944c0500a01d9261ec1892c08a39060959784b48b24db84f36d4b20286"
        },
        {
          "s_no": "63",
          "accesscard_id": "1d6f970a1718d5e3fa57427f034ad59448348d18d8b8d51746d8cde7a65a8978520515"
        },
        {
          "s_no": "64",
          "accesscard_id": "d95adcca97a8e46365372b2e7c8ef612b07369f9d1f517808a44ee2b759666a5982317"
        },
        {
          "s_no": "65",
          "accesscard_id": "92d3b7ac098a88ac752156b1cd9d9e0490083b35dfa0fef141126271f9dc7e23107627"
        },
        {
          "s_no": "66",
          "accesscard_id": "6255987e284a04c8fc55b8a603458892092a8b5750949515030804875ec9144229236"
        },
        {
          "s_no": "67",
          "accesscard_id": "63edb13ab7e9e238c0f6f844d46ae24a3507d5cf41ec9ef5d909b3c68b1b26a2567826"
        },
        {
          "s_no": "68",
          "accesscard_id": "f6cd3a660d6af16e1ca66a14c94b66efbd9c7d72e8921edaf83d2b9babd43b63796070"
        },
        {
          "s_no": "69",
          "accesscard_id": "fbfa1d73c582738f79a2b38cb13c271715d29ce0408306a1996866791aa99cba477732"
        }
      ]
    },
    {
      "sno": "\"10\"",
      "cluster": "10",
      "size": "67523",
      "spread": "48213996.5274512",
      "imp_vars": [
        {
          "y": "91.04",
          "x": "Total ViewedDuration of ANDROID PHONE  of Last 3 Months"
        },
        {
          "y": "2.18",
          "x": "Total ViewedDuration   of Last 3 Months"
        },
        {
          "y": "0.04",
          "x": "Count ViewedDuration of Daytime  of Last 3 Months"
        },
        {
          "y": "0.03",
          "x": "Total ViewedDuration of TV  of Last 3 Months"
        },
        {
          "y": "6.68",
          "x": "Count ViewedDuration of ANDROID PHONE  of Last 3 Months"
        },
        {
          "y": "0.04",
          "x": "Count ViewedDuration of Prime Fringe  of Last 3 Months"
        }
      ],
      "sample_data": [
        {
          "s_no": "70",
          "accesscard_id": "53dc0054dc9c2727c6fdced878528a4358a086362fc12936ab1efcee619a06b4636641"
        },
        {
          "s_no": "71",
          "accesscard_id": "6dc4d40f35221f2a4fc75b2bf175148934e2ce99a554bd750e5cd91065de38af851635"
        },
        {
          "s_no": "72",
          "accesscard_id": "0686ec0c162ab3d4ea07371fc32eb9ae78d697af812d13333d83f7bdce35d585688604"
        },
        {
          "s_no": "73",
          "accesscard_id": "1ed5983d9a4d4a268e99cde5db27fb349d0d0d9be816504599bd188e143df0df254231"
        },
        {
          "s_no": "74",
          "accesscard_id": "21fee43234bfede5b99153a9396d1270ad55d26312dcf4d8061e57f19b374eef350726"
        },
        {
          "s_no": "75",
          "accesscard_id": "4f641f5208b13e7b8fc309da6a7ad87a2b59c42cac14da4a4cdccd64f050da8d620361"
        },
        {
          "s_no": "76",
          "accesscard_id": "77587e44d59ad37b020ae1e8e6df67bacae4761449bc6cf4cadc3101db9a9958859990"
        },
        {
          "s_no": "77",
          "accesscard_id": "77f2a0b43e4a709c780cc893cfcefb815f16fb4c2f7c66aa6425e09e5b1a0b01932936"
        },
        {
          "s_no": "78",
          "accesscard_id": "b67efadaba9709c37b58cca88e58902fba1b725facad472d4bc3fcdd43e3e181705424"
        },
        {
          "s_no": "79",
          "accesscard_id": "73c37642d08106e05ac1348319a8500ac85cdb668e0ec77e8b76a27fea07d975503869"
        }
      ]
    }
  ]
}

	angular.forEach($scope.csv_data.km_data, function(cluster, key){
        cluster.pie_chart = ChartService.pieChart(cluster.imp_vars);
    });
	var data = [
            {
                x: "One",
                y: 5
            },
            {
                x: "Two",
                y: 2
            },
            {
                x: "Three",
                y: 9
            },
            {
                x: "Four",
                y: 7
            },
            {
                x: "Five",
                y: 4
            },
            {
                x: "Six",
                y: 3
            },
            {
                x: "Seven",
                y: .5
            }
        ];
    $scope.pie_chart = ChartService.pieChart(data);

	$scope.test = function(){
		// $scope.api.refresh();
	}
});
