app.service('ChartService', function(){
    this.historicalBarChart= function(data){
        var chart_obj = {}
        chart_obj.options = {
            chart: {
                type: 'historicalBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 50
                },
                x: function(d){return d[0];},
                y: function(d){return d[1]/100000;},
                showValues: true,
                valueFormat: function(d){
                    return d3.format(',.1f')(d);
                },
                transitionDuration: 500,
                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d) {
                        return d3.time.format('%x')(new Date(d))
                    },
                    rotateLabels: 50,
                    showMaxMin: false
                },
                yAxis: {
                    axisLabel: 'Y Axis',
                    axisLabelDistance: 35,
                    tickFormat: function(d){
                        return d3.format(',.1f')(d);
                    }
                }
            }
        };

       
        chart_obj.data =[
          {
              "key" : "Quantity" ,
              "bar": true,
              "values" : [ [ 1136005200000 , 1271000.0] , [ 1138683600000 , 1271000.0] , [ 1141102800000 , 1271000.0] , [ 1143781200000 , 0] , [ 1146369600000 , 0] , [ 1149048000000 , 0] , [ 1151640000000 , 0] , [ 1154318400000 , 0] , [ 1156996800000 , 0] , [ 1159588800000 , 3899486.0] , [ 1162270800000 , 3899486.0] , [ 1164862800000 , 3899486.0] , [ 1167541200000 , 3564700.0] , [ 1170219600000 , 3564700.0] , [ 1172638800000 , 3564700.0] , [ 1175313600000 , 2648493.0] , [ 1177905600000 , 2648493.0] , [ 1180584000000 , 2648493.0] , [ 1183176000000 , 2522993.0] , [ 1185854400000 , 2522993.0] , [ 1188532800000 , 2522993.0] , [ 1191124800000 , 2906501.0] , [ 1193803200000 , 2906501.0] , [ 1196398800000 , 2906501.0] , [ 1199077200000 , 2206761.0] , [ 1201755600000 , 2206761.0] , [ 1204261200000 , 2206761.0] , [ 1206936000000 , 2287726.0] , [ 1209528000000 , 2287726.0] , [ 1212206400000 , 2287726.0] , [ 1214798400000 , 2732646.0] , [ 1217476800000 , 2732646.0] , [ 1220155200000 , 2732646.0] , [ 1222747200000 , 2599196.0] , [ 1225425600000 , 2599196.0] , [ 1228021200000 , 2599196.0] , [ 1230699600000 , 1924387.0] , [ 1233378000000 , 1924387.0] , [ 1235797200000 , 1924387.0] , [ 1238472000000 , 1756311.0] , [ 1241064000000 , 1756311.0] , [ 1243742400000 , 1756311.0] , [ 1246334400000 , 1743470.0] , [ 1249012800000 , 1743470.0] , [ 1251691200000 , 1743470.0] , [ 1254283200000 , 1519010.0] , [ 1256961600000 , 1519010.0] , [ 1259557200000 , 1519010.0] , [ 1262235600000 , 1591444.0] , [ 1264914000000 , 1591444.0] , [ 1267333200000 , 1591444.0] , [ 1270008000000 , 1543784.0] , [ 1272600000000 , 1543784.0] , [ 1275278400000 , 1543784.0] , [ 1277870400000 , 1309915.0] , [ 1280548800000 , 1309915.0] , [ 1283227200000 , 1309915.0] , [ 1285819200000 , 1331875.0] , [ 1288497600000 , 1331875.0] , [ 1291093200000 , 1331875.0] , [ 1293771600000 , 1331875.0] , [ 1296450000000 , 1154695.0] , [ 1298869200000 , 1154695.0] , [ 1301544000000 , 1194025.0] , [ 1304136000000 , 1194025.0] , [ 1306814400000 , 1194025.0] , [ 1309406400000 , 1194025.0] , [ 1312084800000 , 1194025.0] , [ 1314763200000 , 1244525.0] , [ 1317355200000 , 475000.0] , [ 1320033600000 , 475000.0] , [ 1322629200000 , 475000.0] , [ 1325307600000 , 690033.0] , [ 1327986000000 , 690033.0] , [ 1330491600000 , 690033.0] , [ 1333166400000 , 514733.0] , [ 1335758400000 , 514733.0]]
          }
        ];
        return chart_obj;
    },  

    this.lineChart = function(data){
        var chart_obj = {}
        chart_obj.options = {
            chart: {
                type: 'lineChart',
                height: 450,
                width: 800,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 105
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                tooltipContent: function(key, x, e, graph) {
                    return "<p style='padding:10px;color:black;text-align:left;font-weight:bolder'> X : "+x+"<br> Y : "+e+" </p>" ;
                },
                // useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Number of trees',
                },
                yAxis: {
                    axisLabel: 'Training/Validation MSE',
                    tickFormat: function(d){
                        return d3.format('.02f')(d);
                    },
                    axisLabelDistance: 10
                },
            },
        };

        var value_obj = [
          { x: 1.3856102003642983, y: 5490 },
          { x: 3.831309041835356, y: 1482 },
          { x: 6.556818181818181, y: 440 },
          { x: 9.138888888888891, y: 252 },
          { x: 12.396449704142011, y: 169 },
          { x: 15.837606837606833, y: 117 },
          { x: 18.73228346456693, y: 127 },
          { x: 22.126865671641788, y: 134 },
          { x: 25.49305555555555, y: 144 },
          { x: 28.654545454545456, y: 110 },
          { x: 31.014492753623188, y: 69 },
          { x: 33.48888888888888, y: 90 },
          { x: 36.314814814814824, y: 54 },
          { x: 40.43939393939395, y: 66 },
          { x: 44.72222222222222, y: 18 },
          { x: 48.29999999999999, y: 10 },
          { x: 52.66666666666667, y: 3 },
          { x: 56, y: 1 },
          { x: 61, y: 1 },
          { x: 75, y: 1 }
        ];

        angular.forEach(value_obj, function(value, key) {
          value_obj[key]={x:Math.round(value.x),y:value.y};
        });
        console.log("value_obj is : ",value_obj)
        chart_obj.data = [
            {
              "key": "Training MSEfdsfs",
              "values": value_obj
            },
           
        ]
        return chart_obj;
    },

    this.scatterChart = function(){
      var chart_obj = {};
      chart_obj.options = {
          chart: {
              type: 'scatterChart',
              height: 450,
              color: d3.scale.category10().range(),
              scatter: {
                  onlyCircles: false
              },
              showDistX: true,
              showDistY: true,
              tooltipContent: function(key) {
                  return '<h3>' + key + '</h3>';
              },
              transitionDuration: 350,
              xAxis: {
                  axisLabel: 'X Axis',
                  tickFormat: function(d){
                      return d3.format('.02f')(d);
                  }
              },
              yAxis: {
                  axisLabel: 'Y Axis',
                  tickFormat: function(d){
                      return d3.format('.02f')(d);
                  },
                  axisLabelDistance: 30
              }
          }
      };

      chart_obj.data = [
        {
          "key":"Something",
          "values": [
            { x: '000028d58eab541ef66809f5d26fcc7badda7eee7d926be0aa7ed96e4ebb546f827071',
             y: '0' },
            { x: '000081ed3fee2731b7185b0b654df072107e27a6c0cd0e0212d40f2b1a739ea4687062',
             y: '0' },
            { x: '00009d69765d529d4c67598b8fa109685e977f2c4fc332433acc5e49cc19b1f9703984',
             y: '0' },
            { x: '0000a0a8bf20ecfec308f7efd53de03dcd0b325896dc5e08c29bbdb1b2476bed460742',
             y: '0' },
            { x: '0000ab49a5520dca5e37fc772e7d186e85c77bbe0c7f26073e19db80235db842992674',
             y: '0' },
            { x: '0000ba3daab076db18efa199b2a5e96d109fe780511d4ffa1a8126095f19a846523369',
             y: '0' },
            { x: '0000c10e8b33820db774a1cbf930555e7e3822cb9dd00fa6b6a64128149a3178168748',
             y: '0' },
            { x: '0000e9ed4e05267eb3ef9959afd84b82e32631c3bd5b7254483f3ae49fda1d2417880',
             y: '0' },
            { x: '0000fb2f885366735698fbfe9ac5ce4eb822f9faa41648949e79c61e2f36b8d9851665',
             y: '0' },
            { x: '000127dafa74e828e0c4a3001a0872aac2edde032eed4dc2e27c49160a99131649388',
             y: '0' },
            { x: '00012b88fbd122b692f3513acba0a391e7374387d79ea5c6cfd265ad38b81665443525',
             y: '0' },
            { x: '00012c52b01849b549ef19f224322bb3b754e34bbcfac37ee865d425811bb66b679281',
             y: '0' },
            { x: '00013bf07bad94c4c656fa6e65631947f99b147dad2ba0477acacd63cbefb7b569866',
             y: '0' },
            { x: '000155c42343cecebb4128ca678f424feb622ea990c0c1fab08de76e20edd186361404',
             y: '0' },
            { x: '0001cbb7e60e8b92977e43445ae7a46737d9c2589ae5fb876bd0824816146d10225758',
             y: '0' },
            { x: '00020b01d1ffb6e7f0c3549a17f4a5eae9d0e6725319f3ae59198f74313a684f681885',
             y: '0' },
            { x: '00023b6e60800673d88068011dc6667bae7a485dc5434852fc7b34db965c60ad759443',
             y: '0' },
            { x: '000249cac8dfb5d3576e4d856028e69051f6f80ba4ca0157b112d6238f055c1d707191',
             y: '0' },
            { x: '00024a8d2fb88f33a58a9b021d377b1e1f128778d06d379209f42134dad24c9b921634',
             y: '0' },
            { x: '00026191485f6126d16467d90c3067aa61680d2a62b88a4dce611b68b0e4c89c398287',
             y: '0' },
            { x: '00026ea5ef21d3fbb4818f0af344511cdf6eba553685d56f43382abe308deee7924745',
             y: '0' },
            { x: '00027934734faf03fe9108a61318c8693f69b1cd70757f15c67f519cd0fb986d731368',
             y: '0' },
            { x: '000292dae0f27992ecc41b5c5130026021aa9dece449b6e043b58d8bbcfff559907025',
             y: '0' },
            { x: '0002adf65f52690f0a037d0f7d7bbdbaa4877f784c81ad9be9a7cf25ace91fea693754',
             y: '0' },
            { x: '0002d659dac46228ea088e27c40a97b84265114b7089a9ad7f71f10897f4d809947719',
             y: '0' },
            { x: '0002d8fd4dbb9cb5b0d3b6847ca0584d36f77d3a710a9f3ba4cb437963ce1e0e791745',
             y: '0' },
            { x: '0002eb23a23608a769b26a48ce9e84365a1709766e253da76453839035501dfe567776',
             y: '0' },
            { x: '0002eccab60487c88ba5cc7e92036e1cd96df90813af2e86657f05f43101997b364727',
             y: '0' },
            { x: '0002ee885c213c0d8ad212f3a2c350e2b9d05644d2215d406c9a4ebb31effb4b306958',
             y: '0' },
            { x: '0003004d9332180adaf4cf1569e7940e8a74cd54dd8f1b94c2632b30a2c018cb670688',
             y: '0' },
            { x: '00030824b01e8dfedf48dfeb87a4e8d58645e17e2da35dd6630beab77c0d64f3419967',
             y: '0' },
            { x: '0003232fe97f840836ef6c2a251ca8d38df210fc843ec1f7b10bbbd870336636735279',
             y: '0' },
            { x: '000329c394c5e333db4a9065cd75767ce8e57b778e4a84d3c9b4dd1e74db4293383211',
             y: '0' },
            { x: '000349ff24b1c96379354f35283d97aa85cdcf9b8cedc286a7aef904e38ba32e878242',
             y: '0' },
            { x: '0003691711e3dc82f22aef5b63ffa68577d2e69df228795f2fd2b321c8fc9e29228445',
             y: '0' },
            { x: '00037cbfdd19130a32af852349475a5b5a06d19897bdfadd4e40ddfa040fddca713486',
             y: '0' },
            { x: '000382b2605eccd3b056dee2b64c34a97383b622326fd09d1a9bf710d895471f4290',
             y: '0' },
            { x: '000391b395b7c27c07a03e8dda625c74991ca04adc7fa867d50e7f15b2fe47d3977453',
             y: '0' },
            { x: '0003aba196d2066bd0fefebcaa72e11e7ded219b45c2b6694eaf5a18b84ee7c8197681',
             y: '0' },
            { x: '0003bdb361d9b648dad936055dfab5ded5d296f6f20ec7207a78d76c855d0908937487',
             y: '0' },
            { x: '0003e87f12bcad7e25f53bf6036d83f8ce61610b7fc2e7edf5dd8b6665c5da7f824356',
             y: '0' },
            { x: '00040a373e8d4b9a8ee5bb3ef348c4b912159298351afb9ce25186839f509c5b407558',
             y: '0' },
            { x: '00045602dbe6dbd14870c4f4c67648effced60080991f85cf6f480950fc5b098153174',
             y: '0' },
            { x: '00045a2004056688d1e5cb177f7b7d2fe0570b868235080499566048738b03a5314451',
             y: '0' },
            { x: '00047b38e883d27121e34af466643c1fb30d45f13ca77c780166fc7105fd0c97617346',
             y: '0' },
            { x: '00048d2dccb3a8d8ddbf43f378c40bfa6b38e1f776dc3e3719a5376e83bdd69690834',
             y: '0' },
            { x: '0004ae01e86bf8ff201c3d7d1faf885d068797349094c5a04071bc70845e737c914613',
             y: '0' },
            { x: '0004cadbabafe407ac83978631f63f6ca8f43c099f5493040bd4dc67b8fc5ecf379675',
             y: '0' },
            { x: '0004ec54e90d2b6ed4cd23201a634bcc0d5df6e9e94d2a2b7a842a8b3914bfb4938286',
             y: '0' },
            { x: '0004f479c70601c070f421735236e4b2077f4374ead1af3be2581ede19dc4173896478',
             y: '0' },
            { x: '00050010f384b6d15394e738c0463020aae222d2accc80f42af2b9fdc8a41dc9136112',
             y: '0' },
            { x: '00053cbaba0813146e6537f573410a86da5aa2fbc3eb77e26452fe5883ef9f53530349',
             y: '0' },
            { x: '00053f72a104329918e9fad0c8f605c04d5fe903787e3337f7b6b2fbefc5e324674815',
             y: '0' },
            { x: '00055f1c27e1c80ca470169e1ec22b1b55fc7c84890551cfc2d78be1f563a1b9471501',
             y: '0' },
            { x: '000563545ec9276c3eaf1eeb012c5e3915428009ceb6dbb634b2569324cf0813970265',
             y: '0' },
            { x: '00057bfa54e24ca7c7e61430b8630c20d9673333129ebd94b41afb1c808992c0676041',
             y: '0' },
            { x: '0005836830006ab82e5424d960744fb283ef97f9e4f7787ec0b3d1995c43fd5d208283',
             y: '0' },
            { x: '0005866f92d6217dbfb81393d6df156a3d3b19b40f2baf0b209fbbfd0d1e2994166804',
             y: '0' },
            { x: '0005dad0752ee00df8809145b1569c470ae9e48e228af9da9077493c91b27ce5918973',
             y: '0' },
            { x: '0006268fce256a6210fd49d731e045900e41231967e510cfede4bf6ddba707e2728325',
             y: '0' },
            { x: '00063fe393833afb2097ee834c8aaf3933cf0c98e7ad42df565025cafd9f221e375257',
             y: '0' },
            { x: '0006b7ccfccf9305d76612f0e77852c91c747453d877929d8cb3da6915015929755030',
             y: '0' },
            { x: '0006e97ba194e0bb41183891a823dfddd02f84d357fbbba86a6bc87942a61895644528',
             y: '0' },
            { x: '0006f811ffe1cda350bbc1a07b0010e20702148fb1a743d7b8f938f50dd6060a275314',
             y: '0' },
            { x: '0006fca0e71756a6a5c2019fb058e649cafa15d0bcdbeba976199e9cf8276a45895402',
             y: '0' },
            { x: '00070b071ac0bd4bdc40739b7a20a05b7b349ff37bac1422da14571a16e70435634917',
             y: '0' },
            { x: '00071704f9ca397deb584090d92811375f75210a3f4d3da6376ce8dca20e261d638955',
             y: '0' },
            { x: '0007293aee6f73c7da894b7658a512f5377eef6910855f6ef923d2b422b697fd435451',
             y: '0' },
            { x: '0007336e24367d82d6678141abbf780938aedd73982c1adcd9466ac6975c3ae5175122',
             y: '0' },
            { x: '00076de21cebde741e3ef0ca395203c8749a7d239f642eef586fa9c7b00c6c55109187',
             y: '0' },
            { x: '000796333d920db4563dd0c0cca9e41ccbea78c88076a4728b2d8ea1b95ecfe244366',
             y: '0' },
            { x: '000797e43214722a4d9dea1c09fc7b9ed86e37fbe3c64ace54682d7beb477715179345',
             y: '0' },
            { x: '0007ab2c600d8353bc5e6cbcf537368b1d82ce605b55793947c66c58a06102a7493162',
             y: '0' },
            { x: '0007bb4e23b1ded74b3f7859f04f6ca4244ceeeba2de0f6ae0090c23012542ae364248',
             y: '0' },
            { x: '0007bc06234719f112637476f5fbbd7ed58c7ce51ed6fb941e171d78119ad22b782218',
             y: '0' },
            { x: '0007cfed54acf2136fd8a62807010d773d11360cc30fe3483a42f085565f02ec892559',
             y: '0' },
            { x: '0007de807a6fa93873c44fadd79487e128bf46d0be8b80ee3316dc5917feac71192643',
             y: '0' },
            { x: '0007e62b17240731bec823a3282be60c50e6f24a28a7493fdae1d657c0053524650553',
             y: '0' },
            { x: '000802b50346efe08ae0fa113fc491bd66fc5c71bf7285c24afcace88ea0c631999660',
             y: '0' },
            { x: '00080311d839957cf370e901c52ce6ba5c82adfc7e7e42e861d731ff761c722d361357',
             y: '0' },
            { x: '000806d8eca08fb9f07bdd1f48a7a4b4d09f55dca4316335a774f23412e794f1908427',
             y: '0' },
            { x: '00082b9e9aa09dd7b2e97a3074b60e88a97bb825c6aa7185f24a0485090072a0133618',
             y: '0' },
            { x: '000863a33e7448edd0ab88ffbb46c2242a8ae9dba1cf10e9790c16c4f035d1a7947359',
             y: '0' },
            { x: '00087cff6d660ff0bc486e9642f1f2bcfa55a2e2a6c7c2bb09a8cba43fb50258579269',
             y: '0' },
            { x: '00087e37a558f72d5a502437b56a8eb41c449037cba46da767f80189e71951e3598819',
             y: '0' },
            { x: '0008b1c9af1df3c6b1b3ff3b3b93804f5b445be6722619c9234f8a98e4b9c9fd964551',
             y: '0' },
            { x: '0008c0c7c26c8a673424d149214ab7f6e91ea42ae31c4be88fb6d2fcbc0c1668574448',
             y: '0' },
            { x: '0008ff2d0d85566b2cd6fec1c8a705b2f0b41b9b6df882868c65175be911389d53385',
             y: '0' },
            { x: '000907991a40de6a61c5b27047d086857f8fd4f4f46ea940f4764b06a9f470df108260',
             y: '0' },
            { x: '000907c2f4971a6dbd0c9e14c68c0dd45cf325b1ba39beb15c92f29ab0f4d384118292',
             y: '0' },
            { x: '000914b274512f660322a27ad62783f5df766b52e27b5d3b3d606d9d4cae7d4a49548',
             y: '0' },
            { x: '00091cc9ba242c9ee1ea02799e0283586fc8afe532978b7fcb1bb7df584f7be4572810',
             y: '0' },
            { x: '000924f5e14d0e6d33e9b1c45460465d5b1649129e6b768dbc629b3a6cf260f9421934',
             y: '0' },
            { x: '00092827f2d70607bd87212f3a59d92fac32878ca913931cc679c745c4f32936400480',
             y: '0' },
            { x: '00092f377a4736f2f1338d2815ffb194c608e1f5097b141ec31ca389053a0627392971',
             y: '0' },
            { x: '00092fd35c1ffbe5797bea938cd8717dcf1837adfc7aedb5a8768059d229d650446428',
             y: '0' },
            { x: '000935c8888c4de04d6031de2f4077cb97eba0b722aa639b159e0dff78564839132944',
             y: '0' },
            { x: '0009444c41b16c078ddeab5c1bb8da2036e36229728180eec6c141dd2c3280e3311706',
             y: '0' },
            { x: '0009482bd801e06d15a89225882700bb9c2c728ea71b8ae0e38ba3447411ba3e219543',
             y: '0' },
            { x: '0009575210c6a2642971592e9f9130d5c70890008f97988f10ed1c9b5571c01d264698',
             y: '0' }
          ],
        }
      ]

      /* Random Data Generator (took from nvd3.org) */
      function generateData(groups, points) {
          var data = [],
              shapes = ['circle', 'cross', 'triangle-up', 'triangle-down', 'diamond', 'square'],
              random = d3.random.normal();

          for (var i = 0; i < groups; i++) {
              data.push({
                  key: 'Group ' + i,
                  values: []
              });

              for (var j = 0; j < points; j++) {
                  data[i].values.push({
                      x: random()
                      , y: random()
                      , size: Math.random()
                      , shape: shapes[j % 6]
                  });
              }
          }
          return data;
      }

      return chart_obj;
    },

    this.pieChart = function(data){
        var chart_obj = {}
        chart_obj.options = {
            chart: {
                type: 'pieChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 100
                },
                x: function(d){return d.x;},
                y: function(d){return d.y;},
                showLabels: false,
                tooltipContent: function(key, x, e, graph) {
                    return "<p style='padding:10px;color:black;text-align:left;font-weight:bolder'> "+e.label+"<br> Value : "+e.value+" </p>" ;
                },
                transitionDuration: 500,
                labelThreshold: 0.01,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                },
                legendPosition:"right",
            }
        };
        chart_obj.data = data;
        return chart_obj;
    }
});